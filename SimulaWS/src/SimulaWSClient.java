import java.net.URL;
import java.util.HashMap;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import java.net.MalformedURLException;

public class SimulaWSClient implements SimulaWS {

	//The simula web services are locate in: "http://simula.no:9080/simula/xmlrpcdes"
	private XmlRpcClientConfigImpl rpcConfig;
	public XmlRpcClient rcpClient;
	
	public SimulaWSClient()
	{
		this.rpcConfig = new XmlRpcClientConfigImpl();
		this.rcpClient = new XmlRpcClient();
	}
	
	public void configure(String webServiceUrl) throws MalformedURLException 
	{
		this.rpcConfig.setServerURL(new URL(webServiceUrl));
		this.rcpClient.setConfig(rpcConfig);
	}
	
	/*
	 * This method retrieves all the people from the Simula website*/		
	public Object[] getPeopleList() {
		try
		{
			HashMap hash = new HashMap(); /* When hash is empty, there is no filtering which means: retrive all the people */
			Object[] methodParams = new Object[]{ hash }; /* The hash must always be contained in a Object array according to XmlRcp convention */
	    	Object resultSet = this.rcpClient.execute("getPeople", methodParams);
	    	return (Object[])resultSet;
		}
		catch (XmlRpcException xmlException)
		{
			/* If exception encounted, the default should be an empty array */
			return new Object[]{};	
		}
	}

	public HashMap getPerson(String username) {
		try
		{
			HashMap hash = new HashMap(); 
			hash.put("id", username); /* Keyword 'id' is used when searching criteria is based on username */
			Object[] methodParams = new Object[]{ hash }; /* The hash must always be contained in a Object array according to XmlRcp convention */
	    	Object resultSet = this.rcpClient.execute("getPeople", methodParams);
	    	Object[] elementList = (Object[])resultSet; /* Convert */
	    	HashMap element = (HashMap)elementList[0];
	    	return element;
		}
		catch (XmlRpcException xmlException)
		{
			/* If exception encounted, the default should be an empty HashMap*/
			return new HashMap();	
		}
	}

	/*
	 * This method retrieves all the available publications from the Simula website*/		
	public Object[] getPubicationList() {
		try
		{
			HashMap hash = new HashMap(); /* When hash is empty, there is no filtering which means: retrive all the publications */
			Object[] methodParams = new Object[]{ hash }; /* The hash must always be contained in a Object array according to XmlRcp convention */
	    	Object resultSet = this.rcpClient.execute("getPublications", methodParams);
	    	return (Object[])resultSet;
		}
		catch (XmlRpcException xmlException)
		{
			/* If exception encounted, the default should be an empty array */
			return new Object[]{};	
		}
	}

	public HashMap getPublication(String publicationId) {
		try
		{
			HashMap hash = new HashMap(); /* When hash is empty, there is no filtering which means: retrive all the publications */
			hash.put("id", publicationId); /* Keyword 'id' is used when searching criteria is based on publication id */
			Object[] methodParams = new Object[]{ hash }; /* The hash must always be contained in a Object array according to XmlRcp convention */
	    	Object resultSet = this.rcpClient.execute("getPublications", methodParams);
	    	Object[] elementList = (Object[])resultSet; /* Convert */
	    	HashMap element = (HashMap)elementList[0];
	    	return element;
		}
		catch (XmlRpcException xmlException)
		{
			/* If exception encounted, the default should be an empty HashMap*/
			return new HashMap();	
		}
	}

	public Object[] getPublicationbyAuthor(String authorId) {
		try
		{
			HashMap hash = new HashMap(); /* When hash is empty, there is no filtering which means: retrive all the publications */
			hash.put("user", authorId); /* Keyword 'authors' is used when searching criteria is based on author's any part of their name */
			Object[] methodParams = new Object[]{ hash }; /* The hash must always be contained in a Object array according to XmlRcp convention */
	    	Object resultSet = this.rcpClient.execute("getPublication", methodParams);
	    	Object[] elementList = (Object[])resultSet; /* Convert */
	    	return elementList;
		}
		catch (XmlRpcException xmlException)
		{
			/* If exception encounted, the default should be an empty array */
			return new Object[]{};	
		}
	}

	public Boolean isLogged(String username) {
		return true;
	}

	public Boolean login(String username, String password) {
		// TODO Auto-generated method stub
		return true;
	}
	
	public static void main(String [] args)
	{
		SimulaWSClient client = new SimulaWSClient();
		try 
		{
			client.configure("http://simula.no:9080/simula/xmlrpcdes");
			HashMap hash = new HashMap();
			
			//For people
			System.out.println("1. Results from: client.getPeople({'firstname':'Martin Opstad'})");
			hash.put("firstname", "Martin");
			Object[] methodParams = new Object[]{ hash };
	    	Object resultSet = client.rcpClient.execute("getPeople", methodParams);
	    	Object[] elementList = (Object[])resultSet;
	    	HashMap element = (HashMap)elementList[0];
    		Object[] keyList = element.keySet().toArray();
	    	for(int i = 0; i< keyList.length; i++)
	    		System.out.print(keyList[i]+ ":" + element.get(keyList[i]).toString() + ", ");
	    	
	    	System.out.println("\n");
	    	
	    	System.out.println("2. Results from: client.getPeople({'email':'kaspar@simula.no'})");
			hash.clear();
	    	hash.put("email", "kaspar@simula.no");
			methodParams = new Object[]{ hash };
	    	resultSet = client.rcpClient.execute("getPeople", methodParams);
	    	elementList = (Object[])resultSet;
	    	element = (HashMap)elementList[0];
    		keyList = element.keySet().toArray();
	    	for(int i = 0; i< keyList.length; i++)
	    		System.out.print(keyList[i]+ ":" + element.get(keyList[i]).toString() + ", ");
	    	
	    	System.out.println("\n");
	    	
	    	System.out.println("3. Results from: client.getPeople({'lastname':'Logg'})");
			hash.clear();
	    	hash.put("lastname", "Logg");
			methodParams = new Object[]{ hash };
	    	resultSet = client.rcpClient.execute("getPeople", methodParams);
	    	elementList = (Object[])resultSet;
	    	element = (HashMap)elementList[0];
    		keyList = element.keySet().toArray();
	    	for(int i = 0; i< keyList.length; i++)
	    		System.out.print(keyList[i]+ ":" + element.get(keyList[i]).toString() + ", ");
	    	
	    	System.out.println("\n");
	    	
	    	System.out.println("4. Results from: client.getPeople({'id':'aiko'})");
			hash.clear();
	    	hash.put("id", "aiko");
			methodParams = new Object[]{ hash };
	    	resultSet = client.rcpClient.execute("getPeople", methodParams);
	    	elementList = (Object[])resultSet;
	    	element = (HashMap)elementList[0];
    		keyList = element.keySet().toArray();
	    	for(int i = 0; i< keyList.length; i++)
	    		System.out.print(keyList[i]+ ":" + element.get(keyList[i]).toString() + ", ");
	    	
	    	System.out.println("\n");
	    	
	    	System.out.println("5. Results from: client.getPublications({'user':'benestad', 'sort_on':'Title'})");
			hash.clear();
			hash.put("user", "benestad");
	    	hash.put("sort_on", "title");
			methodParams = new Object[]{ hash };
	    	resultSet = client.rcpClient.execute("getPublications", methodParams);
	    	elementList = (Object[])resultSet;
	    	for(int j = 0; j<elementList.length; j++)
	    	{
	    		element = (HashMap)elementList[j];
	    		System.out.print("'"+element.get("title").toString().replaceAll("%20", " ") + "'  ");
	    		Object[] authors = (Object[])element.get("authors");
		    	System.out.print("Authors:");
		    	for (int i = 0; i < authors.length; i++)
		    		System.out.print(((HashMap)authors[i]).get("firstname")+", ");
		    	System.out.print("\n");
	    	}	    	
	    	System.out.print("\n");
	    	
	    	System.out.println("6. Results from: client.getPublications({'title':'UML', 'authors':'James', 'authors':'Erik', 'sort_on':'id' 'sort_on':'Source'})");
			hash.clear();
			hash.put("title", "UML");
			hash.put("authors", "James");
			hash.put("authors", "Erik");
			hash.put("sort_on", "id");
			hash.put("sort_on", "Source");
			
     		methodParams = new Object[]{ hash };
	    	resultSet = client.rcpClient.execute("getPublications", methodParams);
	    	elementList = (Object[])resultSet;
	    	for(int j = 0; j<elementList.length; j++)
	    	{
		    	element = (HashMap)elementList[j];
		    	Object[] authors = (Object[])element.get("authors");
		    	System.out.print("source:" + element.get("source").toString()+", ");
		    	System.out.print("id:" + element.get("id").toString()+", ");
		    	System.out.print("Authors:");
		    	for (int i = 0; i < authors.length; i++)
		    		System.out.print(((HashMap)authors[i]).get("firstname")+", ");
		    	System.out.println("Title:"+ element.get("title").toString().replaceAll("%20", " "));
		    	
	    	}	    	
	    	System.out.print("\n");
	    	
	    	System.out.println("7. Results from: client.getPublications({'id':'Simula.SE', 'authors':'Aiko', 'sort_on':'Type'})");
			hash.clear();
			hash.put("id", "Simula.SE");
			hash.put("authors", "Aiko");
	    	hash.put("sort_on", "Type");
	    	methodParams = new Object[]{ hash };
	    	resultSet = client.rcpClient.execute("getPublications", methodParams);
	    	elementList = (Object[])resultSet;
	    	for(int j = 0; j<elementList.length; j++)
	    	{
		    	element = (HashMap)elementList[j];
		    	Object[] authors = (Object[])element.get("Authors");
		    	System.out.println("Publication id:"+ element.get("id").toString());
		    	System.out.println("Publication type:"+ element.get("Type").toString());
		    	System.out.print("Authors:");
		    	for (int i = 0; i < authors.length; i++)
		    		System.out.print(((HashMap)authors[i]).get("firstname")+", ");
		    	System.out.println("\n");
	    	}	    	
	    	
	    	System.out.println("8. Results from: client.getPublications({'author':'Shaukat', 'sort_on':'abstract'})");
			hash.clear();
			hash.put("authors", "Shaukat");
			hash.put("authors", "Lionel");
			hash.put("sort_on", "abstract");
	    	methodParams = new Object[]{ hash };
	    	resultSet = client.rcpClient.execute("getPublications", methodParams);
	    	elementList = (Object[])resultSet;
	    	for(int j = 0; j<elementList.length; j++)
	    	{
		    	element = (HashMap)elementList[j];
		    	Object[] authors = (Object[])element.get("authors");
		    	for (int i = 0; i < authors.length; i++)
		    		System.out.print(((HashMap)authors[i]).get("firstname")+", ");
		    	System.out.print("\n");
		    	System.out.println("Abstract:"+ element.get("abstract").toString().replaceAll("%20", " "));
		    }	    	
	    	System.out.println("\n");
	    	
	    	System.out.println("9. Results from: client.getPeople({'fistname':'Kristin', 'sort_on':'lastname'})");
			hash.clear();
	    	hash.put("jobtitle", "PhD");
			hash.put("firstname", "Kristin");
	    	//hash.put("sort_on", "jobtitle");
			methodParams = new Object[]{ hash };
	    	resultSet = client.rcpClient.execute("getPeople", methodParams);
	    	elementList = (Object[])resultSet;
	    	for(int j = 0; j<elementList.length; j++)
	    	{
		    	element = (HashMap)elementList[j];
	    		keyList = element.keySet().toArray();
		    	for(int i = 0; i< keyList.length; i++)
		    		System.out.print(keyList[i]+ ":" + element.get(keyList[i]).toString() + ", ");
		    	System.out.print("\n");
	    	}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
