<%@ page import="com.tec.des.http.*" %>
<%@ page import="com.tec.des.util.Constants" %>
<%@ taglib uri="/util" prefix="util" %>
<jsp:include page="top.jsp"/>
<jsp:useBean id="adminText" class="java.lang.String" scope="request"/> 
<!-- Start navigation text -->
<TABLE cellspacing="0" cellpadding="0" width="98%">
<TR><TD class="path" valign="top">&nbsp;&nbsp;&nbsp;edit administration page</TD>
<TD align="right"><A href="JavaScript:help('help.html#editopeningpage')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
</TR>
</TABLE>
<!-- End navigation text -->
<TABLE cellspacing="4" cellpadding="4">
<jsp:include page="messages.jsp"/>
<FORM name="form" action="<%=WebConstants.FRONTCONTROLLER_URL%>" method="post">
<TABLE cellspacing="4" cellpadding="4"> 
    <TR>
        <TD colspan="3">
            <H2>Edit Text on the Administration Opening Page</H2>
        </TD>
    </TR> 
    <TR>
        <TD class="bodytext">
            <TEXTAREA class="bodytext" cols="150" rows="20" name="<%=WebKeys.REQUEST_PARAM_ADMIN_PAGE_TEXT%>"><%=adminText%></TEXTAREA>
        </TD>
    </TR>
    <TR>
        <TD align="left">
            <INPUT type="submit" name="" value="Save" class="bodytext" onclick="document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_EDIT_ADMIN_PAGE%>';">
            <INPUT type="submit" name="" value="Cancel" class="bodytext" onclick="document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_OPEN_ADMIN_PAGE%>';">
        </TD>
    </TR>
</TABLE>
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="" > 
</FORM>
</TABLE> 
<jsp:include page="bottom.html"/>