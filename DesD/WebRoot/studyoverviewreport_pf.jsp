<%@ page import="com.tec.des.http.*" %>
<%@ page import="com.tec.des.util.Validator" %>
<%@ taglib uri="/util" prefix="util" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<HTML>
<HEAD>
<TITLE>[ simula . research laboratory ]</TITLE>
<META http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<LINK rel="stylesheet" type="text/css" href="simula.css">
</HEAD>
<BODY topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<jsp:useBean id="searchResult" class="com.tec.shared.util.HitList" scope="request"/> 
<TABLE cellspacing="4" cellpadding="4" width="100%" bgcolor="#f5f5f5"><TR><TD>
<jsp:include page="messages.jsp"/>
<util:hitlist name="hitlist" mode="search" maxHits="<%=searchResult.getCount()%>" group="<%= (Object[])searchResult.toArray(new Object[0]) %>">
<TABLE cellspacing="4" cellpadding="1" bgcolor="#f5f5f5"> 
    <TR>
        <TD colspan="2"><H2>Study Overview Report</H2></TD>
        <TD colspan="4" align="right" class="bodytext" valign="top"></TD>
    </TR> 
    <TR>
        <TD class="bodytext-bold" valign="top">Study Name</TD>  
        <TD class="bodytext-bold" valign="top">Type Of Study</TD> 
        <TD class="bodytext-bold" valign="top">End Of Study</TD>
        <TD class="bodytext-bold" valign="top">Study Responsibles</TD>
        <TD class="bodytext-bold" valign="top">Study Description</TD>
        <TD class="bodytext-bold" valign="top">Publications</TD> 
    </TR>

    <util:iteration name="study" type="com.tec.des.dto.StudySDTO" rowAlternator="row" group="<%= (Object[])searchResult.toArray(new Object[0]) %>">
    <TR>
        <TD class="bodytext" valign="top"><%=study.getName()%></TD>
        <TD class="bodytext" valign="top"><%=study.getType()%></TD>
        <TD class="bodytext" valign="top"><%=study.getEndDate()%></TD>
        <TD class="bodytext" valign="top"><util:arrayprinter items="<%= (String[])study.getResponsibles().values().toArray(new String[0]) %>" separator="<BR>" /></TD>
        <TD class="bodytext" valign="top"><%=Validator.replaceEnterWithBreak(study.getDescription())%></TD>
        <TD class="bodytext" valign="top"><util:arrayprinter items="<%= (String[])study.getPublications().values().toArray(new String[0]) %>" separator="<BR>" /></TD> 
    </TR>
    </util:iteration>
</TABLE>
</util:hitlist>
</TD></TR> 
</TABLE> 
