<%@ page import="com.tec.des.http.*" %>
<%@ taglib uri="/util" prefix="util" %>
<jsp:include page="top.jsp"/>
<jsp:useBean id="studyTypes" class="java.util.Hashtable" scope="request"/> 
<jsp:useBean id="searchCriteria" class="com.tec.des.dto.SearchDTO" scope="request"/> 
<!-- Start navigation text -->
<TABLE cellspacing="0" cellpadding="0" width="96%">
    <TR>
        <TD class="path" valign="top"><A id="hl-link" href="<%=WebConstants.FRONTCONTROLLER_URL%>?usecase=<%=WebConstants.USECASE_OPEN_LIST_STUDIES%>">&nbsp;&nbsp;&nbsp;&nbsp;list studies</A></TD>
        <TD align="right"><A href="JavaScript:help('help.html#liststudies')" onmouseover="document.help.src='images/Help.png'" onmouseout="document.help.src='images/Help_gray.png'"><IMG height="18" width="18" border="0" name="help" src="images/Help_gray.png"></A></TD>
    </TR>
</TABLE>
<!-- End navigation text -->
<TABLE cellspacing="6" cellpadding="6"><TR><TD>
<jsp:include page="messages.jsp"/>
<FORM name="form" action="<%=WebConstants.FRONTCONTROLLER_URL%>" method="post">
<TABLE cellspacing="2" cellpadding="0"> 
    <TR>
        <TD colspan="3"><H1>List Studies</H1></TD>
    </TR> 
    <TR>
        <TD class="bodytext">Free text</TD>
        <TD colspan="2"><INPUT class="bodytext" type="text" name="<%=WebKeys.REQUEST_PARAM_SEARCH_TEXT%>" value="<%=searchCriteria.getFreeText()%>" size="50"></TD>
    </TR> 
    <TR>
        <TD class="bodytext">Type of Study</TD>  
        <TD class="bodytext">
            <util:dropdownlist name="<%=WebKeys.REQUEST_PARAM_SEARCH_TYPE_OF_STUDY%>" attributes="class=bodytext" optional="true" displayedvalues="<%=(String[])studyTypes.values().toArray(new String[0]) %>" selectedvalue="<%=searchCriteria.getTypeOfStudy() %>" optionValues="<%=(String[])studyTypes.keySet().toArray(new String[0]) %>" /> 
        </TD>
    </TR>
    <TR>
        <TD class="bodytext">End of Study</TD>
        <TD class="bodytext">
            <INPUT type="text" name="<%=WebKeys.REQUEST_PARAM_SEARCH_END_DATE_FROM%>" value="<%=searchCriteria.getEndOfStudyFrom()%>" size="7" maxlength="10" class="bodytext"> 
            <A href="JavaScript:calendar()" onclick="document.form.target='calendar';document.form.action='calendar.jsp';document.form.<%=WebKeys.REQUEST_PARAM_DATE_FIELD%>.value='<%=WebKeys.REQUEST_PARAM_SEARCH_END_DATE_FROM%>'">From</A>&nbsp;&nbsp;
            <INPUT type="text" name="<%=WebKeys.REQUEST_PARAM_SEARCH_END_DATE_TO%>" value="<%=searchCriteria.getEndOfStudyTo()%>" size="7" maxlength="10" class="bodytext"> 
            <A href="JavaScript:calendar()" onclick="document.form.target='calendar';document.form.action='calendar.jsp';document.form.<%=WebKeys.REQUEST_PARAM_DATE_FIELD%>.value='<%=WebKeys.REQUEST_PARAM_SEARCH_END_DATE_TO%>'">To</A>&nbsp;&nbsp; 
            <FONT class="bodytext-italic">yyyy/mm/dd</FONT> 
        </TD>
    </TR>
    <TR>
        <TD class="bodytext">Study Responsibles</TD>
        <TD colspan="2"><INPUT type="text" name="<%=WebKeys.REQUEST_PARAM_SEARCH_STUDY_RESPONSIBLES%>" value="<%=searchCriteria.getStudyResponsibles()%>" size="50" class="bodytext"></TD>
    </TR> 
    <TR>
        <TD class="bodytext">Sort by</TD> 
        <TD class="bodytext">
            <SELECT name="<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_BY%>" class="bodytext">
            <OPTION value="<%=WebConstants.SORT_BY_STUDY_NAME%>">Study Name</OPTION>
            <OPTION value="<%=WebConstants.SORT_BY_TYPE_OF_STUDY%>">Type of Study</OPTION>
            <OPTION value="<%=WebConstants.SORT_BY_END_OF_STUDY%>" selected>End of Study </OPTION>
            </SELECT>&nbsp;&nbsp;
            <INPUT type="checkbox" name="<%=WebKeys.REQUEST_PARAM_SEARCH_SORT_DESCENDING%>" value="" class="bodytext" checked> Descending &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <INPUT type="submit" name="" value="Search" class="bodytext" onclick="document.form.target='_self';document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_LIST_STUDIES%>';">
        </TD>
    </TR>
</TABLE>
<BR><BR>
<P class="bodytext"><A href="JavaScript:document.form.submit();" onclick="document.form.target='_blank';document.form.action='<%=WebConstants.FRONTCONTROLLER_URL%>';document.form.<%=WebKeys.REQUEST_PARAM_USECASE%>.value='<%=WebConstants.USECASE_GET_GRAPHICAL_REPORT%>';">Graphical Report of Aggregated Study Information</A></P>
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_USECASE%>" value="" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_SENDER_VIEW%>" value="<%=WebConstants.VIEW_LIST_STUDIES%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_MAX_HIT_LIST_LENGTH%>" value="<%=WebConstants.MAX_HIT_LIST_LENGTH%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_START_POSITION%>" value="<%=WebConstants.DEFAULT_START_POSITION%>" > 
<INPUT type="hidden" name="<%=WebKeys.REQUEST_PARAM_DATE_FIELD%>" value="" >  
</FORM>
</TD></TR>  
</TABLE> 
<jsp:include page="bottom.html"/>