package com.tec.des.dto;
import java.util.Hashtable;
import com.tec.shared.util.Nuller;
import com.tec.des.util.Constants;

/**
 * Simple Data transfer object for a study.
 *
 * @author  Norunn Haug Christensen
 */
public class StudySDTO implements DTO {

    private int id = Nuller.getIntNull();   
    private String name = Nuller.getStringNull();
    private String type = Nuller.getStringNull();
    private String endDate = Nuller.getStringNull();
    private Hashtable responsibles;
    private String description = Nuller.getStringNull();
    private Hashtable publications;

    public StudySDTO() {
        super();
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public int getId() {
        return id;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndDate() {
        return endDate;
    }
    
    public void setResponsibles(Hashtable responsibles) {
        this.responsibles = responsibles;
    }

    public void setResponsibles(String[] ids, String[] values) {
        responsibles = new Hashtable();
        for (int i = 0; i < ids.length; i++) { 
            if (!Constants.EMPTY_ID_STUDY_RESPONSIBLES.equals(ids[i]))
                responsibles.put(ids[i], values[i]);
        }
    }
    
    public Hashtable getResponsibles() {
        if (responsibles == null)
            responsibles = new Hashtable();
        return responsibles;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }

    public void setPublications(Hashtable publications) {
        this.publications = publications;
    }
    
    public void setPublications(String[] ids, String[] values) {
        publications = new Hashtable();
        for (int i = 0; i < ids.length; i++) { 
            publications.put(ids[i], values[i]);
        }
    }

    public Hashtable getPublications() {
        if (publications == null)
            publications = new Hashtable();
        return publications;
    }
    
}
