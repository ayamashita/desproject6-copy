package com.tec.des.dto;
import com.tec.shared.util.Nuller;

/**
 * Data transfer object for a study type used in a graphical presentation.
 *
 * @author  Norunn Haug Christensen
 */
public class GraphicalStudyTypeDTO implements DTO {

    private String type = Nuller.getStringNull();
    private int number = Nuller.getIntNull();
    private String colour = Nuller.getStringNull();

    public GraphicalStudyTypeDTO() {
        super();
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String getType() {
        return type;
    }

    public void setNumber(int number) {
        this.number = number;
    }
    
    public int getNumber() {
        return number;
    }
    
    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getColour() {
        return colour;
    }

}
