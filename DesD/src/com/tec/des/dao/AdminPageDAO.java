package com.tec.des.dao;
import java.sql.*;
import com.tec.server.log4j.LoggerFactory;
import com.tec.shared.exceptions.*;
import com.tec.server.db.ResultSetHelper;
import com.tec.server.db.SQLHelper;
import com.tec.des.util.Validator;


/**
 * Class encapsulating Admin Page data access.
 *
 * @author :  Norunn Haug Christensen
 */
public class AdminPageDAO extends DAO {
    
        
 /**
  * Constructs a AdminPageDAO. 
  *
  * @throws SystemException
  * @throws SQLException
  */
  public AdminPageDAO() throws SystemException, SQLException {
    super();
  }

 /**
  * Returns the text to be displayed on the admin page.
  * @throws SQLException
  */
  public String getTextOnAdminPage() throws SQLException {
    
    String text = new String();  
    Statement stmt = getConnection().createStatement();
    ResultSet rs = stmt.executeQuery("select page_text from des_admin_page;");
    ResultSetHelper rsh = new ResultSetHelper(rs);
    if (rs.next()) {
        text = rsh.getString("page_text");
    }
    
    rs.close();
    stmt.close();
    
    return text;
  }

 /**
  * Updates the text to be displayed on the admin page.
  * @param the new text on the admin page.
  * @throws SQLException
  */
  public String updateTextOnAdminPage(String newText) throws SQLException {
    
    Statement stmt = getConnection().createStatement();
    stmt.executeUpdate("update des_admin_page set page_text = " + SQLHelper.getValue(Validator.checkEscapes(newText)) + ";");
    stmt.close();
    
    return getTextOnAdminPage();
  }
  
}
