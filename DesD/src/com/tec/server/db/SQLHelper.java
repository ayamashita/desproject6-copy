package com.tec.server.db;
import com.tec.shared.util.Nuller;

/**
 * SQL helper class. 
 *
 * @author : Norunn Haug Christensen
 */
public class SQLHelper  {
    
  private SQLHelper() {
  }
/**
 * Returns "like" if the search string starts or ends with an %, else "=" is returned.
 */
  public static String getLikeOrEquals(String searchString){
    String percent = "%";
    if (searchString.startsWith(percent)||searchString.endsWith(percent)){
      return "like";
    } else {
      return "=";
    }
    
  }
  
/**
 * Returns a "like" condition.
 */
  public static String getLike(String searchString){
    return "like '%" + searchString + "%'";
  }

/**
 * Returns null if the input value is STRINGNULL
 */
  public static String getValue(String value) {
    if (Nuller.isNull(value))
        return null;
    else 
        return "'" + value + "'";
  }

/**
 * Returns null if the input value is INTNULL
 */
  public static String getValue(int value) {
    if (Nuller.isNull(value))
        return null;
    else 
        return String.valueOf(value);
  }

  
/**
 * Returns 1 if the input value is true, 0 otherwise.
 */
  public static int getValue(boolean value) {
    if (value)
        return 1;
    else 
        return 0;
  }

}