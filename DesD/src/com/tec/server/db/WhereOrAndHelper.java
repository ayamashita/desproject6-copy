package com.tec.server.db;
/**
 * Helper class for setting where or and in sql. 
 *
 * @author : Norunn Haug Christensen
 */
public class WhereOrAndHelper  {
  private boolean first = true;
  public WhereOrAndHelper() {
    super();
  }
/**
 * Returns "where" the first time the method is called after the class is 
 * instantiated, then returns "and".
 */
  public String get(){
    if (first){
      first = false;
      return " where ";
    } else {
      return " and ";
    }
    
  }
}