<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html>
<script>
	function checkForm(){
		if(document.add.name.value==""){
			alert("The name must be specified");
		}
		else{
			document.add.submit();
		}
	}
</script>

<body>
<form>
  <head>
    <title>Add</title>
    <link rel="stylesheet" href="http://www.simula.no/simula.css">
    <meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0"> 
  </head>

  <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" link="#fe6d17" alink="#ee1e13" vlink="#d30f0c" onLoad="window.focus()">
   	<table bgcolor="#F5F5F5" width="100%" cellspacing="0" cellpadding="0" border="0">
   		<tr bgcolor="#ef1607">
			<td></td>
			<td>&nbsp;&nbsp;</td>
   			<td class="h1"><font color="#ADAEAD">Add <logic:equal name="type" value="studytype">study type</logic:equal>
   			<logic:equal name="type" value="duration">duration unit</logic:equal>
   			</font></td>
			<td>&nbsp;&nbsp;</td>
   		</tr>
   		<tr>
   		<td></td>
		<td>&nbsp;&nbsp;</td>
   		<td valign="top">
   			<form name ="add" action="editStudyTypeDuration.do">
   			<input type="hidden" name="action" value="add">
   			<input type="hidden" name="type" value="<bean:write name="type"/>">
   			<input type="hidden" name="execute" value="ok">
   			
   			<table width="100%" cellspacing="2" cellpadding="0" border="0">
			   			<tr>
			   				<td class="h2"><b>Add <logic:equal name="type" value="studytype">study type</logic:equal>
   						<logic:equal name="type" value="duration">duration unit</logic:equal></b></td>
			   			</tr>
			   			<tr>
								<td class="bodytext">&nbsp;</td>
			   		</tr>
			   			<tr>
								<td class="bodytext-bold">Name: <input type="text" name="name" size="25"></td>
			   			</tr>
							<tr>
								<td class="bodytext-bold"></td>
			   		</tr>
			  </table>
			</td>
			<td>&nbsp;&nbsp;</td>
   		</tr>
   		<tr>
			   				<td></td>
								<td>&nbsp;&nbsp;</td>
						   	<td align="center" class="bodytext-bold">
						   		<a href="JavaScript:checkForm();">Ok</a> 
						   		&nbsp;&nbsp;
						   		<a href="Javascript:window.self.close()">Cancel</a></td>
								<td>&nbsp;&nbsp;</td>
   		</tr>
   	</table>
   	</form>
   </body>
</html>