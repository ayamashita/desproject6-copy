<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>



<%@ include file="head.jsp"%>


<table border="0" width="100%" bgcolor="#F5F5F5" cellpadding="0"
	cellspacing="0">
	<tr>
		<td></td>
		<td colspan="7" class="path">
			<div id="navActions">
				<logic:present name="user">
					<a href="frontPage.do">Front page</a> > </logic:present>
				<a href="listReports.do">Reports</a>
			</div>
		</td>
	</tr>
	<tr>
	<tr>
		<td>

		</td>
		<td colspan="6">
			<h1>
				Reports
			</h1>
		</td>
		<td>

		</td>
	</tr>
	<tr>
		<td></td>
		<td colspan="6">
			<table width="100%">
				<tr>
					<td class="bodytext">
					</td>
					<td align="right" class="bodytext-bold">
						<div id="navActions">
							<logic:present name="user">
								<logic:greaterThan name="user" property="privilege" value="0">
									<a href="editReport.do">Create new report</a>
								</logic:greaterThan>
							</logic:present>
						</div>
					</td>
				</tr>
			</table>
		</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td colspan="6">
			<hr style="height: 1px; color: black; width: 100%;">
		</td>
		<td></td>
	</tr>


	<tr class="bodytext-bold">
		<td></td>
		<th align="left">
			Report name
		</th>
		<td></td>
	</tr>

	<tr>
		<td></td>
		<td colspan="6">
			<hr style="height: 1px; color: black; width: 100%;">
		</td>
		<td></td>
	<tr>
		<%
			boolean color = true;
		%>
		<logic:iterate id="report" name="reportForm" property="reports">
			<tr class="bodytext"
				<%=(color == true ? "bgcolor=\"#cccccc\""
											: "")%>
				valign="top">
				<td bgcolor="#F5F5F5"></td>
				<td align="left">
					<bean:write name="report" property="name" filter="true" />
				<td>
					<html:link page="/showReport.do" paramId="report"
						paramName="report" paramProperty="id">
						[show]
					</html:link>
					<html:link page="/showReportPdf.do" paramId="report"
						paramName="report" paramProperty="id">
						[create PDF]
					</html:link>					
					<html:link page="/editReport.do" paramId="report"
						paramName="report" paramProperty="id">
						[edit]
					</html:link>
					<html:link page="/deleteReport.do" paramId="report"
						paramName="report" paramProperty="id">
						[delete]
					</html:link>
				</td>
			</tr>
			<%
				color = (color == true ? false : true);
			%>
		</logic:iterate>
	<tr>
		<td></td>
		<td colspan="6">
			<hr style="height: 1px; color: black; width: 100%;">
		</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td colspan="6">
			<table width="100%">
				<tr>
					<td class="bodytext">
						&nbsp;
					</td>
					<td align="right" class="bodytext-bold">
						<div id="navActions">
							<logic:present name="user">
								<logic:greaterThan name="user" property="privilege" value="0">
									<a href="editReport.do">Create new report</a>&nbsp;&nbsp;&nbsp;&nbsp;
	  							</logic:greaterThan>
							</logic:present>
						</div>
					</td>
				</tr>
			</table>
		</td>
		<td></td>
	</tr>

</table>

<%@ include file="footer.jsp"%>