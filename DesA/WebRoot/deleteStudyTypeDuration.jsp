<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html>

<body>
<form>
  <head>
    <title>Add</title>
    <link rel="stylesheet" href="http://www.simula.no/simula.css">
    <meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Pragma" content="no-cache">
		<meta http-equiv="Expires" content="0"> 
  </head>

  <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" link="#fe6d17" alink="#ee1e13" vlink="#d30f0c" onLoad="window.focus()">
   	<table bgcolor="#F5F5F5" width="100%" cellspacing="0" cellpadding="0" border="0">
   		<tr bgcolor="#ef1607">
			<td></td>
			<td>&nbsp;&nbsp;</td>
   			<td class="h1"><font color="#ADAEAD">Delete <logic:equal name="type" value="studytype">study type</logic:equal>
   			<logic:equal name="type" value="duration">duration unit</logic:equal>
   			</font></td>
			<td>&nbsp;&nbsp;</td>
   		</tr>
   		<tr>
   		<td></td>
		<td>&nbsp;&nbsp;</td>
   		<td valign="top">
   			<form name ="deleteStudyTypeDuration" action="editStudyTypeDuration.do">
   			<input type="hidden" name="action" value="delete">
   			<input type="hidden" name="type" value="<bean:write name="type"/>">
   			<input type="hidden" name="execute" value="ok">
   			<input type="hidden" name="id" value="<bean:write name="id"/>">
   			<input type="hidden" name="name" value="<bean:write name="name"/>">
   			
   			<table width="100%" cellspacing="2" cellpadding="0" border="0">
			   			<tr>
			   				<td class="h2"><b>Delete <logic:equal name="type" value="studytype">study type</logic:equal>
   						<logic:equal name="type" value="duration">duration unit</logic:equal></b></td>
			   			</tr>
			   			<tr>
								<td class="bodytext">
									<span style="color:red;">
    							<html:errors/>
									</span>
								</td>
			   		</tr>
			   			<tr>
								<td class="bodytext-bold">Name: 
			   			</tr>
							<tr bgcolor="white">
								<td class="bodytext-bold"><bean:write name="name"/></td>
			   		</tr>
			  </table>
			</td>
			<td>&nbsp;&nbsp;</td>
   		</tr>
   		<tr>
			   				<td></td>
								<td>&nbsp;&nbsp;</td>
						   	<td align="center" class="bodytext-bold">
						   		<a href="JavaScript:document.deleteStudyTypeDuration.submit();">Ok</a>
						   		&nbsp;&nbsp;
						   		<a href="Javascript:window.self.close()">Cancel</a></td>
								<td>&nbsp;&nbsp;</td>
   		</tr>
   	</table>
   	</form>
   </body>
</html>