<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>

<html>
<body>
  <head>
    <title>Delete Responsible</title>
    <link rel="stylesheet" href="http://www.simula.no/simula.css">
    <meta http-equiv="Cache-Control" content="no-cache">
		<meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0"> 
  </head>

  <body topmargin="0" leftmargin="0" marginheight="0" marginwidth="0" link="#fe6d17" alink="#ee1e13" vlink="#d30f0c" onLoad="window.focus()">
   	<table bgcolor="#F5F5F5" width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
   		<tr bgcolor="#ef1607">
			<td></td>
			<td>&nbsp;&nbsp;</td>
   			<td class="h1"><font color="#ADAEAD">Confirm</font></td>
			<td>&nbsp;&nbsp;</td>
   		</tr>
   		<tr>
   		<td></td>
		<td>&nbsp;&nbsp;</td>
   		<td valign="top">
   			<table width="100%" cellspacing="2" cellpadding="0" border="0">
			   			<tr>
			   				<td class="h2"><b>You are about to delete</b></td>
			   			</tr>
			   			<tr>
									<td class="h2"><b>Publication</b></td>
			   			</tr>
			   			<tr>
								<td bgcolor="white" class="bodytext"><bean:write name="name"/></td>
			   			</tr>
			   			<tr>
								<td class="bodytext-bold">From study:</td>
			   			</tr>
						<tr bgcolor="white">
								<td class="bodytext"><bean:write name="studyForm" property="name"/></td>
			   			</tr>
						<tr><td>&nbsp;</td>
						</tr>
						<tr>
							<td align="center" class="bodytext-bold">
																<a href="removePublication.do?publication=<bean:write name="id"/>">OK</a>&nbsp;&nbsp;&nbsp;										
													   		<a href="Javascript:window.opener.location.href=window.opener.location;window.self.close()">Cancel</a></td>
								<td>&nbsp;&nbsp;</td>
						</tr>
			  </table>
			</td>
			<td>&nbsp;&nbsp;</td>
   		</tr>
   		
   	</table>
   </body>
</html>