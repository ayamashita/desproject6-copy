CREATE TABLE reports (
  id int(4) NOT NULL auto_increment,
  name varchar(255) NOT NULL,
  owner_id varchar(255) NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY (name, owner_id)
) ENGINE=MyISAM DEFAULT CHARSET=latin2 PACK_KEYS=1;

CREATE TABLE report_parameters (
  id int(4) NOT NULL auto_increment,
  report_id int(4) not null,
  value varchar(255) NOT NULL,
  type varchar(255) NOT NULL,
  report_index varchar(255) NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY (report_id, report_index)
) ENGINE=MyISAM DEFAULT CHARSET=latin2 PACK_KEYS=1;

