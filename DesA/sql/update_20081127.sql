drop table publication_study;

CREATE TABLE `publication_study` (
  `study_id` int(10) NOT NULL,
  `publication_id` varchar(255) NOT NULL,
  PRIMARY KEY  (`study_id`,`publication_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin2 PACK_KEYS=1;

CREATE TABLE `responsible_study` (
  `study_id` int(10) NOT NULL,
  `responsible_id` varchar(255) NOT NULL,
  PRIMARY KEY  (`study_id`,`responsible_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin2 PACK_KEYS=1;


drop table adminprivileges;

CREATE TABLE `adminprivileges` (
  `people_id` varchar(255) NOT NULL,
  `admin_privileges` int(1) NOT NULL,
  PRIMARY KEY  (`people_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 PACK_KEYS=1;

alter table study CHANGE study_edited_by study_edited_by varchar(255) not null;

alter table study CHANGE study_created_by study_created_by varchar(255) not null;

