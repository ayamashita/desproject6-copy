package no.simula.des.tools;

import java.util.ResourceBundle;

public class PropertiesHelper {

	private static ResourceBundle prb = ResourceBundle.getBundle("des");
	
	public static boolean isSecurityDisabled() {
		String strIsSecurityDisabled = prb.getString("IS_SECURITY_DISABLED");
		return Boolean.valueOf(strIsSecurityDisabled.trim()).booleanValue();
	}
	
}
