package no.simula.des.tools;

import java.util.Iterator;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

public class RequestHelper {

	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(RequestHelper.class);
	
	public static void debugParameters(HttpServletRequest request) {
		debugParameters(request, LOG);
	}
	
	public static void debugParameters(HttpServletRequest request, org.apache.log4j.Logger LOG) {
		java.util.Map params = request.getParameterMap();
		Set keys = params.keySet();
		LOG.debug(params.size() + " <-- params.size()");
		for (Iterator iterator = keys.iterator(); iterator.hasNext();) {
			Object key = (Object) iterator.next();
			LOG.debug(key + " <-- key");
			LOG.debug(params.get(key) + " <-- value");
		}
	}	
	
}
