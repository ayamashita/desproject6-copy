/*
 * Created on 07.okt.2003
 *
 */
package no.simula.des.beans;

import java.io.Serializable;


/**
 * Simple bean used in various settings. Contains a single name - value pair as Strings.
 */
public class NameValueBean implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2596535742689341647L;
	private String name;
    private String value;

    public NameValueBean(String name, String value) {
    	this.name = name;
    	this.value = value;
    }
    
    public NameValueBean() {
	}

	/**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param string the new name
     */
    public void setName(String string) {
        name = string;
    }

    /**
     * @param string the new value
     */
    public void setValue(String string) {
        value = string;
    }
}
