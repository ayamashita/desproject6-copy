/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.forms;

import no.simula.des.data.beans.PeoplesBean;

import org.apache.struts.action.ActionForm;


/**
 * ActionForms are placeholders for data that are displayed in or collected from a page in a
 * Struts based solution, whether the page actually contains a HTML form or not.
 *
 * This form represents dynamic data on the "grant privileges" popup - that is a list of people
 * along with their privileges.
 */
public class GrantPrivilegesForm extends ActionForm {
    /**
	 * 
	 */
	private static final long serialVersionUID = -1533011219746178809L;
	private PeoplesBean peoples;

    public PeoplesBean getPeoples() {
        return peoples;
    }

    public void setPeoples(PeoplesBean peoples) {
        this.peoples = peoples;
    }
}
