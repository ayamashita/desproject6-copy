package no.simula.des.struts.forms;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import no.simula.des.beans.NameValueBean;
import no.simula.des.data.beans.ReportBean;
import no.simula.des.data.beans.ReportParameterBean;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ReportForm extends ActionForm implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 375804574327169766L;
	
	public static final int SAVE_REPORT_ACTION = 0;
	public static final int ADD_PARAMETER_ACTION = 1;

	public static final int FIRST_RESPONSIBLE_PARAMETER_INDEX = 0;
	public static final int ORDER_PARAMETER_INDEX = 1;
	
	public static final String JOIN_FIELD_PREFIX = "JOIN_";
	public static final String JOIN_PUBLICATIONS_FILED_VALUE = JOIN_FIELD_PREFIX + "publications";
	public static final String JOIN_RESPONSIBLES_FILED_VALUE = JOIN_FIELD_PREFIX + "responsibles";
	
	private ReportBean currentReport;
	private List reports;

	private List allFields;

	private int action = SAVE_REPORT_ACTION;
	
	private String selectParameters;
	
	private List executedReport;
	
	public ReportForm() {
		currentReport = new ReportBean();
		currentReport.setParameters(new ArrayList());
		ReportParameterBean firstParameter = new ReportParameterBean();
		firstParameter.setType(ReportParameterBean.TYPE_AND);
		firstParameter.setIndex(0);
		currentReport.getParameters().add(0, firstParameter);
		
		ReportParameterBean secondParameter = new ReportParameterBean();
		secondParameter.setType(ReportParameterBean.TYPE_ORDER);
		secondParameter.setIndex(1);
		currentReport.getParameters().add(1, secondParameter);		
		
		reports = new ArrayList();
		allFields = new ArrayList();
		allFields.add(new NameValueBean("study_id", "study_id"));
		allFields.add(new NameValueBean("study_name", "study_name"));
		allFields.add(new NameValueBean("study_type", "study_type"));
		allFields.add(new NameValueBean("study_description", "study_description"));
		allFields.add(new NameValueBean("study_keywords", "study_keywords"));
		allFields.add(new NameValueBean("study_notes", "study_notes"));
		allFields.add(new NameValueBean("study_students", "study_students"));
		allFields.add(new NameValueBean("study_professionals", "study_professionals"));
		allFields.add(new NameValueBean("study_duration", "study_duration"));
		allFields.add(new NameValueBean("study_duration_unit", "study_duration_unit"));
		allFields.add(new NameValueBean("study_start_date", "study_start_date"));
		allFields.add(new NameValueBean("study_end_date", "study_end_date"));
		allFields.add(new NameValueBean("study_created_by", "study_created_by"));
		allFields.add(new NameValueBean("study_created_date", "study_created_date"));
		allFields.add(new NameValueBean("study_edited_by", "study_edited_by"));
		allFields.add(new NameValueBean("study_edited_date", "study_edited_date"));
		allFields.add(new NameValueBean("publications", JOIN_PUBLICATIONS_FILED_VALUE));
		allFields.add(new NameValueBean("responsibles", JOIN_RESPONSIBLES_FILED_VALUE));
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		boolean isOk = true;
		if (action == SAVE_REPORT_ACTION) {
	        if (StringUtils.isEmpty(getCurrentReport().getName())) {
	            //errors.add("description", new ActionError("Fields requiered"));
	            isOk = false;
	        }
	        
	        List params = getCurrentReport().getParameters();

	        boolean responsibleSelected = false;
	        
	        for (Iterator iterator = params.iterator(); iterator.hasNext();) {
				ReportParameterBean param = (ReportParameterBean) iterator.next();
				
				if (param.getType().equals(ReportParameterBean.TYPE_AND) && StringUtils.isNotEmpty(param.getValue())) {
					responsibleSelected = true;
					break;
				}
			}

	        boolean fieldsSelected = (selectParameters.indexOf(';') > 0);

	        
	        if (!fieldsSelected) {
	            //errors.add("description", new ActionError("Fields requiered"));
	            isOk = false;
	        }
	        
	        if (!responsibleSelected) {
	            //errors.add("description", new ActionError("Fields requiered"));
	            isOk = false;
	        }
		}
		
		if (!isOk) {
			errors.add(null, new ActionError("error.des.report.required"));
		}
		
		return isOk ? null : errors;
	}
	
	public ReportBean getCurrentReport() {
		return currentReport;
	}


	public void setCurrentReport(ReportBean currentReport) {
		this.currentReport = currentReport;
		initSelectParameters();
	}

	private void initSelectParameters() {
		if (currentReport != null) {
			List params = currentReport.getParameters();
			List selectParameters = new ArrayList();
			for (int i = 0; i < params.size(); i++) {
				ReportParameterBean reportParameterBean = (ReportParameterBean) params.get(i);
				if (ReportParameterBean.TYPE_SELECT.equals(reportParameterBean.getType())) {
					selectParameters.add(reportParameterBean.getValue());
				}
			}
			setSelectParameters(selectParameters);
		}

		
	}
	
	public List getReports() {
		return reports;
	}


	public void setReports(List reports) {
		this.reports = reports;
	}


	public List getAllFields() {
		return allFields;
	}


	public void setAllFields(List allFields) {
		this.allFields = allFields;
	}

	public int getAction() {
		return action;
	}


	public void setAction(int action) {
		this.action = action;
	}


	public String getSelectParameters() {
		return selectParameters;
	}


	public void setSelectParameters(String selectParameters) {
		this.selectParameters = selectParameters;
	}

	public String[] getSelectparametersAsArray() {
		if (selectParameters == null) {
			return null;
		}
		return selectParameters.split(";");
	}
	
	public void setSelectParameters(List selectParameters) {
		if (selectParameters == null) {
			this.selectParameters = null;
		}
		this.selectParameters = "";
		for (Iterator iterator = selectParameters.iterator(); iterator
				.hasNext();) {
			String string = (String) iterator.next();
			this.selectParameters += string + ";";
		}
	}

	public List getExecutedReport() {
		return executedReport;
	}

	public void setExecutedReport(List report) {
		this.executedReport = report;
	}

}
