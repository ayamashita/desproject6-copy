/*
 * Created on 01.okt.2003
 *
 *
 */
package no.simula.des.struts.forms;

import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


/**
 * ActionForms are placeholders for data that are displayed in or collected from a page in a
 * Struts based solution, whether the page actually contains a HTML form or not.
 *
 * This form contains logon data
 */
public class LogonForm extends ActionForm {
    /**
	 * 
	 */
	private static final long serialVersionUID = -5750001790876816283L;
	private String username;
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void reset(ActionMapping mapping,
        javax.servlet.http.HttpServletRequest request) {
        this.username = null;
        this.password = null;
    }

    public ActionErrors validate(ActionMapping mapping,
        javax.servlet.http.HttpServletRequest request) {
        if (username.equals("") || password.equals("")) {
            ActionErrors errors = new ActionErrors();
            errors.add("username",
                new ActionError("error.des.logon.fieldsRequired",
                    "Fields requiered"));

            return errors;
        }

        return null;
    }

    public String toString() {
        return "Username: " + username + ", Password: " + password;
    }
}
