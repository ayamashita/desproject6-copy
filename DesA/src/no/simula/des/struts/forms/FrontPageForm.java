package no.simula.des.struts.forms;

import org.apache.struts.action.ActionForm;


/**
 * ActionForms are placeholders for data that are displayed in or collected from a page in a
 * Struts based solution, whether the page actually contains a HTML form or not.
 *
 * This form represents dynamic data on the front page.
 */
public class FrontPageForm extends ActionForm {
    /**
	 * 
	 */
	private static final long serialVersionUID = 8437468192711639700L;
	private int id;
    private int page_id;
    private String text;

    public FrontPageForm() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setPage_id(int page_id) {
        this.page_id = page_id;
    }

    public int getPage_id() {
        return page_id;
    }

    public String getText() {
        return text;
    }


    public void setText(String text) {
        this.text = text;
    }
}
