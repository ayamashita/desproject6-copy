/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import java.util.Collection;
import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.PeoplesBean;
import no.simula.des.service.PeopleService;
import no.simula.des.service.factory.ServiceFactory;
import no.simula.des.struts.forms.StudyForm;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Adds a responsible (person) to a study
 */
public class AddResponsibleAction extends DesAction {

    public int getAccessLevel() {
        super.min_access_level = 1;

        return min_access_level;
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        StudyForm stdForm = (StudyForm) form;

        PeopleService pdao = ServiceFactory.getPeopleService();
        PeoplesBean peoples = pdao.getPeoples();
        Collection responsibles = stdForm.getResponsibles();
        Iterator iter = responsibles.iterator();

        while (iter.hasNext()) {
            PeopleBean responsible = (PeopleBean) iter.next();
            Iterator iter2 = peoples.getPeoples().iterator();

            while (iter2.hasNext()) {
                PeopleBean poeple = (PeopleBean) iter2.next();

                if (poeple.getId() == responsible.getId()) {
                    if(!responsible.getDeleted()){
                      poeple.setAdded(true);
                    }
                }
            }
        }

        request.getSession().setAttribute("peoples", peoples);

        return (mapping.findForward("success"));
    }
}
