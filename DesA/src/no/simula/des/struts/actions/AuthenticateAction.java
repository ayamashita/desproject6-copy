/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.data.beans.PeopleBean;
import no.simula.des.service.PeopleService;
import no.simula.des.service.factory.ServiceFactory;
import no.simula.des.struts.forms.LogonForm;
import no.simula.des.util.Constants;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Authenticates the user
 */
public class AuthenticateAction extends Action {
    private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        log.debug("LogonForm: " + form.toString());

        LogonForm loForm = (LogonForm) form;
        PeopleService pdb = ServiceFactory.getPeopleService();
        PeopleBean user = pdb.authentiateUser(loForm.getUsername(),
                loForm.getPassword());

        if (user != null) {
            log.debug("User privileges is: " + user.getPrivilege());
            request.getSession().setAttribute("user", user);

            return (mapping.findForward("success"));
        } else {
            ActionErrors errors = new ActionErrors();

            //ActionMessage msg = new ActionMessage( "des.logon.userNotAuthenticated", "Bruker er ikke autentifisert" );
            errors.add("username",
                new org.apache.struts.action.ActionError(
                    "des.logon.userNotAuthenticated", "NotAutenticated"));
            this.saveErrors(request, errors);

            //throw new org.apache.struts.action.ActionException();
            return (mapping.getInputForward());
        }
    }
}
