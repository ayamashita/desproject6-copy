/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.data.beans.PeopleBean;
import no.simula.des.service.factory.ServiceFactory;
import no.simula.des.struts.forms.ReportForm;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class ShowReportsAction extends DesAction {
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(ShowReportsAction.class);

	public int getAccessLevel() {
		LOG.debug("ShowReportsAction.getAccessLevel()");
		return 1;
	}

	public ActionForward executeAuthenticated(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		LOG.debug("ShowReportsAction.executeAuthenticated()");
		
		ReportForm reportForm = (ReportForm) form;
		
		PeopleBean loggedUser = getLoggedUser(request);
		List reports = ServiceFactory.getReportService().LoadReportsForUser(loggedUser);
		reportForm.setReports(reports);
		return mapping.findForward("success");
	}
	
}
