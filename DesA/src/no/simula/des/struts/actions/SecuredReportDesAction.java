package no.simula.des.struts.actions;

import javax.servlet.http.HttpServletRequest;

import no.simula.des.data.beans.PeopleBean;
import no.simula.des.struts.forms.ReportForm;

import org.apache.struts.action.ActionForm;


/**
 * @author mateusz
 *
 */
public abstract class SecuredReportDesAction extends SecuredDesAction {
	

	protected boolean checkPermissions(ActionForm form,
			HttpServletRequest request) {

		ReportForm reportForm = (ReportForm) form;
		PeopleBean peopleBean = getLoggedUser(request);
		
		if (reportForm.getCurrentReport() != null) {
			return reportForm.getCurrentReport().hasPermision(peopleBean);
		}
		return false;
	}


}
