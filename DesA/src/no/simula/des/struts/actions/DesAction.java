/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.data.beans.AdminPrivilegesBean;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.service.factory.ServiceFactory;
import no.simula.des.tools.PropertiesHelper;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * This abstract class is used as a super class for all Struts actions that
 * may need authentication. Hence all authentication mechanisms is located here.
 */
public abstract class DesAction extends Action {
	
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(DesAction.class);
	

    //Set this vatiable i the subclass to controll access level
    protected int min_access_level = 2;

    protected PeopleBean getLoggedUser(HttpServletRequest request) {
    	
    	PeopleBean bean = (PeopleBean) request.getSession().getAttribute("user");
    	
    	if (PropertiesHelper.isSecurityDisabled() && bean == null) {
    		bean = ServiceFactory.getPeopleService().getPeople("aiko");
    		bean.setPrivilege(AdminPrivilegesBean.DBA_PRIVILEGE);
    		request.getSession().setAttribute("user", bean);
    	}
    	
    	return bean;
    }
    

    
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        
    	PeopleBean user = getLoggedUser(request);

        LOG.debug("The user is: " + user);

        boolean isSecurityDisabled = PropertiesHelper.isSecurityDisabled();

        LOG.debug("Security disabeled: " + isSecurityDisabled);

        if (isSecurityDisabled) {
        	return executeAuthenticated(mapping, form, request, response);
        }
        
        if (user != null) {
           	LOG.debug(user.getPrivilege() + " <-- user.getPrivilege()");
           	LOG.debug(getAccessLevel() + " <-- getAccessLevel()");
            if (user.getPrivilege() >= getAccessLevel()) {
                return executeAuthenticated(mapping, form, request, response);
            } else {
                request.setAttribute("AccessLevel",
                    new Integer(getAccessLevel()));

                return (mapping.findForward("notAuthenticated"));
            }
        }

        return (mapping.findForward("logon"));

        //failure: login screen / frontpage?
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        return (mapping.findForward("logon"));
    }

    public abstract int getAccessLevel();
}
