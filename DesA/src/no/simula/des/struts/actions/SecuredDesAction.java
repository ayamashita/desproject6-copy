package no.simula.des.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public abstract class SecuredDesAction extends DesAction {
	
	public ActionForward executeAuthenticated(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		if (checkPermissions(form, request)) {
			return super.executeAuthenticated(mapping, form, request, response);
		}
		return mapping.findForward("faliture"); 
	}
	
	protected abstract boolean checkPermissions(ActionForm form, HttpServletRequest request);

}
