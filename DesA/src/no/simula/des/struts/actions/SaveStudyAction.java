/*
 * Created on 03.okt.2003
 *
 */
package no.simula.des.struts.actions;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.beans.DateBean;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.StudyBean;
import no.simula.des.data.dao.StudyDao;
import no.simula.des.data.dao.impl.JdbcStudyDao;
import no.simula.des.struts.forms.StudyForm;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 * This action saves a new / edited study to the database
 */
public class SaveStudyAction extends DesAction {
	/**
	 * Logging output for this class.
	 */
	// private static final Log LOG = LogFactory.getLog(Constants.GLOBAL_LOG);
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(SaveStudyAction.class);

	public int getAccessLevel() {
		super.min_access_level = 1;

		return min_access_level;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.struts.action.Action#execute(org.apache.struts.action.ActionMapping,
	 *      org.apache.struts.action.ActionForm,
	 *      javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	public ActionForward executeAuthenticated(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		LOG.debug("SaveStudyAction.executeAuthenticated()");

		StudyForm studyForm = (StudyForm) form;
		StudyBean bean = new StudyBean();

		if (LOG.isDebugEnabled()) {
			LOG.debug("from form: " + studyForm.getName());
			LOG.debug("Operation: " + studyForm.getOperation());
		}

		if (studyForm.getOperation() == StudyForm.RELOAD_STUDY_OPERATION) {
			LOG.debug("Reoloading study, Do not save");
			return new ActionForward("/studyForm.jsp");
		}

		try {
			populateStudyBean(studyForm, bean);

			LOG.debug(bean.getType() + " <-- bean.getType()");

			// Get editor id from session object
			PeopleBean user = (PeopleBean) request.getSession().getAttribute(
					"user");
			bean.setEditorId(user.getId());
		} catch (Exception e) {
			LOG.error("Error populating study bean from study form", e);
		}

		if (LOG.isDebugEnabled()) {
			LOG.debug("StudyForm: " + studyForm.toString());
		}

		// Check action / id to check wether new or edited study
		StudyDao db = JdbcStudyDao.getInstance();
		int studyId = bean.getId();

		try {
			if (studyForm.getAction() == StudyForm.EDIT_STUDY_ACTION) {
				LOG.debug(bean.getType() + " <-- bean.getType()");
				db.updateStudy(bean);
				LOG.debug(bean.getType() + " <-- bean.getType()");
				studyForm.setOperation(StudyForm.RELOAD_STUDY_OPERATION);
				request.setAttribute("msgKey", "no.simula.des.updated");
			} else {
				studyId = db.insertStudy(bean);
				studyForm.setOperation(StudyForm.RELOAD_STUDY_OPERATION);
				LOG.debug("Id of new study: " + studyId);
				request.setAttribute("msgKey", "no.simula.des.inserted");
			}
		} catch (Exception e) {
			LOG.error("Error writing study to database", e);

			return mapping.findForward("failure");
		}

		request.setAttribute("study", new Integer(studyId));

		return new ActionForward("/showStudy.do?study=" + studyId, false);

		// return mapping.findForward("success");
		// return (new ActionForward("/editStudy.do?study="+studyId, true) );
	}

	/**
	 * Populates the StudyBean with posted data
	 * 
	 * @param studyForm
	 *            the form object (source)
	 * @param study
	 *            the data bean (target)
	 */
	void populateStudyBean(StudyForm form, StudyBean bean) throws Exception {
		// TODO handle null values in optional fields
		// Required fields
		// Id is not set for new studies
		bean.setId(Integer.parseInt(form.getId()));
		bean.setName(form.getName());
		bean.setDescription(form.getDescription());

		// TODO should type be type name or value?
		LOG.debug(form.getTypeId() + " <-- form.getTypeId()");
		bean.setType(form.getTypeId());
		LOG.debug(form.getTypeId() + " <-- form.getTypeId()");

		// Move date input explicitly. Assume data is already validated
		// End date is required
		int day = Integer.parseInt(form.getEndDay());
		int month = Integer.parseInt(form.getEndMonth());
		int year = Integer.parseInt(form.getEndYear());
		DateBean endDate = new DateBean(year, month, day);

		if (LOG.isDebugEnabled()) {
			LOG.debug("endDate = " + endDate.toString());
		}

		bean.setEndDate(endDate.getDate());

		// Start date is optional
		if (!form.getStartDay().equals("") && !form.getStartMonth().equals("")
				&& !form.getStartYear().equals("")) {
			day = Integer.parseInt(form.getStartDay());
			month = Integer.parseInt(form.getStartMonth());
			year = Integer.parseInt(form.getStartYear());

			DateBean startDate = new DateBean(year, month, day);

			if (LOG.isDebugEnabled()) {
				LOG.debug("startDate = " + startDate.toString());
			}

			bean.setStartDate(startDate.getDate());
		}

		// Optional fields
		bean.setKeywords(form.getKeywords());
		bean.setNotes(form.getNotes());

		if (!form.getStudents().equals("")) {
			bean.setStudents(Integer.parseInt(form.getStudents()));
		}

		if (!form.getProfessionals().equals("")) {
			bean.setProfessionals(Integer.parseInt(form.getProfessionals()));
		}

		if (!form.getDuration().equals("")) {
			bean.setDuration(Integer.parseInt(form.getDuration()));
		}

		bean.setDurationUnit(Integer.parseInt(form.getDurationUnit()));

		// Output only
		bean.setCreatedDate(form.getCreatedDate());
		bean.setCreatedBy(form.getCreatedBy());
		bean.setEditedDate(form.getEditedDate());
		bean.setEditedBy(form.getEditedBy());

		// TODO Include collections
		bean.setPublications(form.getPublications());
		bean.setResponsibles(form.getResponsibles());

		bean.setStudyMaterial((ArrayList) form.getStudyMaterial());
	}
}
