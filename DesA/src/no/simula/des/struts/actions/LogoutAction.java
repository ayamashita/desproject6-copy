/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * Logs a user out of the DES site
 */
public class LogoutAction extends Action {
    //private Log log = LogFactory.getLog(Constants.GLOBAL_LOG);

    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        request.getSession().removeAttribute("user");
        request.getSession().invalidate();

        return (mapping.findForward("success"));
    }
}
