/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.struts.actions;

import java.io.ByteArrayOutputStream;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.des.data.beans.ReportBean;
import no.simula.des.data.beans.ReportColumnBean;
import no.simula.des.data.beans.ReportParameterBean;
import no.simula.des.data.beans.ReportRowBean;
import no.simula.des.service.factory.ServiceFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.web.bind.ServletRequestUtils;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class ShowReportPdfAction extends SecuredReportDesAction {
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(ShowReportPdfAction.class);

	public int getAccessLevel() {
		LOG.debug("ShowReportsAction.getAccessLevel()");
		return 1;
	}

	public ActionForward executeAuthenticated(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		int reportId = ServletRequestUtils.getIntParameter(request, "report",
				-1);
		if (reportId > 0) {
			List report = ServiceFactory.getReportService().executeReport(
					reportId);
			Document document = new Document();

			ByteArrayOutputStream pdfStream = new ByteArrayOutputStream();
			PdfWriter.getInstance(document, pdfStream);
			
			document.open();
			
			ReportBean reportBean = ServiceFactory.getReportService().loadReportWithParameters(reportId);
			List columnNames = reportBean.getColumnNames();
			
			PdfPTable table = new PdfPTable(columnNames.size());

			for (Iterator iterator = columnNames.iterator(); iterator.hasNext();) {
				ReportParameterBean parameterBean = (ReportParameterBean) iterator.next();
				table.addCell(parameterBean.getValue());
			}
			
			for (Iterator iterator = report.iterator(); iterator.hasNext();) {
				ReportRowBean bean = (ReportRowBean) iterator.next();
				List columns = bean.getColumns();
				for (Iterator iterator2 = columns.iterator(); iterator2
						.hasNext();) {
					ReportColumnBean column = (ReportColumnBean) iterator2.next();
					table.addCell(column.getNames());
				}
			}
			document.add(table);
			document.close();

			response.setContentType("application/pdf");
			response.setContentLength(pdfStream.size());

			ServletOutputStream sos = response.getOutputStream();

			sos.write(pdfStream.toByteArray());
			// pdfStream.writeTo(sos);

			sos.flush();
			return null;
		}
		return mapping.findForward("faliture");
	}

}
