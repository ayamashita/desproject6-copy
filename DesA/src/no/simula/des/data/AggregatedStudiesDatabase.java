package no.simula.des.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Hashtable;
import java.util.Iterator;

import no.simula.des.beans.NameValueBean;
import no.simula.des.data.beans.StudyBean;
import no.simula.des.data.dao.StudyDao;
import no.simula.des.data.dao.impl.JdbcStudyDao;


/**
 * Loads data necessary to render the aggregated study graph from the database. Uses
 * a StudyDatabase object to retrieve the data.
 *
 * @see JdbcStudyDao
 */
public class AggregatedStudiesDatabase extends DataObject {
    private String[] types;
    private String[] years;
    private Hashtable data = new Hashtable();

    /**
     * Populates the aggregated study information.
     * This methode does so by calling StudyDatabase classes.
     */
    public void populateAggregatesInformation() throws Exception {
        StudyDao studyDb = JdbcStudyDao.getInstance();
        this.getStudyTypes(studyDb);

        ArrayList studies = (ArrayList) studyDb.getAllStudies();
        setData(studies);
    }

    public Hashtable getData() {
        return this.data;
    }

    public String[] getYears() {
        int size = data.keySet().size();
        years = new String[size];

        Iterator it = data.keySet().iterator();
        int i = 0;

        while (it.hasNext()) {
            this.years[i] = ((Integer) it.next()).toString();
            i++;
        }

        java.util.Arrays.sort(years);

        return years;
    }

    public String[] getStudyTypes() {
        return this.types;
    }

    //-------------- Private helper methods
    private void getStudyTypes(StudyDao studyDb) throws Exception {
        ArrayList typesArr = studyDb.getStudyTypes();
        int size = typesArr.size();
        types = new String[size];

        Iterator it = typesArr.iterator();
        int i = 0;

        while (it.hasNext()) {
            NameValueBean bean = (NameValueBean) it.next();
            types[i++] = bean.getName();
        }
    }

    private void setData(ArrayList studies) {
        Iterator it = studies.iterator();
        Calendar cal = Calendar.getInstance();

        while (it.hasNext()) {
            StudyBean study = (StudyBean) it.next();

            if (study.getEndDate() == null) {
                continue;
            }

            cal.setTime(study.getEndDate());

            int endYear = cal.get(Calendar.YEAR);
            String studyType = study.getType();

            if (studyType == null) {
                continue;
            }

            if (data.containsKey(new Integer(endYear))) {
                Hashtable values = (Hashtable) data.get(new Integer(endYear));
                Integer count = (Integer) values.get(studyType);
                values.put(studyType, new Integer(count.intValue() + 1));
                data.put(new Integer(endYear), values);
            } else {
                int size = types.length;
                Hashtable values = new Hashtable();

                for (int i = 0; i < size; i++) {
                    values.put(types[i], new Integer(0));
                }

                values.put(studyType, new Integer(1));
                data.put(new Integer(endYear), values);
            }
        }
    }
}
