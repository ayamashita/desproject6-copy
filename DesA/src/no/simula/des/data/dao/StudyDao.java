package no.simula.des.data.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;

import no.simula.des.beans.StudySortBean;
import no.simula.des.data.beans.StudiesBean;
import no.simula.des.data.beans.StudyBean;

public interface StudyDao {

	/**
	 * Retrieves a single study from the database
	 *
	 * @param id the id of the study to retrieve
	 * @return the StudyBean object representing the study
	 * @throws Exception
	 */
	public abstract StudyBean getStudy(int id, Connection conn)
			throws Exception;

	/**
	 * Retrieves study data for a single study
	 *
	 * @param id identifies the study
	 * @return a StudyBean containing the study data
	 * @throws Exception
	 */
	public abstract StudyBean getStudy(int id) throws Exception;

	/**
	 * Retrieves a number of studies from the study table
	 *
	 * @param start row number to start on
	 * @param count number of studies to retrieve
	 * @return a Collection of StudyBean objects
	 */
	public abstract StudiesBean getStudies(StudySortBean sortBean)
			throws Exception;

	/**
	 * Performs an advanced search
	 *
	 * @param StudySortBean containg the sort and search information
	 * @return a StudiesBean object containing all the studies that matched the query
	 */
	public abstract StudiesBean advancedSearchStudies(StudySortBean sortBean)
			throws Exception;

	/**
	 * Updates an existing study
	 *
	 * @param bean contains all data related to the study
	 * @throws Exception
	 */
	public abstract void updateStudy(StudyBean bean) throws Exception;

	/**
	 * Creates a new study entry
	 *
	 * @param bean contains all data related to the study
	 * @return the ID of the inserted study
	 */
	public abstract int insertStudy(StudyBean bean) throws Exception;

	/**
	 * Retrieves study type ids and names from the database
	 *
	 * @param rs query results
	 * @return an ArrayList containing the found study types
	 * @throws SQLException
	 */
	public abstract ArrayList getStudyTypes() throws Exception;

	/**
	 * Retrieves duration unit ids and names from the database
	 *
	 * @param rs query results
	 * @return an ArrayList containing the found duration units
	 * @throws SQLException
	 */
	public abstract ArrayList getDurationUnits() throws Exception;

	/**
	 * Deletes a single study
	 *
	 * @param studyId the study to delete
	 * @throws Exception
	 */
	public abstract void deleteStudy(int studyId) throws Exception;

	/**
	 * Retrieves all registered studies
	 *
	 * @return a Collection containing StudyBean objects
	 * @throws Exception
	 *
	 * @see no.simula.des.data.beans.StudyBean
	 */
	public abstract Collection getAllStudies() throws Exception;

	public abstract boolean isStudyNameValid(String studyName, int id)
			throws Exception;

	public abstract ArrayList getStudyEndDates() throws Exception;

}