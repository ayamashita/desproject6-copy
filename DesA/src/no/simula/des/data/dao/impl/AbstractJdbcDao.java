package no.simula.des.data.dao.impl;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

public class AbstractJdbcDao {

	private JdbcTemplate jdbcTemplate;
	private static final String DB_JNDI = "java:comp/env/jdbc/desdb";

	protected AbstractJdbcDao() {
	}

	protected JdbcTemplate getJdbcTemplate() {
		if (jdbcTemplate == null) {

			try {
				Context ctx = new InitialContext();
				DataSource ds = (DataSource) ctx.lookup(DB_JNDI);
				jdbcTemplate = new JdbcTemplate(ds);
			} catch (NamingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return jdbcTemplate;
	}

}
