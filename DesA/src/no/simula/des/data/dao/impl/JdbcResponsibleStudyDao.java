package no.simula.des.data.dao.impl;

import java.util.Collection;

import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.dao.ResponsibleStudyDao;

public class JdbcResponsibleStudyDao extends AbstractJdbcDao implements
		ResponsibleStudyDao {

	private final static JdbcResponsibleStudyDao instance = new JdbcResponsibleStudyDao();
	private static final String GET_RESPONSIBLES_SQL = "SELECT responsible_id FROM responsible_study WHERE study_id = ?";
	private static final String DELETE_RESPONSIBLES_SQL = "DELETE FROM responsible_study WHERE study_id = ? AND responsible_id = ?";
	private static final String INSERT_RESPONSIBLES_SQL = "INSERT IGNORE INTO responsible_study (study_id, responsible_id) VALUES (?, ?)";

	
	public static JdbcResponsibleStudyDao getInstance() {
        return instance;
    }
	
	public void createResponsibleStudy(PeopleBean peopleBean, int studyId) {
		getJdbcTemplate().update(INSERT_RESPONSIBLES_SQL, new Object[] {new Integer(studyId), peopleBean.getId()});
	}

	public void deleteResponsibleStudy(PeopleBean peopleBean, int studyId) {
		getJdbcTemplate().update(DELETE_RESPONSIBLES_SQL, new Object[] {new Integer(studyId), peopleBean.getId()});
	}

	public Collection getResponsibles(int studyId) {
		return getJdbcTemplate().queryForList(GET_RESPONSIBLES_SQL, new Object[] {new Integer(studyId)}, String.class);
	}

}
