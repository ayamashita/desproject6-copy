package no.simula.des.data.dao.impl;

import java.util.Collection;

import no.simula.des.data.dao.PublicationStudyDao;

public class JdbcPublicationStudyDao extends AbstractJdbcDao implements
		PublicationStudyDao {

	private static final String GET_PUBLICATIONS_SQL = "SELECT publication_id FROM publication_study WHERE study_id = ?";
	private static final String DELETE_PUBLICATIONS_SQL = "DELETE FROM publication_study WHERE study_id = ? AND publication_id = ?";
	private static final String INSERT_PUBLICATIONS_SQL = "INSERT IGNORE INTO publication_study (study_id, publication_id) VALUES (?, ?)";

	private final static JdbcPublicationStudyDao instance = new JdbcPublicationStudyDao();
	public static JdbcPublicationStudyDao getInstance() {
        return instance;
    }
	
	public void createPublication(int studyId, String publicationId) {
		getJdbcTemplate().update(INSERT_PUBLICATIONS_SQL, new Object[] {new Integer(studyId), publicationId});
	}

	public void deletePublication(int studyId, String publicationId) {
		getJdbcTemplate().update(DELETE_PUBLICATIONS_SQL, new Object[] {new Integer(studyId), publicationId});
	}

	public Collection getPublicationIds(int studyId) {
		return getJdbcTemplate().queryForList(GET_PUBLICATIONS_SQL, new Object[] {new Integer(studyId)}, String.class);
	}

}
