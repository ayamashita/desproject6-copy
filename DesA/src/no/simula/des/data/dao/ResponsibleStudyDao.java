package no.simula.des.data.dao;

import java.util.Collection;

import no.simula.des.data.beans.PeopleBean;

public interface ResponsibleStudyDao {

	public void createResponsibleStudy(PeopleBean peopleBean, int studyId);
	public void deleteResponsibleStudy(PeopleBean peopleBean, int studyId);
	/**
	 * @param studyId
	 * @return collection of {@link PeopleBean} ids
	 */
	public Collection getResponsibles(int studyId);
}
