package no.simula.des.data.dao;

import java.util.List;

import no.simula.des.data.beans.ReportParameterBean;

/**
 * @author mateusz
 *
 */
public interface ReportParameterDao {

	/**
	 * @param parameterId
	 */
	void deleteReportParameter(int parameterId);
	
	/**
	 * @param reportParameterBean
	 */
	void createReportParameter(ReportParameterBean reportParameterBean);
	
	/**
	 * @param reportParameterBean
	 */
	void updateReportParameter(ReportParameterBean reportParameterBean);
	
	/**
	 * @param parameterId
	 * @return
	 */
	ReportParameterBean loadReportParameter(int parameterId);
	
	/**
	 * @param reportId
	 * @return
	 */
	List loadReportParametersForReport(int reportId);
	
	/**
	 * @param reportId
	 */
	void deleteReportParameters(int reportId);
	
}
