package no.simula.des.data.dao;

import java.util.Collection;

/**
 * @author mateusz
 *
 */
public interface PublicationStudyDao {

	/**
	 * @param studyId
	 * @param publicationId
	 */
	void deletePublication(int studyId, String publicationId);
	
	/**
	 * @param studyId
	 * @param publicationId
	 */
	void createPublication(int studyId, String publicationId);
	
	/**
	 * @param studyId
	 * @return
	 */
	Collection getPublicationIds(int studyId);
	
}
