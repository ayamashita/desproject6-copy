package no.simula.des.data.beans;

import java.io.Serializable;

public abstract class AbstractBean implements Serializable {

	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final AbstractBean other = (AbstractBean) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public boolean isPersistent() {
		return id > 0;
	}
	
}
