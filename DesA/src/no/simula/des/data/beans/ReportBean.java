package no.simula.des.data.beans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import no.simula.des.struts.forms.ReportForm;


/**
 * @author mateusz
 *
 */
public class ReportBean extends AbstractNamedBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2811050599850769457L;
	
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(ReportBean.class);
	
	private String ownerId;
	private List parameters = new ArrayList();
	
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public List getParameters() {
		return parameters;
	}
	public void setParameters(List parameters) {
		this.parameters = parameters;
	}

	public boolean hasPermision(PeopleBean bean) {
		if (bean != null) {
			return bean.getPrivilege() == AdminPrivilegesBean.DBA_PRIVILEGE || bean.getId().equals(ownerId);
		}
		return false;
	}

	private List getParameters(String type) {
		List out = new ArrayList();
		for (Iterator iterator = parameters.iterator(); iterator.hasNext();) {
			ReportParameterBean reportParameterBean = (ReportParameterBean) iterator.next();
			if (reportParameterBean.getType().equals(type)) {
				out.add(reportParameterBean);
			}
		}
		return out;
	}
	
	private List getParameters(String type1, String type2) {
		List out = new ArrayList();
		for (Iterator iterator = parameters.iterator(); iterator.hasNext();) {
			ReportParameterBean reportParameterBean = (ReportParameterBean) iterator.next();
			if (reportParameterBean.getType().equals(type1) || reportParameterBean.getType().equals(type2)) {
				out.add(reportParameterBean);
			}
		}
		return out;
	}	
	
	public Object[] getSqlQueryParameters() {
		List responsibles = getParameters(ReportParameterBean.TYPE_AND, ReportParameterBean.TYPE_OR);
		Object[] out = new Object[responsibles.size()];
		int i = 0;
		for (Iterator iterator = responsibles.iterator(); iterator.hasNext();) {
			ReportParameterBean bean = (ReportParameterBean) iterator.next();
			out[i] = bean.getValue();
			LOG.debug(bean.getValue() + " <-- bean.getValue()");
			i++;
		}
		return out;
	}
	
	public String getSqlQuery() {
		StringBuffer buffer = new StringBuffer("SELECT study_id, ");
		StringBuffer selectFields = new StringBuffer();
		List selectParams = getParameters(ReportParameterBean.TYPE_SELECT);
		for (Iterator iterator = selectParams.iterator(); iterator.hasNext();) {
			ReportParameterBean bean = (ReportParameterBean) iterator.next();
			String value = bean.getValue();
			if (value.startsWith(ReportForm.JOIN_FIELD_PREFIX)) {
				selectFields.append("\'" + bean.getValue() + "\',");
			} else {
				selectFields.append(bean.getValue() + ",");
			}
			
		}
		selectFields.deleteCharAt(selectFields.length()-1);
		
		LOG.debug(selectFields + " <-- selectFields");
		
		buffer.append(selectFields);
		buffer.append(" FROM study ");

		List responsibles = getParameters(ReportParameterBean.TYPE_AND, ReportParameterBean.TYPE_OR);
		boolean first = true;
		LOG.debug(responsibles.size() + " <-- responsibles.size()");
		for (Iterator iterator = responsibles.iterator(); iterator.hasNext();) {
			ReportParameterBean bean = (ReportParameterBean) iterator.next();
			if (first) {
				first = false;
				buffer.append("WHERE ");
			} else {
				
				buffer.append(" " + bean.getType() + " ");
			}
			buffer.append("study_id IN (SELECT study_id FROM responsible_study WHERE responsible_id = ?)");
		}

		buffer.append(" ORDER BY ");
		buffer.append("study_type ");
		
		List orderConditions = getParameters(ReportParameterBean.TYPE_ORDER);
		for (Iterator iterator = orderConditions.iterator(); iterator.hasNext();) {
			ReportParameterBean bean = (ReportParameterBean) iterator.next();
			String value = bean.getValue();
			if ("ASC".equals(value) || "DESC".equals(value)) {
				buffer.append(value);
			}
		}
		
		String out = buffer.toString();
		LOG.debug(out + " <-- out");
		
		return out;
	}

	public List getColumnNames() {
		return getParameters(ReportParameterBean.TYPE_SELECT);
	}
	
}
