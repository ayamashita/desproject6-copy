package no.simula.des.data.beans;


/**
 * Represents the (front) page content
 */
public class ContentBean {
    private int id;
    private int page_id;
    private String text;

    public ContentBean() {
    }

    public int getId() {
        return id;
    }

    public int getPage_id() {
        return page_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setPage_id(int page_id) {
        this.page_id = page_id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
