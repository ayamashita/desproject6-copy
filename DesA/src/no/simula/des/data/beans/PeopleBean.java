/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.data.beans;



/**
 * Represents a single person
 */
/**
 * @author mateusz
 *
 */
public class PeopleBean {
    private String id;
    private String first_name;
    private String family_name;
    private String position;
    private String homeSiteUrl;
    private int privilege = 0;

    //Utility attributes
    private boolean isAdded;

    //Utility attributes
    private boolean isDeleted;

    public String getFamily_name() {
        return family_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getId() {
        return id;
    }

    public String getIdAsString() {
        return id;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public void setId(String id) {
        this.id = id;
    }

    /**
     * @param id
     * @deprecated use setId(String id)
     */
    public void setId(int id) {
        this.id = new Integer(id).toString();
    }
    
    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public int getPrivilege() {
        return privilege;
    }

    public void setPrivilege(int privilege) {
        this.privilege = privilege;
    }

    public boolean getAdded() {
        return isAdded;
    }

    public boolean getDeleted() {
        return isDeleted;
    }

    public void setAdded(boolean b) {
        isAdded = b;
    }

    public void setDeleted(boolean b) {
        isDeleted = b;
    }

	public String getHomeSiteUrl() {
		return homeSiteUrl;
	}

	public void setHomeSiteUrl(String homeSiteUrl) {
		this.homeSiteUrl = homeSiteUrl;
	}
	
	public String getFullName() {
		return first_name + " " + family_name;
	}
	
}
