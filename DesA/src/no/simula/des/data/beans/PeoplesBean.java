package no.simula.des.data.beans;

import java.util.ArrayList;
import java.util.Iterator;

import no.simula.des.data.AdminPrivilegesDatabase;


/**
 * Represents a list of people
 */
public class PeoplesBean {
	
	//private static transient final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger(PeoplesBean.class);
	
    private ArrayList peoples = new ArrayList();

    public ArrayList getPeoples() {
        return peoples;
    }

    public void setPeoples(ArrayList peoples) {
        this.peoples = peoples;
        try {
			initPrivileges();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }

    private void initPrivileges() throws Exception {
        AdminPrivilegesDatabase privilegesDB = new AdminPrivilegesDatabase();
        AdminPrivilegesBean privileges = privilegesDB.getAdminPrivileges();
        Iterator iter = peoples.iterator();

        while (iter.hasNext()) {
            PeopleBean people = (PeopleBean) iter.next();
            String id = people.getId();
            int privilege = privileges.getAdminPrivileges(id);

            if (!(privilege == AdminPrivilegesBean.GUEST_PRIVILEGE)) {
                people.setPrivilege(privilege);
            }
        }
    }
    
    /**
     * Sets (loads) the privilege level for all people in the list
     * @param privileges
     * @deprecated does nothig
     */
    public void setPrivileges(AdminPrivilegesBean privileges) {
/*    	try {
			initPrivileges();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
    }
}
