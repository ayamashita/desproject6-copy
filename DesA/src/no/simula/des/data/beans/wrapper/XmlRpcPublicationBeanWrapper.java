package no.simula.des.data.beans.wrapper;

import java.util.Map;

import no.simula.des.data.beans.PublicationBean;

public class XmlRpcPublicationBeanWrapper extends AbstractXmlRpcBeanWrapper {
	private PublicationBean publicationBean;
	
	public XmlRpcPublicationBeanWrapper(Map xmlRpcResults) {
		super(xmlRpcResults);
		publicationBean = new PublicationBean();
		/*
        bean.setId(rs.getInt(1));
        bean.setYear(rs.getInt(2));
        bean.setTitle(rs.getString(3));
        bean.setAbstact(rs.getString(4));
        boolean b = (rs.getString(5).equals("n")? true:false);
        bean.setExternal(b);
        String firstname = rs.getString(6);
        String lastname = rs.getString(7);
        if(b || (firstname == null && lastname == null ) ){
          firstname = (firstname == null? "": firstname );
          lastname = (lastname == null? "n/a": lastname);

          bean.setEditor(lastname);
        }
        else{
          bean.setEditor(lastname+","+firstname);
        }

		id=Simula.SE.159,
		abstract=It is well known that software development companies tend to produce over-optimistic cost estimates and that this over-optimism may lead to delivery problems for the clients as well as the providers. In this paper we summarize evidence suggesting that the clients can reduce the likelihood of selecting providers with bids based on over-optimistic cost estimates through their control of the bidding processes. Important means for this purpose include: avoid inviting many bidders when price is an important criterion for selection; avoid using price as an important criterion for selection when the ability to assess provider competence is low; apply bidding processes that ensure that the provider understands the complexity of the project; avoid budget or price expectation related information in the bidding material and avoid a negotiation process in which you ask for bid updates on reduced functionality. The evidence presented in this paper can also be used by software providers to identify bidding rounds where it is likely that they win only when strongly over-optimistic about the cost.
		authors=[Ljava.lang.Object;@1037c71,
		title=How%20to%20Avoid%20Selecting%20Providers%20with%20Bids%20Based%20on%20Over-Optimistic%20Cost%20Estimates,
		source=Accepted for publication in IEEE Software (May/June).
		year=2009
		hasPdf=true
		type=Article in International Journal
		url=http://simula.no:9080/simula/research/engineering/publications/Simula.SE.159
		creator=magnej
		*/
		publicationBean.setId(getString("id"));
		publicationBean.setYear(getInteger("year", null));
		publicationBean.setTitle(decode(getString("title")));
		publicationBean.setAbstact(getString("abstract"));
		publicationBean.setUrl(getString("url"));
		//TODO verify
		publicationBean.setExternal(true);
		// TODO verify,
		// maybe it one should see for creator's first and last name
		// or use author property
		publicationBean.setEditor(getString("creator"));
	}
	
	public PublicationBean getPublicationBean() {
		return publicationBean;
	}
}
