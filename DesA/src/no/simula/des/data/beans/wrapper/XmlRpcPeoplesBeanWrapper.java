package no.simula.des.data.beans.wrapper;

import java.util.ArrayList;
import java.util.Map;

import no.simula.des.data.beans.PeoplesBean;

public class XmlRpcPeoplesBeanWrapper {
	
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(XmlRpcPeoplesBeanWrapper.class);
	
	private PeoplesBean peoplesBean;
	
	public XmlRpcPeoplesBeanWrapper(Map[] xmlRpcResults) {
		if (xmlRpcResults == null) {
			throw new IllegalArgumentException("xmlRpcResults == null");
		}
		ArrayList peoples = new ArrayList(xmlRpcResults.length);
		for (int i = 0; i < xmlRpcResults.length; i++) {
			Map map = xmlRpcResults[i];
			LOG.debug(map + " <-- map");
			peoples.add(new XmlRpcPeopleBeanWrapper(map).getPeopleBean());
			
		}
		peoplesBean = new PeoplesBean();
		peoplesBean.setPeoples(peoples);
	}
	
	public PeoplesBean getPeoplesBean() {
		return peoplesBean;
	}
}
