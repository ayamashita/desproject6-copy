package no.simula.des.data.beans.wrapper;

import java.util.Map;

import no.simula.des.data.beans.PeopleBean;

public class XmlRpcPeopleBeanWrapper extends AbstractXmlRpcBeanWrapper {
	private PeopleBean peopleBean;
	
	public XmlRpcPeopleBeanWrapper(Map xmlRpcResults) {
		super(xmlRpcResults);
		peopleBean = new PeopleBean();
		/*
		id=andremcc,
		jobtitle=Adjunct Research Scientist, 
		email=andremcc@simula.no, 
		lastname=McCulloch, 
		firstname=Andrew, 
		url=http://simula.no:9080/simula/people/andremcc, 
		Title=Andrew McCulloch
		
		    people.setId(rs.getInt(1));
            people.setFirst_name(rs.getString(2));
            people.setFamily_name(rs.getString(3));
            people.setPosition(rs.getString(6));
		
		*/
		peopleBean.setId(getString("id"));
		peopleBean.setFirst_name(getString("firstname"));
		peopleBean.setFamily_name(getString("lastname"));
		peopleBean.setPosition(getString("jobtitle"));
		peopleBean.setHomeSiteUrl(getString("url"));
		//peopleBean.set
	}
	
	public PeopleBean getPeopleBean() {
		return peopleBean;
	}
}
