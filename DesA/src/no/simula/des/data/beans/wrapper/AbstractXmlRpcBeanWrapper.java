package no.simula.des.data.beans.wrapper;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

public abstract class AbstractXmlRpcBeanWrapper {

	private Map xmlRpcResults;
	private String defaultEncoding = "UTF-8";
	
	public AbstractXmlRpcBeanWrapper(Map xmlRpcResults) {
		this.xmlRpcResults = xmlRpcResults;
	}
	
	protected String decode(String s) {
		try {
			return URLDecoder.decode(s, defaultEncoding);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return s;
	}
	
	protected int getInt(String fieldName) {
		String s = getString(fieldName);
		if (StringUtils.isNotEmpty(s)) {
			return Integer.valueOf(s).intValue();
		}
		return -1;
		
	}
	
	protected Integer getInteger(String fieldName) {
		String s = getString(fieldName);
		if (StringUtils.isNotEmpty(s)) {
			return Integer.valueOf(s);
		}
		return null;
	}	

	protected Integer getInteger(String fieldName, Integer defaultValue) {
		try {
			return getInteger(fieldName);
		} catch (NumberFormatException e) {
			return defaultValue;
		}
	}	
	
	
	protected String getString(String fieldName) {
		return xmlRpcResults.get(fieldName).toString();
	}
	
}
