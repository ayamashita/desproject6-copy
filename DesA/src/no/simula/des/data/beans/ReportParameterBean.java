package no.simula.des.data.beans;

import org.apache.commons.lang.StringUtils;


/**
 * @author mateusz
 *
 */
public class ReportParameterBean extends AbstractBean {

	public static final String TYPE_AND = "AND";
	public static final String TYPE_OR = "OR";
	public static final String TYPE_ORDER = "DESC";
	public static final String TYPE_SELECT = "SELECT";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8476790658042637356L;
	
	private String value;
	private String type;
	private int index;
	private ReportBean reportBean;
	
	public ReportParameterBean() {
		
	}
	
public ReportParameterBean(String type) {
		this.type = type;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	public ReportBean getReportBean() {
		return reportBean;
	}
	public void setReportBean(ReportBean reportBean) {
		this.reportBean = reportBean;
	}

	public boolean isEmpty() {
		return StringUtils.isEmpty(value);
	}
	
}
