package no.simula.des.data.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import no.simula.des.data.beans.ReportParameterBean;

import org.springframework.jdbc.core.RowMapper;

/**
 * @author mateusz
 *
 */
public class ReportParameterBeanRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ReportParameterBean reportParameterBean = new ReportParameterBean();
		reportParameterBean.setId(rs.getInt("id"));
		reportParameterBean.setValue(rs.getString("value"));
		reportParameterBean.setType(rs.getString("type"));
		reportParameterBean.setIndex(rs.getInt("report_index"));
		return reportParameterBean;
	}

}
