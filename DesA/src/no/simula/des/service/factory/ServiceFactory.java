package no.simula.des.service.factory;

import no.simula.des.service.PeopleService;
import no.simula.des.service.PublicationService;
import no.simula.des.service.ReportService;
import no.simula.des.service.SearchService;
import no.simula.des.service.impl.DefaultReportService;
import no.simula.des.service.impl.DefaultSearchService;
import no.simula.des.service.impl.XmlRpcPeopleService;
import no.simula.des.service.impl.XmlRpcPublicationService;

public class ServiceFactory {

    //private final static PeopleService peopleService = JdbcPeopleService.getInstance();
    //private final static PublicationService publicationService = JdbcPublicationService.getInstance();
	private final static PeopleService peopleService = XmlRpcPeopleService.getInstance();
	private final static PublicationService publicationService = XmlRpcPublicationService.getInstance();
	private final static SearchService searchService = DefaultSearchService.getInstance();
	private final static ReportService reportService = DefaultReportService.getInstance();
	
    //�eby unikn�� automatycznego tworzenia domy�lnego, publicznego, bezargumentowego konstruktora
    private ServiceFactory() {
    }

	public static PeopleService getPeopleService() {
		return peopleService;
	}

	public static PublicationService getPublicationService() {
		return publicationService;
	}

	public static SearchService getSearchService() {
		return searchService;
	}

	public static ReportService getReportService() {
		return reportService;
	}


	
}
