package no.simula.des.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import no.simula.des.beans.NameValueBean;
import no.simula.des.data.beans.AdminPrivilegesBean;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.PublicationBean;
import no.simula.des.data.beans.ReportBean;
import no.simula.des.data.beans.ReportColumnBean;
import no.simula.des.data.beans.ReportParameterBean;
import no.simula.des.data.beans.ReportRowBean;
import no.simula.des.data.dao.impl.JdbcReportDao;
import no.simula.des.data.dao.impl.JdbcReportParameterDao;
import no.simula.des.service.ReportService;
import no.simula.des.service.factory.ServiceFactory;
import no.simula.des.struts.forms.ReportForm;
import no.simula.des.tools.StudyHelper;

public class DefaultReportService implements ReportService {

	
	private final static DefaultReportService instance = new DefaultReportService();
	private static final List PEOPLE_COLUMN_NAMES = Arrays.asList(new String[] {"study_edited_by", "study_created_by"});
	private static final List STUDY_COLUMN_NAMES = Arrays.asList(new String[] {"study_id"});

	
	public static DefaultReportService getInstance() {
		return instance;
	}

	private DefaultReportService() {
	}
	
	public List loadAllReportsOrderByName() {
		return JdbcReportDao.getInstance().loadAllReportsOrderByName();
	}

	public ReportBean loadReportWithParameters(int reportId) {
		ReportBean reportBean = JdbcReportDao.getInstance().loadReport(reportId);
		List parameters = JdbcReportParameterDao.getInstance().loadReportParametersForReport(reportId);
		for (Iterator iterator = parameters.iterator(); iterator.hasNext();) {
			ReportParameterBean parameterBean = (ReportParameterBean) iterator.next();
			parameterBean.setReportBean(reportBean);
		}
		reportBean.setParameters(parameters);
		return reportBean;
	}

	public List LoadReportsForUser(PeopleBean peopleBean) {
		if (peopleBean == null) {
			throw new IllegalArgumentException("peopleBean == null");
		}
		int privilege = peopleBean.getPrivilege();
		if (AdminPrivilegesBean.DBA_PRIVILEGE == privilege) {
			return loadAllReportsOrderByName();
		} else if (AdminPrivilegesBean.SA_PRIVILEGE == privilege) {
			return JdbcReportDao.getInstance().loadAllReportsWithGivenCreatorOrderByName(peopleBean.getId());
		}
		return null;
	}

	public void createReport(ReportBean reportBean) {
		JdbcReportDao.getInstance().createReport(reportBean);		
		String name = reportBean.getName();
		String ownerId = reportBean.getOwnerId();
		List params = reportBean.getParameters();
		reportBean = JdbcReportDao.getInstance().loadReport(name, ownerId);
		reportBean.setParameters(params);
		createParameters(reportBean);

	}

	private void createParameters(ReportBean bean) {
		for (Iterator iterator = bean.getParameters().iterator(); iterator.hasNext();) {
			ReportParameterBean reportParameterBean = (ReportParameterBean) iterator.next();
			reportParameterBean.setReportBean(bean);
			JdbcReportParameterDao.getInstance().createReportParameter(reportParameterBean);
		}
	}
	
	public void updateReport(ReportBean reportBean) {
		JdbcReportDao.getInstance().updateReport(reportBean);		
		JdbcReportParameterDao.getInstance().deleteReportParameters(reportBean.getId());
		createParameters(reportBean);
	}

	public void deleteReport(int reportId) {
		JdbcReportParameterDao.getInstance().deleteReportParameters(reportId);
		JdbcReportDao.getInstance().deleteReport(reportId);
	}

	public List executeReport(int reportId) throws Exception {
		ReportBean reportBean = loadReportWithParameters(reportId);
		String sql = reportBean.getSqlQuery();
		Object[] params = reportBean.getSqlQueryParameters();
		List reportRows = JdbcReportDao.getInstance().executeQuery(sql, params);
		
		List columnNames = reportBean.getColumnNames();
		int columnIndex = 1; // first column contains study ID


		for (Iterator iterator = columnNames.iterator(); iterator.hasNext();) {
			String columnName = ((ReportParameterBean) iterator.next()).getValue();
			if (PEOPLE_COLUMN_NAMES.contains(columnName)) {
				for (Iterator iterator2 = reportRows.iterator(); iterator2.hasNext();) {
					ReportRowBean reportRowBean = (ReportRowBean) iterator2.next();
					ReportColumnBean bean = (ReportColumnBean) reportRowBean.getColumns().get(columnIndex);
					PeopleBean peopleBean = ServiceFactory.getPeopleService().getPeople(bean.getValue());
					if (peopleBean != null) {
						bean.setUrl(peopleBean.getHomeSiteUrl());
					}
				}
			} else if (STUDY_COLUMN_NAMES.contains(columnName)) {
				for (Iterator iterator2 = reportRows.iterator(); iterator2.hasNext();) {
					ReportRowBean reportRowBean = (ReportRowBean) iterator2.next();
					ReportColumnBean bean = (ReportColumnBean) reportRowBean.getColumns().get(columnIndex);
					Integer id = Integer.valueOf(bean.getValue());
					bean.setUrl(StudyHelper.getUrl(id.intValue()));
				}
			} else if (ReportForm.JOIN_PUBLICATIONS_FILED_VALUE.equals(columnName)) {
				for (Iterator iterator2 = reportRows.iterator(); iterator2.hasNext();) {
					ReportRowBean reportRowBean = (ReportRowBean) iterator2.next();
					ReportColumnBean idBean = (ReportColumnBean) reportRowBean.getColumns().get(0);
					int studyId = Integer.valueOf(idBean.getValue()).intValue();
					ReportColumnBean columnBean = (ReportColumnBean) reportRowBean.getColumns().get(columnIndex);
					columnBean.setNamesAndUrlValues(new ArrayList());
					Collection publications = ServiceFactory.getPublicationService().getPublicationsForStudy(studyId);
					for (Iterator iterator3 = publications.iterator(); iterator3
							.hasNext();) {
						PublicationBean publicationBean = (PublicationBean) iterator3.next();
						NameValueBean nameValueBean = new NameValueBean(publicationBean.getTitle(), publicationBean.getUrl());
						columnBean.getNamesAndUrlValues().add(nameValueBean);
						
					}
				}
			} else if (ReportForm.JOIN_RESPONSIBLES_FILED_VALUE.equals(columnName)) {
				for (Iterator iterator2 = reportRows.iterator(); iterator2.hasNext();) {
					ReportRowBean reportRowBean = (ReportRowBean) iterator2.next();
					ReportColumnBean idBean = (ReportColumnBean) reportRowBean.getColumns().get(0);
					int studyId = Integer.valueOf(idBean.getValue()).intValue();
					ReportColumnBean columnBean = (ReportColumnBean) reportRowBean.getColumns().get(columnIndex);
					columnBean.setNamesAndUrlValues(new ArrayList());
					Collection responsibles = ServiceFactory.getPeopleService().getResponsiblesForStudy(studyId);
					for (Iterator iterator3 = responsibles.iterator(); iterator3
							.hasNext();) {
						PeopleBean peopleBean = (PeopleBean) iterator3.next();
						NameValueBean nameValueBean = new NameValueBean(peopleBean.getFullName(), peopleBean.getHomeSiteUrl());
						columnBean.getNamesAndUrlValues().add(nameValueBean);
					}
				}
			}
			columnIndex++;
		}
		
		// delete idBean
		for (Iterator iterator2 = reportRows.iterator(); iterator2.hasNext();) {
			ReportRowBean reportRowBean = (ReportRowBean) iterator2.next();
			reportRowBean.getColumns().remove(0);
		}
		
		return reportRows;
	}
	
}
