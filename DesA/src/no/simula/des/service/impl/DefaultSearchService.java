package no.simula.des.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import no.simula.des.beans.StudySortBean;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.StudiesBean;
import no.simula.des.data.beans.StudyBean;
import no.simula.des.data.dao.StudyDao;
import no.simula.des.data.dao.impl.JdbcStudyDao;
import no.simula.des.service.SearchService;
import no.simula.des.service.factory.ServiceFactory;

import org.apache.commons.lang.StringUtils;

public class DefaultSearchService implements SearchService {

	private final static DefaultSearchService instance = new DefaultSearchService();
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(DefaultSearchService.class);

	public static DefaultSearchService getInstance() {
		return instance;
	}

	private DefaultSearchService() {
	}

	public StudiesBean advancedSearchStudies(StudySortBean sortBean) {
		try {
			StudyDao list = JdbcStudyDao.getInstance();
			StudiesBean studies = list.advancedSearchStudies(sortBean);

			String firstName = sortBean.getResponsible_firstname();
			String familyName = sortBean.getResponsible_familyname();

			if (StringUtils.isEmpty(firstName) && StringUtils.isEmpty(familyName)) {
				return studies;
			}
			
			
			Collection studiesCollection = studies.getStudies();

			Pattern firstNamePattern = Pattern.compile(firstName);
			Pattern familyNamePattern = Pattern.compile(familyName);

			// Pattern firstNamePattern = new Pattern()

			Collection filteredStudiesCollection = new ArrayList();
			
			for (Iterator iterator = studiesCollection.iterator(); iterator.hasNext();) {
				StudyBean studyBean = (StudyBean) iterator.next();
				Collection responsibles = ServiceFactory.getPeopleService().getResponsiblesForStudy(studyBean.getId());
				LOG.debug(studyBean.getName() + " <-- studyBean.getName()");
				studyBean.setResponsibles(responsibles);
				Collection publications = ServiceFactory.getPublicationService().getPublicationsForStudy(studyBean.getId());
				studyBean.setPublications(publications);				
				if (responsibles == null) {
					LOG.warn("EVERY study should have a responsible");
					continue;
				}
				for (Iterator iterator2 = responsibles.iterator(); iterator2
						.hasNext();) {
					PeopleBean peopleBean = (PeopleBean) iterator2.next();
					if (peopleBean != null) {
						Matcher firstNameMatcher = firstNamePattern.matcher(peopleBean.getFirst_name());
						Matcher familyNameMatcher = familyNamePattern.matcher(peopleBean.getFamily_name());
						if ( (StringUtils.isEmpty(firstName) || firstNameMatcher.matches()) && (StringUtils.isEmpty(familyName) || familyNameMatcher.matches())) {
							filteredStudiesCollection.add(studyBean);
						}
					}
					
				}
				
			}
			studies.setStudies(filteredStudiesCollection);
			return studies;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

}
