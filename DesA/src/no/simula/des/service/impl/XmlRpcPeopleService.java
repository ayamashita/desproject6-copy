package no.simula.des.service.impl;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import no.simula.des.data.AdminPrivilegesDatabase;
import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.PeoplesBean;
import no.simula.des.data.beans.wrapper.XmlRpcPeopleBeanWrapper;
import no.simula.des.data.beans.wrapper.XmlRpcPeoplesBeanWrapper;
import no.simula.des.data.dao.impl.JdbcResponsibleStudyDao;
import no.simula.des.service.PeopleService;

import org.apache.xmlrpc.XmlRpcException;

public class XmlRpcPeopleService extends AbstractXmlRpcService implements
		PeopleService {

	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(XmlRpcPeopleService.class);

	private final static XmlRpcPeopleService instance = new XmlRpcPeopleService();

	public static XmlRpcPeopleService getInstance() {
		return instance;
	}

	public PeopleBean authentiateUser(String username, String password)
			throws Exception {
		boolean correctPassword = authenticate(username, password);
		if (correctPassword) {
			PeopleBean peopleBean = getPeople(username);
			AdminPrivilegesDatabase adminPrivilegeDb = new AdminPrivilegesDatabase();
			peopleBean.setPrivilege(adminPrivilegeDb.getPeoplePrivilege(username));
			return peopleBean;
		}
		return null;
	}

	public PeoplesBean getPeoples() throws Exception {
		HashMap params = new HashMap();
		try {
			Map[] out = executeMethod("getPeople", params);
			return new XmlRpcPeoplesBeanWrapper(out).getPeoplesBean();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Collection getResponsiblesForStudy(int studyId) throws Exception {
		LOG.debug("XmlRpcPeopleService.getResponsibleList()");
		Collection responsibleIds = JdbcResponsibleStudyDao.getInstance()
				.getResponsibles(studyId);
		Collection out = new ArrayList(responsibleIds.size());
		for (Iterator iterator = responsibleIds.iterator(); iterator.hasNext();) {
			String personId = (String) iterator.next();
			LOG.debug(personId + " <-- personId");
			out.add(getPeople(personId));
		}
		return out;
	}

	public void updateResponsibleStudyRelationship(int studyId,
			Collection responsibles) throws Exception {
		if (responsibles == null) {
			return;
		}
		Iterator iterator = responsibles.iterator();

		// Discover any changes
		while (iterator.hasNext()) {
			PeopleBean bean = (PeopleBean) iterator.next();

			if (bean.getDeleted()) {
				// Delete publication from study
				JdbcResponsibleStudyDao.getInstance().deleteResponsibleStudy(
						bean, studyId);
			} else if (bean.getAdded()) {
				// Add publication to study
				JdbcResponsibleStudyDao.getInstance().createResponsibleStudy(
						bean, studyId);
			}
		}
	}

	public PeopleBean getPeople(String id) {
		HashMap params = new HashMap();
		params.put("id", id);
		try {
			Map[] out = executeMethod("getPeople", params);
			if (out.length == 1) {
				return new XmlRpcPeopleBeanWrapper(out[0]).getPeopleBean();
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean authenticate(String username, String password) {
		LOG.debug(password + " <-- password");
		HashMap params = new HashMap();
		params.put("login", username);
		params.put("password", password);
		try {
			Object out = executeMethodForObject("check_credentials", params);
			return out != null && Boolean.valueOf(out.toString()).booleanValue();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

}
