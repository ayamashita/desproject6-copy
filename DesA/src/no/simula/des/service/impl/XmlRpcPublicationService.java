package no.simula.des.service.impl;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import no.simula.des.data.beans.PublicationBean;
import no.simula.des.data.beans.wrapper.XmlRpcPublicationBeanWrapper;
import no.simula.des.data.dao.impl.JdbcPublicationStudyDao;
import no.simula.des.service.PublicationService;

import org.apache.xmlrpc.XmlRpcException;

public class XmlRpcPublicationService extends AbstractXmlRpcService implements
		PublicationService {

    private final static XmlRpcPublicationService instance = new XmlRpcPublicationService();
    public static XmlRpcPublicationService getInstance() {
        return instance;
    }	
	
	public PublicationBean getPublication(String id) throws Exception {
		HashMap params = new HashMap();
		params.put("id", id);
		try {
			Map[] out = executeMethod("getPublications", params);
			if (out.length == 1) {
				return new XmlRpcPublicationBeanWrapper(out[0])
						.getPublicationBean();
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public Collection getPublicationsForStudy(int studyId) throws Exception {
		Collection ids = JdbcPublicationStudyDao.getInstance().getPublicationIds(studyId);
		Collection out = new ArrayList(ids.size());
		for (Iterator iterator = ids.iterator(); iterator.hasNext();) {
			String id = (String) iterator.next();
			out.add(getPublication(id));
		}
		return out;
	}

	public Collection getPublicationList() throws Exception {
		HashMap params = new HashMap();
		try {
			Map[] out = executeMethod("getPublications", params);
			Collection collection = new ArrayList(out.length);
			for (int i = 0; i < out.length; i++) {
				Map map = out[i];
				collection.add(new XmlRpcPublicationBeanWrapper(map)
						.getPublicationBean());
			}
			return collection;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void updatePublicationStudyRelationship(int studyId,
			Collection publications) throws Exception {
		if (publications == null) {
			return;
		}
		Iterator iterator = publications.iterator();

		// Discover any changes
		while (iterator.hasNext()) {
			PublicationBean bean = (PublicationBean) iterator.next();

			if (bean.getDeleted()) {
				// Delete publication from study
				JdbcPublicationStudyDao.getInstance().deletePublication(studyId, bean.getId());
			} else if (bean.getAdded()) {
				// Add publication to study
				JdbcPublicationStudyDao.getInstance().createPublication(studyId, bean.getId());
			}
		}

	}

}
