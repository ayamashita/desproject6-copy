package no.simula.des.service;

import no.simula.des.beans.StudySortBean;
import no.simula.des.data.beans.StudiesBean;

/**
 * @author mateusz
 *
 */
public interface SearchService {

	/**
	 * @return
	 */
	StudiesBean advancedSearchStudies(StudySortBean sortBean);
	
}
