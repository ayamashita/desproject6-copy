package no.simula.des.service;

import java.util.Collection;

import no.simula.des.data.beans.PeopleBean;
import no.simula.des.data.beans.PeoplesBean;

public interface PeopleService {

	/**
	 * The method adds and removed relationships between responsibles and studies at the
	 * database level.
	 *
	 * @param studyId identifies which study we are changing the relationships for.
	 * @param publications all responsibles related to the study, including any added or deleted
	 *                     ones.
	 * @throws Exception
	 */
	public abstract void updateResponsibleStudyRelationship(int studyId,
			Collection responsibles) throws Exception;

	/**
	 * Retrieves all people from the database
	 *
	 * @return a PeoplesBean object containing a list of all persons
	 * @throws Exception
	 */
	public abstract PeoplesBean getPeoples() throws Exception;

	/**
	 * Autheticates a user.
	 * Returns null if the user is not authenticated
	 *
	 * @param String username - (e-mail adress)
	 * @param String password - (in clear text)
	 * @return PeopleBean, null if user not authenticated
	 * @throws Exception if SQL error
	 */
	public abstract PeopleBean authentiateUser(String username, String password)
			throws Exception;

	/**
	 * Retrives all persons listed as responsibles for a given study
	 *
	 * @param studyId ID of the study
	 * @return a Collection of PeopleBean objects. An empty Collection if no responsibles exist
	 */
	public abstract Collection getResponsiblesForStudy(int studyId) throws Exception;
	
	
	/**
	 * @param id
	 * @return
	 */
	public PeopleBean getPeople(String id);

}