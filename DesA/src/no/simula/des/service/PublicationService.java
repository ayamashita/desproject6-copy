package no.simula.des.service;

import java.util.Collection;

import no.simula.des.data.beans.PublicationBean;

public interface PublicationService {

	/**
	 * Retrieves a single publication from the publication table
	 *
	 * @param id the publication to retrieve
	 * @return the PublicationBean representing the given id
	 */
	public abstract PublicationBean getPublication(String id) throws Exception;

	/**
	 * Retrievs all publications related to one study
	 *
	 * @param studyId identifies the study
	 * @return the publications
	 */
	public abstract Collection getPublicationsForStudy(int studyId) throws Exception;

	/**
	 * Retrieves all publications
	 *
	 * @return the publications
	 * @throws Exception
	 */
	public abstract Collection getPublicationList() throws Exception;

	/**
	 * The method adds and removes relationships between publications and studies at the
	 * database level.
	 *
	 * @param studyId identifies which study we are changing the relationships for.
	 * @param publications all publications related to the study, including any added or deleted
	 *                     ones.
	 * @throws Exception
	 */
	public abstract void updatePublicationStudyRelationship(int studyId,
			Collection publications) throws Exception;

}