package pl.matt.exercises;

import java.util.Collection;
import java.util.List;

public class SimulaService {

	private final static SimulaService instance = new SimulaService();
	
	private JDBCService jdbcService = JDBCService.getInstance();

    
    public static SimulaService getInstance() {
        return instance;
    }
 
    //�eby unikn�� automatycznego tworzenia domy�lnego, publicznego, bezargumentowego konstruktora
    private SimulaService() {
    }
	
	
	public Collection<Integer> getResponsableStudies(String personId) {
		String sql = "SELECT rs.study_id FROM responsible_study rs WHERE rs.responsible_id = ?;";
		List<String[]> results = jdbcService.executeQuery(sql, new Object[] {personId});
		return CollectionTools.getListOfIntegers(results, 0);
	}
	
	public Collection<String> getPublicationIds(int studyId) {
		String sql = "SELECT ps.publication_id FROM publication_study ps WHERE ps.study_id = ?;";
		List<String[]> results = jdbcService.executeQuery(sql, new Object[] {studyId});
		return CollectionTools.getListOfStrings(results, 0);
	}

	public int getNumberOfStudiesPerType(int studyType) {
		String sql = "SELECT count(*) FROM study WHERE study_type = ?;";
		List<String[]> results = jdbcService.executeQuery(sql, new Object[] {studyType});
		return Integer.valueOf(results.get(0)[0]);
	}
	
}
