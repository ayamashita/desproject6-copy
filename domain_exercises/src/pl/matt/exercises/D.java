package pl.matt.exercises;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;


public class D {

	public static void main(String[] args) {
		Map<String, String>[] people = XmlRPCService.getInstance().getAllPeople();
		List<Map<String, String>> peopleList = Arrays.asList(people);
		
		Collections.sort(peopleList, new PersonComparator());

		for (Map<String, String> map : peopleList) {
			System.out.println(map.get("jobtitle"));
		}
		
	}

}
