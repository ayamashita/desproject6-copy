package pl.matt.exercises;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class C {

	public static void main(String[] args) {

		List<String> emails = Arrays.asList("benestad@simula.no", "tore.dyba@sintef.no", "fake@onet.pl");
		Set<Map<String, String>> people = new HashSet<Map<String, String>>(emails.size());
		for (String string : emails) {
			Map<String, String> person = XmlRPCService.getInstance().getFirstPersonWithEmail(string);
			if (person != null) {
				people.add(person);
			}
		}
		System.out.println(people);
		System.out.println(people.size());
	}

}
