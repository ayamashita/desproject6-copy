package pl.matt.exercises;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class JDBCService {

	private final static JDBCService instance = new JDBCService();
	
	private String userName = "root";
	private String password = "123komputer";
	private String url = "jdbc:mysql://localhost:3306/domain_exercises";
	private String driverClassName = "com.mysql.jdbc.Driver";

    
    public static JDBCService getInstance() {
        return instance;
    }
 
    //�eby unikn�� automatycznego tworzenia domy�lnego, publicznego, bezargumentowego konstruktora
    private JDBCService() {
    }
	
	
	public List<String[]> executeQuery(String sql) {
		Connection conn = null;
		List<String[]> results = new ArrayList<String[]>();
		try {
			Class.forName(driverClassName).newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			Statement s = conn.createStatement();
			s.executeQuery(sql);
			ResultSet rs = s.getResultSet();
			int size = rs.getMetaData().getColumnCount()-1;
			while (rs.next()) {
				String[] row = new String[size];
				for (int i = 1; i <= size; i++) {
					row[i-1] = rs.getString(i);
				}
				results.add(row);
			}
			rs.close();
			s.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return results;
	}
	
	public List<String[]> executeQuery(String preparedStatement, Object[] params) {
		Connection conn = null;
		List<String[]> results = new ArrayList<String[]>();
		try {
			Class.forName(driverClassName).newInstance();
			conn = DriverManager.getConnection(url, userName, password);
			java.sql.PreparedStatement s = conn.prepareStatement(preparedStatement);
			if (params != null) {
				for (int i = 0; i < params.length; i++) {
					s.setString(i+1, params[i] != null ? params[i].toString() : null );
				}
			}
			s.execute();
			ResultSet rs = s.getResultSet();
			int size = rs.getMetaData().getColumnCount()-1;
			while (rs.next()) {
				String[] row = new String[size+1];
				for (int i = 0; i <= size; i++) {
					row[i] = rs.getString(i+1);
				}
				results.add(row);
			}
			rs.close();
			s.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return results;
	}	
	
	public void printResults(List<String[]> results) {
		System.out.println("results:");
		for (String[] row : results) {
			for (String s : row) {
				System.out.print("\'" + s + "\'; ");
			}
			System.out.println();
		}
	}
	
}
