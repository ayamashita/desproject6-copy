package pl.matt.exercises;

import java.util.Comparator;
import java.util.Map;

public class PersonComparator implements Comparator<Map<String, String>> {

	private String fieldName = "jobtitle";
	
	public PersonComparator() {
		
	}
	
	public PersonComparator(String fieldName) {
		this.fieldName = fieldName;
	}
	
	@Override
	public int compare(Map<String, String> o1, Map<String, String> o2) {
		
		if (o1 != null && o2 != null) {
			String v1 = o1.get(fieldName);
			String v2 = o2.get(fieldName);
			if (v1 != null) {
				return v1.compareTo(v2);
			}
		}
		return 0;
	}

}
