package pl.matt.exercises;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;



public class A {

	public static void main(String[] args) {

		Collection<Integer> results =  SimulaService.getInstance().getResponsableStudies("2");
		Set<String> publicationIds = new HashSet<String>();
		for (Integer studyId : results) {
			publicationIds.addAll(SimulaService.getInstance().getPublicationIds(studyId));
		}
		for (String publicationId : publicationIds) {
			Map<String, String> out = XmlRPCService.getInstance().getPublication(publicationId);
			System.out.println(out);	
		}

	}
	
}
