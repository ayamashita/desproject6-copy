package pl.matt.exercises;

import java.util.ArrayList;
import java.util.List;

public class CollectionTools {

	/**
	 * @param results
	 * @param columnIndex
	 * @return
	 */
	public static List<Integer> getListOfIntegers(List<String[]> results, int columnIndex) {
		if (results == null) {
			return null;
		}
		List<Integer> out = new ArrayList<Integer>(results.size());
		for (String[] strings : results) {
			out.add(Integer.valueOf(strings[columnIndex]));
		}
		return out;
		
	}

	public static List<String> getListOfStrings(List<String[]> results, int columnIndex) {
		if (results == null) {
			return null;
		}
		List<String> out = new ArrayList<String>(results.size());
		for (String[] strings : results) {
			out.add(strings[columnIndex]);
		}
		return out;
		
	}
	
}
