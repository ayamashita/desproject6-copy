package pl.matt.exercises;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;


public class XmlRPCService {

	private final static XmlRPCService instance = new XmlRPCService();
	private String rpcUrl = "http://simula.no:9080/simula/xmlrpcdes";
	
    public static XmlRPCService getInstance() {
        return instance;
    }
 
    //�eby unikn�� automatycznego tworzenia domy�lnego, publicznego, bezargumentowego konstruktora
    private XmlRPCService() {
    }
	/*
    1XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
    2XmlRpcClient client = new XmlRpcClient();
    3config.setServerURL(new URL("http://simula.no:9080/simula/xmlrpcdes"));
    4client.setConfig(config);
    //When hash is empty, there is no filtering, meaning: retrieve all publications 
    6HashMap hash = new HashMap(); 
    7Object[] methodParams = new Object[]{ hash };
    8Object test = client.execute("getPublications", methodParams);
    9Object[] list = (Object[])test;
    10HashMap element = (HashMap)list[0];
    11System.out.println(element.get("Title").toString());
    */
    
    public Map<String,String> getPublication(String publicationId) {
    	HashMap<String, String> params = new HashMap<String, String>();
    	params.put("id", publicationId);
		try {
	    	Map<String, String>[] out = executeMethod("getPublications", params);
	    	if (out.length == 1) {
	    		return out[0];
	    	}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
    
	@SuppressWarnings("unchecked")
	private Map<String,String>[] executeMethod(String methodName, HashMap<String, String> params) throws MalformedURLException, XmlRpcException {
       	XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
    	XmlRpcClient client = new XmlRpcClient();
    	config.setServerURL(new URL(rpcUrl));
    	client.setConfig(config);
    	Object[] methodParams = new Object[]{ params };
    	Object[] response = (Object[]) client.execute(methodName, methodParams);
    	HashMap<String, String>[] out  = new HashMap[response.length];
    	for (int i = 0; i < response.length; i++) {
    		out[i] = (HashMap<String, String>) response[i];
		}
    	return out;
    }
    
    public Map<String,String> getFirstPersonWithEmail(String email) {
    	HashMap<String, String> params = new HashMap<String, String>();
    	params.put("email", email);
		try {
	    	Map<String, String>[] out = executeMethod("getPeople", params);
	    	if (out.length == 1) {
	    		return out[0];
	    	}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }

    public Map<String,String>[] getAllPeople() {
    	HashMap<String, String> params = new HashMap<String, String>();
 		try {
	    	Map<String, String>[] out = executeMethod("getPeople", params);
    		return out;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
}
