CREATE TABLE `study` (
  `study_id` int(10) NOT NULL auto_increment,
  `study_name` varchar(200) NOT NULL default '',
  `study_type` int(3) NOT NULL default '0',
  `study_description` text NOT NULL,
  `study_keywords` varchar(100) default NULL,
  `study_notes` text,
  `study_students` int(3) default NULL,
  `study_professionals` int(3) default NULL,
  `study_duration` int(4) default NULL,
  `study_duration_unit` int(1) default NULL,
  `study_start_date` date default NULL,
  `study_end_date` date NOT NULL default '0000-00-00',
  `study_created_by` int(4) default NULL,
  `study_created_date` date default NULL,
  `study_edited_by` int(4) default NULL,
  `study_edited_date` date default NULL,
  PRIMARY KEY  (`study_id`),
  UNIQUE KEY `study_id` (`study_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin2 PACK_KEYS=1;

INSERT INTO `study` (`study_id`,`study_name`,`study_type`,`study_description`,`study_keywords`,`study_notes`,`study_students`,`study_professionals`,`study_duration`,`study_duration_unit`,`study_start_date`,`study_end_date`,`study_created_by`,`study_created_date`,`study_edited_by`,`study_edited_date`) VALUES ('1','A study','4','A simple test study','study, simple','note','2','4','2','4','2003-01-01','2004-12-31','17','2003-01-01','90','2008-04-23');
INSERT INTO `study` (`study_id`,`study_name`,`study_type`,`study_description`,`study_keywords`,`study_notes`,`study_students`,`study_professionals`,`study_duration`,`study_duration_unit`,`study_start_date`,`study_end_date`,`study_created_by`,`study_created_date`,`study_edited_by`,`study_edited_date`) VALUES ('2','The study','1','Simple test study','study, simple','more notes','3','2','23','3','2001-01-12','2003-01-11','10','2001-10-12','90','2008-04-02');
INSERT INTO `study` (`study_id`,`study_name`,`study_type`,`study_description`,`study_keywords`,`study_notes`,`study_students`,`study_professionals`,`study_duration`,`study_duration_unit`,`study_start_date`,`study_end_date`,`study_created_by`,`study_created_date`,`study_edited_by`,`study_edited_date`) VALUES ('3','Study for SW design','2','Description of a study','study','notes','8','6','0','4','2001-08-01','2002-01-07','45','1999-08-01','90','2008-04-23');
INSERT INTO `study` (`study_id`,`study_name`,`study_type`,`study_description`,`study_keywords`,`study_notes`,`study_students`,`study_professionals`,`study_duration`,`study_duration_unit`,`study_start_date`,`study_end_date`,`study_created_by`,`study_created_date`,`study_edited_by`,`study_edited_date`) VALUES ('5','Study for testing the System','1','DES Testing','DES, Maintainability, Software Evaluation, Expert Assessment','This study will attempt to evaluate the design of DES (2008-04-23).\r\n','1','3','-1','1','2008-06-01','2008-10-10','90','2008-04-23','90','2008-04-23');

CREATE TABLE `publication_study` (
  `study_id` int(10) NOT NULL,
  `publication_id` varchar(255) NOT NULL,
  PRIMARY KEY  (`study_id`,`publication_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin2 PACK_KEYS=1;

CREATE TABLE `responsible_study` (
  `study_id` int(10) NOT NULL,
  `responsible_id` varchar(255) NOT NULL,
  PRIMARY KEY  (`study_id`,`responsible_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin2 PACK_KEYS=1;

-- TODO Foreign keys
