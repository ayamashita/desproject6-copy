<%@include file="include/include-top.jsp"%>

  <h1>Study types</h1>

  <html:form action="/adm/studytypes/manage">
    <halogen:table name="<%= Constants.TABLE_STUDYTYPES %>" selectableRows="true" id="id"/>
    <html:submit property="new" value="new..."/>
    <html:submit property="delete" value="delete selected"/>
  </html:form>

<%@include file="include/include-bottom.jsp"%>