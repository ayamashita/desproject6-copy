<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<%@ include file="include/include-top.jsp"%>

	<div id="navActions">
		<logic:present name="user">
			<a href="frontPage.do">Front page</a> > </logic:present>
		<a href="list">Reports</a>
	</div>
	<br />
	<br />

	<table
		style="background-color: white; width: 100%; border: 1px outset gray; border-collapse: separate; border-spacing: 2px;">
		<tr>
			<logic:iterate id="parameter" name="reportForm"
				property="currentReport.columnNames">
				<th>
					<bean:write property="value" name="parameter" />
				</th>
			</logic:iterate>
		</tr>


		<logic:iterate id="row" name="reportForm" property="executedReport">
			<tr>
				<logic:iterate id="column" name="row" property="columns">
					<td
						style="text-align: center; border: 1px inset gray;">
						<logic:iterate id="nameAndUrl" name="column" property="namesAndUrlValues">
							<logic:empty property="value" name="nameAndUrl">
								<bean:write property="name" name="nameAndUrl" />
							</logic:empty>
							<logic:notEmpty property="value" name="nameAndUrl">
								<a href="<bean:write property="value" name="nameAndUrl" />">
									<bean:write property="name" name="nameAndUrl" />
								</a>;<br />
							</logic:notEmpty>
						</logic:iterate>
					</td>
				</logic:iterate>
			</tr>
		</logic:iterate>
	</table>
	<%@ include file="include/include-bottom.jsp"%>