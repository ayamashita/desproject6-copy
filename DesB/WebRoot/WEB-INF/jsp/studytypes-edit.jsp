<%@page import="no.simula.des.presentation.web.forms.SimpleForm"%>
<%@include file="include/include-top.jsp"%>

  <h1>
    <logic:present name="<%= Constants.FORM_SIMPLE %>" property="id">Edit study type</logic:present>
    <logic:notPresent name="<%= Constants.FORM_SIMPLE %>" property="id">Register new study type</logic:notPresent>
  </h1>

  <html:form action="/adm/studytypes/edit" method="post">

    <logic:messagesPresent>
      <table class="error">
        <tr>
          <td>
            <ul>
              <html:messages id="error">
                <li><bean:write name="error"/></li>
              </html:messages>
            </ul>
          </td>
        </tr>
      </table>
    </logic:messagesPresent>

    <table class="container">
      <tr>
        <td>name</td>
        <td><html:text property="name" size="36"/></td>
      </tr>
    </table>
    <html:submit property="save" value="save changes"/>
    <html:cancel value="cancel"/>
 </html:form>

<%@include file="include/include-bottom.jsp"%>