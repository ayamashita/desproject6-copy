<%@include file="include/include-top.jsp"%>

  <h1>Manage privileges</h1>

  <html:form action="/adm/privileges/manage">

    <logic:messagesPresent>
      <table class="error">
        <tr>
          <td>
            <ul>
              <html:messages id="error">
                <li><bean:write name="error"/></li>
              </html:messages>
            </ul>
          </td>
        </tr>
      </table>
    </logic:messagesPresent>

    <table class="container">
      <tr>
        <td>

          <!-- Not needed if user has activated scripting because then the
          select-element will allways reflect the selected privilege -->
          <noscript>
            selected privilege
            <b><bean:write name="<%= Constants.FORM_PRIVILEGES %>" property="selected.name"/></b>
          </noscript>

          <hr/>
          
          <html:select property="privilegeId" onchange="document.forms(1).elements['change'].click()">
            <html:optionsCollection property="all" value="id" label="name"/>
          </html:select>
          <html:submit property="change" value="change"/>

          <hr/>

          <table class="container">
            <tr>
              <th>available persons</th>
              <th/>
              <th>persons with privilege</th>
            </tr>
            <tr>
              <td>
                <logic:empty name="<%= Constants.FORM_PRIVILEGES %>" property="persons">
                  <html:select property="addPersons" multiple="true" size="5" disabled="true">
                    <html:option value="-1" key="lists.option.empty"/>
                  </html:select>
                </logic:empty>
                <logic:notEmpty name="<%= Constants.FORM_PRIVILEGES %>" property="persons">
                  <html:select property="addPersons" multiple="true" size="5">
                    <html:optionsCollection property="persons" value="id" label="fullName"/>
                  </html:select>
                </logic:notEmpty>
              </td>
              <td>
                <html:submit property="add" value=">"/><br/>
                <html:submit property="remove" value="<"/>
              </td>
              <td>
                <logic:empty name="<%= Constants.FORM_PRIVILEGES %>" property="selected.persons">
                  <html:select property="removePersons" multiple="true" size="5" disabled="true">
                    <html:option value="-1" key="lists.option.empty"/>
                  </html:select>
                </logic:empty>
                <logic:notEmpty name="<%= Constants.FORM_PRIVILEGES %>" property="selected.persons">
                  <html:select property="removePersons" multiple="true" size="5">
                    <html:optionsCollection property="selected.persons" value="id" label="fullName"/>
                  </html:select>
                </logic:notEmpty>
              </td>
            </tr>
          </table>

        </td>
      </tr>
    </table>

    <html:submit property="save" value="save changes"/>
    <html:cancel value="cancel"/>

  </html:form>

<%@include file="include/include-bottom.jsp"%>