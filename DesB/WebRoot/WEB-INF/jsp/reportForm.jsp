<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

	<%@ include file="include/include-top.jsp"%>

	<script type="text/javascript">
      $(document).ready(function() {
          fillSelectedFils();
      });
	</script>

	<table bgcolor="#F5F5F5" border="0" width="100%" class="bodytext">
		<tr>
			<td colspan="3">
				<h1>
					Report edit form
				</h1>
				<span style="color: red;"> <b><html:errors /> </b> </span>
			</td>
			<td>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				Complete form below, then click "Save". Mandatory fields are marked
				with *
			</td>
			<td align="right" class="bodytext-bold">
				<a href="JavaScript:location.href='listReports.do'">Cancel</a>
			</td>
			<td>

			</td>
		</tr>
		<tr>
			<td colspan="3">
				<hr style="height: 1px; color: black; width: 100%;">
			</td>
			<td></td>
		<tr>
	</table>
	<html:form action="/adm/reports/save" method="post">
		<html:hidden property="action" value="0" />
		<html:hidden property="selectParameters" />
		<html:hidden property="currentReport.id" />
		<input type="submit" value="save" />
		<table bgcolor="#F5F5F5" border="0" width="100%" class="bodytext">
			<tr class="bodytext-bold">
				<td>
					Report name*
				</td>
				<td>
					<html:text property="currentReport.name" size="60" />
				</td>
			</tr>
			<tr>
				<td>
					Select fileds*
				</td>
				<td>
					<table>
						<tr>
							<td>
								<select id="allFields" size="17" style="width: 150px;">
									<logic:iterate id="field" name="reportForm"
										property="allFields">
										<option value="<bean:write name="field" property="value" />">
											<bean:write name="field" property="name" />
										</option>
									</logic:iterate>
								</select>
							</td>
							<td>
								<a href="JavaScript:addField()">---></a>
								<br />
								<br />
								<br />
								<a href="JavaScript:removeField()"><---</a>
							</td>
							<td>
								<select id="selectedFields" name="selectedFields" size="17"
									style="width: 150px;">
								</select>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			
			<tr>
				<td>
					Order by
				</td>
				<td>
					<html:select property="currentReport.parameters[1].value">
						<html:option value="DESC">DESC</html:option>
						<html:option value="ASC">ASC</html:option>
					</html:select>
				</td>
			</tr>			
			
			<tr>
				<td>
					Add responsible condition
				</td>
				<td>
					<a
						href="JavaScript:document.reportForm.action.value='1';document.reportForm.submit();">Add</a>
				</td>
			</tr>

			<tr>
				<td>
					Responsible's id*
				</td>
				<td>
					<html:text property="currentReport.parameters[0].value" size="60" />
				</td>
			</tr>

			<logic:iterate offset="2" indexId="index" id="param"
				name="reportForm" property="currentReport.parameters">
				<logic:notEqual value="DESC" name="param" property="type">
					<logic:notEqual value="ASC" name="param" property="type">
						<logic:notEqual value="SELECT" name="param" property="type">
							<tr>
								<td>
									Condition's type
								</td>
								<td>
									<logic:equal value="AND" name="param" property="type">
										<select name="param_<%=index%>_type"
											id="param_<%=index%>_type">
											<option selected="true">
												AND
											</option>
											<option>
												OR
											</option>
										</select>
									</logic:equal>
									<logic:notEqual value="AND" name="param" property="type">
										<select name="param_<%=index%>_type"
											id="param_<%=index%>_type">
											<option>
												AND
											</option>
											<option selected="true">
												OR
											</option>
										</select>
									</logic:notEqual>
								</td>
							</tr>
							<tr>
								<td>
									Responsible's id
								</td>
								<td>
									<input type="text" name="param_<%=index%>_value"
										id="param_<%=index%>_value"
										value="<bean:write name="param" property="value"/>" />
								</td>
							</tr>
						</logic:notEqual>
					</logic:notEqual>
				</logic:notEqual>
			</logic:iterate>

			<tr align="right">
				<td colspan="2" align="right"></td>
			</tr>
		</table>
	</html:form>

	<%@ include file="include/include-bottom.jsp"%>