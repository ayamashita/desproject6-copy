
function openPopup(url, popupname) {
	//alert(url);
	window.open(url, popupname, "width=380, height=195 toolbar=0,directories=0,menubar=0,status=no,resizable=no,location=0,scrollbars=no,copyhistory=0,alwaysRaised=1");
}
function openPopupGeneral(url, popupname, width, height) {
	//alert(url);
	openPopupGeneral(url, popupname, width, height, "no");
}
function openPopupGeneral(url, popupname, width, height, scollbar) {
	window.open(url, popupname, "width=" + width + ", height=" + height + ", toolbar=0,directories=0,menubar=0,status=no,resizable=no,location=0,scrollbars=" + scollbar + ",copyhistory=0,alwaysRaised=1");
}
function performSimulaSearch() {
	if (document.simple_search.search_text.value) {
		document.simple_search.submit();
	} else {
		alert("Please input text to search.");
		return false;
	}
}

function addField() {
	var selectedFields = document.reportForm.selectedFields;
	var allFields = $('#allFields')[0];

	var allOptions = allFields.options;
	for (var i=0; i<allOptions.length; i++){
		if (allOptions[i].selected == true) {
			selectedFields.options[selectedFields.options.length] = allOptions[i];
			//selectedFields.options[selectedFields.options.length].selected=false;
		}
	}
	fillSelectParameters();
}

function removeField() {
	var selectedFields = document.reportForm.selectedFields;
	var allFields = $('#allFields')[0];

	var selectedOptions = selectedFields.options;
	for (var i=0; i<selectedOptions.length; i++){
		if (selectedOptions[i].selected == true) {
			allFields.options[allFields.options.length] = selectedOptions[i];
		}
	}
	
	fillSelectParameters();
}

function fillSelectParameters() {
	var selectedFields = $('#selectedFields')[0];
	var selectParameters = document.reportForm.selectParameters;
	var value = '';
	var selectedOptions = selectedFields.options;
	for (var i=0; i<selectedOptions.length; i++){
		value = value + selectedOptions[i].value + ';';
	}
	selectParameters.value = value;
}

function fillSelectedFils() {
	var value = document.reportForm.selectParameters.value;
	var values = value.split(";");
	var selectedFields = $('#selectedFields')[0];	
	var allFields = $('#allFields')[0];	
	var allOptions = allFields.options;
	
	for (var i=0; i<values.length; i++){
		if (values[i].length > 0) {
			var optn = document.createElement("OPTION");
			optn.value = values[i];
			optn.text = getOptionText(values[i], allOptions);
			selectedFields.options[i] = optn;		
		}
	}

	var targetOptions;
	var j = 0;
    
	for (var i=allOptions.length-1; i >=0 ; i--) {
		var currentOption = allOptions[i];
		if ( isOptionOnTheList(selectedFields.options, currentOption.value) ) {
			allFields.remove(i);
		}
	}
}

function getOptionText(optionValue, options) {
	for (var i=0; i<options.length; i++){
		var currentOption = options[i];
		if (currentOption.value == optionValue) {
			return currentOption.text;
		}
	}
	return null;
}

function isOptionOnTheList(options, optionValue) {
	for (var i=0; i<options.length; i++){
		var currentOption = options[i];
		if (currentOption.value == optionValue) {
			return true;
		}
	}
	return false;
}
