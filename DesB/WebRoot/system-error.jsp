<%@include file="WEB-INF/jsp/include/include-top.jsp"%>

  <h1>System failure</h1>

  <p>An unexpected system failure occured.</p>

  <p>
    We apologize for this inconvenience and if this 
    error persists, please contact
    <a href="mailto:webmaster@simula.no">webmaster@simula.no</a>
    and attach a description of the error.
  </p>

<%@include file="WEB-INF/jsp/include/include-bottom.jsp"%>
