package no.halogen.persistence;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;


public abstract class AbstractXmlRpcPersistentObject implements Serializable {

	private String rpcUrl = "http://simula.no:9080/simula/xmlrpcdes";
	
	protected Map[] executeMethod(String methodName, HashMap params) throws MalformedURLException, XmlRpcException {
       	XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
    	XmlRpcClient client = new XmlRpcClient();
    	config.setServerURL(new URL(rpcUrl));
    	client.setConfig(config);
    	Object[] methodParams = new Object[]{ params };
    	Object[] response = (Object[]) client.execute(methodName, methodParams);
    	HashMap[] out  = new HashMap[response.length];
    	for (int i = 0; i < response.length; i++) {
    		out[i] = (HashMap) response[i];
		}
    	return out;
    }
 
	/**
	 * @param methodName
	 * @param params
	 * @return
	 * @throws MalformedURLException
	 * @throws XmlRpcException
	 */
	protected Object executeMethodForObject(String methodName, HashMap params) throws MalformedURLException, XmlRpcException {
       	XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
    	XmlRpcClient client = new XmlRpcClient();
    	config.setServerURL(new URL(rpcUrl));
    	client.setConfig(config);
    	Object[] methodParams = new Object[]{ params };
    	return client.execute(methodName, methodParams);
    }	
	
}
