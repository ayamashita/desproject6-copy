package no.halogen.persistence;

/**
 * Objects that shall be persisted must implement
 * this interface
 *
 * @author  Stian Eide
 */
public interface XmlRpcPersistable extends Persistable {
  
  /**
   * Returns the id in the persistent store.
   * @return the id
   */
  public String getIdAsString();
  /**
   * Sets the id in the persistent store
   * @param id the id
   */
  public void setId(String id);

}