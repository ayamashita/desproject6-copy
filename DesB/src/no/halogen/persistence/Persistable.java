package no.halogen.persistence;

/**
 * Objects that shall be persisted must implement
 * this interface
 *
 * @author  Stian Eide
 */
public interface Persistable {
  
  /**
   * Returns the id in the persistent store.
   * @return the id
   */
  public Object getId();
  /**
   * Sets the id in the persistent store
   * @param id the id
   */
  public void setId(Object id);

}