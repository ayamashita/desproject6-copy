package no.halogen.statements;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Contains common methods for all statement classes.
 * Offers services for finding data nad getting search results, creation, update and deletion of entities in a database table
 * @author Frode Langseth
 */
public interface ObjectStatement {
  /**
   * Method executeDelete
   * Deletes an entity from a database table
   * This method doesn't receives any parameters, and a databean must be set for the statement, such that the method can know what entity to delete
   * 
   * @return boolean sucess - true/false
   * @throws StatementException if the databean doesn't exist, is null or empty.
   */
  boolean executeDelete() throws StatementException;
  /**
   * Method executeDelete
   * Deletes an entity from a database table, idetified by <code>entityId</code>
   * @param entityId Integer idnetifies the entity to delete
   * @return boolean sucess - true/false
   * @throws StatementException if there's an error during statement generation
   */
  boolean executeDelete(Integer entityId) throws StatementException;

  /**
   * Method executeInsert
   * Creates a new instance of an entity in a database table.
   * This method doesn't receives any parameters, so a databean or the conditions must be set for the statement before the enityt is created
   *
   * @return Integer value of the key generated for the newly created entity
   * @throws StatementException if there's an error during statement generation
   */
  Integer executeInsert() throws StatementException;

  /**
   * Method executeSelect
   * Executes a select statement in a database table, and fetches the result data
   * This method doesn't receive any parameters, so the databean, where clause or conditions must be set before executing the statement.
   * If the data bean is set, a condition using the table key and the dataean's id will be generated.
   *
   * @return List result from the search
   * @throws StatementException if there's an error during statement generation
   */
  List executeSelect() throws StatementException;
  /**
   * Method executeSelect
   * Executes a select statement in a database table, and fetches the result data
   * This methods receives a key as parameter, and generates a search condition based on a table's key column
   *
   * @param entityId Integer the id of the entity to fetch
   * @return Object the entity that contains the <code>identificator</code>, with populated values from the database
   * @throws StatementException if there's an error during statement generation
   */
  Object executeSelect(Integer entityId) throws StatementException;
  /**
   * Method executeSelect
   * Executes a select statement in a database table, and fetches the result data
   * This methods receives a list of keys as parameter, and generates a search condition based on a table's key column
   * 
   * @param entityIds List a list of integer ids for entities to fetch
   * @return List objects related to any of the <code>identificators</code> in <code>entityIds</code>, with populated values from the database
   * @throws StatementException if there's an error during statement generation
   */
  List executeSelect(List entityIds) throws StatementException;

  /**
   * Method executeUpdate
   * Updates values for a entity in the database
   * This method doesn't receive any parameters, so the databean, where clause or conditions must be set before executing the statement.
   *
   * @return boolean sucess - true/false
   * @throws StatementException if there's an error during statement generation
   */
  boolean executeUpdate() throws StatementException;
  /**
   * Method executeUpdate
   * Updates values for a entity in the database
   * Even if this method receive a entity id as a parameter, a databean must be set to make it possible for the statement to update the database entity 
   *
   * @param entityId Integer id of the entity to update
   * @return boolean sucess - true/false
   * @throws StatementException if there's an error during statement generation
   */
  boolean executeUpdate(Integer entityId) throws StatementException;

  //List fetchListResults( ResultSet resultSet, Object entity );
  /**
   * Method fetchListResults
   * The method builds the result structure with a list of databeans from the <code>ResultSet</code>, 
   * and calls the <code>fetchResults</code> method to populate the data beans
   *
   * @param resultSet ResultSet the <code>ResultSet</code> that contains the results from a query. Will only be called after select statements
   * @return List the results from the query
   * @throws SQLException if there's an error during statement generation
   */
  List fetchListResults(ResultSet resultSet) throws SQLException;

  //	Object fetchResults( ResultSet resultSet, Object entity );
  /**
   * Method fetchResults
   * Gets the reulsts from a <code>ResultSet</code>, and populates the databean with the values
   *
   * @param resultSet ResultSet the <code>ResultSet</code> that contains the results from a query. Will only be called after select statements
   * @return Object the databean to populated with result data
   * @throws SQLException if there's an error during statement generation
   */
  Object fetchResults(ResultSet resultSet) throws SQLException;

  /**
   * Method setConditions
   * In a <code>PreparedStatement</code>, values are represented by '?' characters. 
   * This methods receives a list with the actual values, so that the <code>PreparedStatement</code> can be populated with 
   * the values before it is executed
   *
   * @param conditions List of condition values
   */
  void setConditions(List conditions);
  /**
   * Method getConditions
   * In a <code>PreparedStatement</code>, values are represented by '?' characters. 
   * This methods gets a list with the actual values
   *
   * @return List of condition values
   */
  List getConditions();

}
