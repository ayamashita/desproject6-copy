package no.halogen.presentation.web;

import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;


/**
 *
 * This class contains utility methods to check actions that
 * may be either a new-request or a edit-request.
 *
 * @author  Stian Eide
 */
public class CheckNewAction {
  
  /** Checks whether the given <CODE>ActionMapping</CODE> is a new-request or not.
   * Edit-actions may be either edit-requests or new-requests depending on whether
   * the request parameter <CODE>new=true</CODE> is set.
   * @param request the current http-request object
   * @param form the <CODE>ActionForm</CODE> accociated with this request
   * @param mapping the <CODE>ActionMapping</CODE> accociated with this request
   * @throws Exception if action could not be successfully checked
   */  
  public static void checkAction(
    HttpServletRequest request,
    ActionForm form,
    ActionMapping mapping)
    throws Exception
  {
    Set newActions = (Set)request.getSession().getAttribute(Constants.IS_NEW);
    if(newActions == null) {
      newActions = new TreeSet();
      request.getSession().setAttribute(Constants.IS_NEW, newActions);
    }
    if(form instanceof CheckNewActionForm && ((CheckNewActionForm)form).getId() == null) {
       newActions.add(mapping.getPath());
    }
    else {
      newActions.remove(mapping.getPath());
    }
  }
}