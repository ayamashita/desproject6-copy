package no.halogen.presentation.web;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.Globals;
import org.apache.struts.util.RequestUtils;

/**
 *  This tag writes a navigation path based on recursive
 *  "return"-forwards in the current action, the action of
 *  the "return"-forward and so forth.
 */
public class HistoryPathTag extends TagSupport {
  
  /** Creates a new instance of <CODE>HistoyPathTag</CODE>. */  
  public HistoryPathTag() {
    super();
  }
  
  /** */  
  private static Log log = LogFactory.getLog(HistoryPathTag.class);
  
  /** Writes the navigation path to the JspWriter.
   * @throws JspException if a valid url for a item in the navigation path could not be created
   */
  public void otherDoStartTagOperations() throws JspException {
    
    try {
      JspWriter out = pageContext.getOut();
      HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
      Set newActions = (Set)request.getSession().getAttribute(Constants.IS_NEW);
      List navigationPath = (List)request.getAttribute(Constants.NAVIGATION_PATH);
      if(navigationPath != null) {
        for(Iterator i = navigationPath.iterator(); i.hasNext();) {
          String path = (String)i.next();
          String labelKey = "navigation.history." + path;
          if(newActions != null && newActions.contains(path)) {
            labelKey += ".new";
          }
          String label = RequestUtils.message(pageContext, Globals.MESSAGES_KEY, Globals.LOCALE_KEY, labelKey);
          if(i.hasNext()) {
            String url = RequestUtils.computeURL(pageContext, null, null, null, path, null, null, false);
            out.println("<a href=\"" + url + "\">" + label + "</a>");
            out.println("&gt;");
          }
          else {
            out.println("<span class=\"pathcurrentpage\">" + label + "</span>");
          }
        }
      }
    }
    catch (IOException ioe) {
      log.error("Failed to generate historytag.", ioe);
      throw new JspException("Failed to generate historytag");
    }
  }
  
  /** Fill in this method to determine if the tag body should be evaluated
   * Called from doStartTag().
   * @return <CODE>true</CODE>
   */
  public boolean theBodyShouldBeEvaluated()  {
    return(true);
  }
  
  /**
   * Fill in this method to perform other operations from doEndTag().
   */
  public void otherDoEndTagOperations()  {}
  
  /** Fill in this method to determine if the rest of the JSP page
   * should be generated after this tag is finished.
   * Called from doEndTag().
   * @return <CODE>true</CODE>
   */
  public boolean shouldEvaluateRestOfPageAfterEndTag()  {
    return(true);
  }
  
  /** This method is called when the JSP engine encounters the start tag,
   * after the attributes are processed.
   * Scripting variables (if any) have their values set here.
   * @return EVAL_BODY_INCLUDE if the JSP engine should evaluate the tag body, otherwise return SKIP_BODY.
   * This method is automatically generated. Do not modify this method.
   * Instead, modify the methods that this method calls.
   * @throws JspException if an <CODE>JspException</CODE> is thrown from <CODE>otherDoEndTagOperations()</CODE>
   * or <CODE>shouldEvaluateRestOfPageAfterEndTag()</CODE>
   */
  public int doStartTag() throws JspException {
    otherDoStartTagOperations();
    
    if(theBodyShouldBeEvaluated()) {
      return(EVAL_BODY_INCLUDE);
    } 
    else {
      return(SKIP_BODY);
    }
  }
  
  /** This method is called after the JSP engine finished processing the tag.
   * @return EVAL_PAGE if the JSP engine should continue evaluating the JSP page, otherwise return SKIP_PAGE.
   * This method is automatically generated. Do not modify this method.
   * Instead, modify the methods that this method calls.
   * @throws JspException if a <CODE>JspException</CODE> is thrown from <CODE>otherDoEndTagOperations()</CODE>
   * or <CODE>shouldEvaluateRestOfPageAfterEndTag()</CODE>
   */
  public int doEndTag() throws JspException {
    otherDoEndTagOperations();
    if(shouldEvaluateRestOfPageAfterEndTag()) {
      return(EVAL_PAGE);
    }
    else {
      return(SKIP_PAGE);
    }
  }
}