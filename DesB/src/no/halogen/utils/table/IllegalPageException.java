package no.halogen.utils.table;

/** This exception is thrown if a non-existing row is specified in
 * <CODE>Table.setCurrentPage()</CODE>.
 * @author Stian Eide
 */
public class IllegalPageException extends Exception {
  
  /** Creates a new <CODE>IllegalPageException</CODE>. */  
  public IllegalPageException() {}
  
  /** Creates a new <CODE>IllegalPageException</CODE> with the specified message.
   * @param message a text message, which describes the error
   */  
  public IllegalPageException(String message) {
    super(message);
  }
}
