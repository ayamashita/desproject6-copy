package no.halogen.utils.table;

import java.lang.reflect.Method;
import java.util.Comparator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/** This comparator uses reflection to sort two objects
 * by a given property using the natural sorting order
 * of that property's class. This class will sort the rows
 * in a table by the values in the specified column.
 * @author Stian Eide
 */
public class ColumnSortComparator implements Comparator {
  
  /** Creates a new <CODE>ColumnSortComparator</CODE>.
   * @param column the name of the column that the rows will be sorted by
   * @param ascending <CODE>true</CODE> if the rows should be sorted ascending.
   */
  public ColumnSortComparator(String column, boolean ascending) {
    this.column = column;
    this.ascending = ascending;
  }
  
  private static Log log = LogFactory.getLog(ColumnSortComparator.class);
  private String column;
  private boolean ascending;
  
  /** Compares its two arguments for order. Returns a negative integer, zero, or a
   * positive integer as the first argument is less than, equal to, or greater
   * than the second.
   * @param o1 the first object to be compared
   * @param o2 the second object to be compared
   * @return a negative integer, zero, or a positive integer as the first argument is less
   * than, equal to, or greater than the second
   */  
  public int compare(Object o1, Object o2) {
    try {
      String methodName = "get" + column.substring(0, 1).toUpperCase() + column.substring(1);
      if(log.isDebugEnabled()) log.debug("Comparing by invoking method: " + methodName);
      if(log.isDebugEnabled()) {
        log.debug("Comparing object '" + o1.getClass().getName() + 
          "' and object '" + o2.getClass().getName() + "'.");
      }

      Method method1 = o1.getClass().getMethod(methodName, null);
      Object value1 = method1.invoke(o1, null);
      
      Method method2 = o2.getClass().getMethod(methodName, null);
      Object value2 = method2.invoke(o2, null);      

      int order = ((Comparable)value1).compareTo(value2);
      if(log.isDebugEnabled()) {
        log.debug("Comparing value '" + value1 + "' and value '" + 
          value2 + "' -- Result: " + order);
      }
      if(ascending) {
        return(order);
      }
      else {
        return(-order);
      }
    }
    catch(Exception e) {
      log.error("Failed to compare objects.", e);
    }
    return(0);
  }
}