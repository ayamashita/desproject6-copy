package no.halogen.utils.table;

/** This exception is thrown if a non-existing column is specified in
 * <CODE>Table.sortBy()</CODE>.
 * @author Stian Eide
 */
public class IllegalColumnException extends Exception {
  
  /** Creates a new <CODE>IllegalColumnException</CODE>. */  
  public IllegalColumnException() {}
  
  /** Creates a new <CODE>IllegalColumnException</CODE> with the specified message.
   * @param message a text message, which describes the error
   */  
  public IllegalColumnException(String message) {
    super(message);
  }
}