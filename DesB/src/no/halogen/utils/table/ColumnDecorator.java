package no.halogen.utils.table;

/** This interface should be implemented by classes that are used to format the rows
 * of a specific column in a table. Each column in a table may be given a unique
 * decorator to allow maximum flexibility in displaying content in a table.
 * @author Stian Eide
 */
public interface ColumnDecorator {
  
  /** This method takes the value object of a row and returns a <CODE>String</CODE>
   * that will be displayed in the table.
   * @param value an <CODE>Object</CODE> with the value
   * @throws Exception if the value cannot be rendered
   * @return a <CODE>String</CODE>-representation of the value.
   */  
  public String render(Object value) throws Exception;
  
}
