package no.halogen.utils.table;

import java.util.ArrayList;
import java.util.List;

/** This implementation of <CODE>TableSource</CODE> is itself an
 * <CODE>ArrayList</CODE>. In addition, this <CODE>List</CODE> keeps a reference to
 * the original <CODE>List</CODE>, which this source is made from.
 * @author Stian Eide
 */
public class TableSourceArrayList extends ArrayList implements TableSource {
  
  /** Creates a new <CODE>TableSourceArrayList</CODE> based on <CODE>original</CODE>.
   * A reference to the original <CODE>List</CODE> will be kept in order to update
   * this source when <CODE>update()</CODE> is invoked.
   * @param original the original <CODE>List</CODE> that this <CODE>List</CODE> is made from
   */
  public TableSourceArrayList(List original) {
    super(original);
    this.original = original;
  }
  
  /** A reference to the original <CODE>List</CODE> */  
  private List original;
  
  /** Returns the source itself, which is a <CODE>List</CODE>.
   * @return <CODE>this</CODE>
   */  
  public List getContent() {
    return(this);
  }
  
  /** Updates this <CODE>List</CODE> by clearing it, and then adding all elements from
   * <CODE>original</CODE>.
   */  
  public void update() {
    this.clear();
    this.addAll(original);
  }
}