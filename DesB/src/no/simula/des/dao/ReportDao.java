package no.simula.des.dao;

import java.util.List;

import no.simula.ReportBean;

public interface ReportDao {

	/**
	 * @param reportBean
	 */
	public void createReport(ReportBean reportBean);

	/**
	 * @param reportBean
	 */
	public void updateReport(ReportBean reportBean);

	/**
	 * @param reportId
	 * @return
	 */
	public ReportBean loadReport(int reportId);

	/**
	 * @param reportId
	 */
	public void deleteReport(int reportId);

	/**
	 * @return
	 */
	public List loadAllReportsOrderByName();
	
	/**
	 * @param userId
	 * @return
	 */
	public List loadAllReportsWithGivenCreatorOrderByName(String userId);
	
	/**
	 * @param name
	 * @param ownerId
	 * @return
	 */
	public ReportBean loadReport(String name, String ownerId);
	
	/**
	 * @param sql
	 * @param params
	 * @return
	 */
	public List executeQuery(String sql, Object[] params);
	
}
