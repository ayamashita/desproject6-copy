package no.simula.des.dao.impl;

import java.util.List;

import no.simula.ReportParameterBean;
import no.simula.des.dao.ReportParameterDao;
import no.simula.rowmapper.ReportParameterBeanRowMapper;

/**
 * @author mateusz
 *
 */
public class JdbcReportParameterDao extends AbstractJdbcDao implements
		ReportParameterDao {

	private static final String INSERT_REPORT_PARAMETER_QUERY= "INSERT INTO report_parameters (report_id, value, type, report_index) VALUES (?, ?, ?, ?)";
	private static final String DELETE_REPORT_PARAMETER_QUERY= "DELETE FROM report_parameters WHERE id = ?";
	private static final String UPDATE_REPORT_PARAMETER_QUERY= "UPDATE report_parameters SET report_id = ?, value = ?, type = ?, report_index = ?  WHERE id = ?";
	private static final String SELET_SINGLE_REPORT_QUERY = "SELECT id, report_id, value, type, report_index FROM report_parameters WHERE id = ?";
	private static final String SELET_ALL_REPORTS_FOR_REPORT_QUERY = "SELECT id, report_id, value, type, report_index FROM report_parameters WHERE report_id = ? ORDER BY report_index";
	private static final String DELETE_REPORT_PARAMETERS_QUERY= "DELETE FROM report_parameters WHERE report_id = ?";

	
	private final static JdbcReportParameterDao instance = new JdbcReportParameterDao();
	public static JdbcReportParameterDao getInstance() {
        return instance;
    }
	
	public void createReportParameter(ReportParameterBean reportParameterBean) {
		getJdbcTemplate().update(INSERT_REPORT_PARAMETER_QUERY, new Object[] {new Integer(reportParameterBean.getReportBean().getId()), reportParameterBean.getValue(), reportParameterBean.getType(), new Integer(reportParameterBean.getIndex())});
	}
	public void deleteReportParameter(int parameterId) {
		getJdbcTemplate().update(DELETE_REPORT_PARAMETER_QUERY, new Object[]{new Integer(parameterId)});
	}
	
	public void deleteReportParameters(int reportId) {
		getJdbcTemplate().update(DELETE_REPORT_PARAMETERS_QUERY, new Object[]{new Integer(reportId)});
	}
	
	public ReportParameterBean loadReportParameter(int parameterId) {		
		return (ReportParameterBean) getJdbcTemplate().queryForObject(SELET_SINGLE_REPORT_QUERY, new Object[]{new Integer(parameterId)}, new ReportParameterBeanRowMapper());
	}
	public List loadReportParametersForReport(int reportId) {
		return getJdbcTemplate().query(SELET_ALL_REPORTS_FOR_REPORT_QUERY, new Object[]{new Integer(reportId)}, new ReportParameterBeanRowMapper());
	}
	public void updateReportParameter(ReportParameterBean reportParameterBean) {
		getJdbcTemplate().update(
				UPDATE_REPORT_PARAMETER_QUERY, 
				new Object[] {
						new Integer(reportParameterBean.getReportBean().getId()), 
						reportParameterBean.getValue(), 
						reportParameterBean.getType(), 
						new Integer(reportParameterBean.getIndex()), 
						new Integer(reportParameterBean.getId())});
	}

}
