package no.simula.des.dao.impl;

import java.util.List;

import no.simula.ReportBean;
import no.simula.des.dao.ReportDao;
import no.simula.rowmapper.ReportBeanRowMapper;
import no.simula.rowmapper.ReportRowRowMapper;

public class JdbcReportDao extends AbstractJdbcDao implements ReportDao {

	private static final String INSERT_REPORT_QUERY= "INSERT INTO reports (name, owner_id) VALUES (?, ?)";
	private static final String DELETE_REPORT_QUERY= "DELETE FROM reports WHERE id = ?";
	private static final String UPDATE_REPORT_QUERY= "UPDATE reports SET name = ?, owner_id = ? WHERE id = ?";
	private static final String SELET_SINGLE_REPORT_QUERY = "SELECT id, name, owner_id FROM reports WHERE id = ?";
	private static final String SELET_ALL_REPORTS_QUERY = "SELECT id, name, owner_id FROM reports ORDER BY name";
	private static final String SELECT_REPORT_BY_NAME_AND_OWNER_ID= "SELECT id, name, owner_id FROM reports WHERE name = ? AND owner_id = ?";
	private static final String SELET_ALL_REPORTS_FOR_USER_QUERY = "SELECT id, name, owner_id FROM reports WHERE user_id = ? ORDER BY name";

	private final static JdbcReportDao instance = new JdbcReportDao();
	public static JdbcReportDao getInstance() {
        return instance;
    }
	
	public void createReport(ReportBean reportBean) {
		getJdbcTemplate().update(INSERT_REPORT_QUERY, new Object[] {reportBean.getName(), reportBean.getOwnerId()});
	}

	public void deleteReport(int reportId) {
		getJdbcTemplate().update(DELETE_REPORT_QUERY, new Object[] {new Integer(reportId)});
	}

	public ReportBean loadReport(int reportId) {
		return (ReportBean) getJdbcTemplate().queryForObject(SELET_SINGLE_REPORT_QUERY, new Object[]{new Integer(reportId)}, new ReportBeanRowMapper());
	}

	public ReportBean loadReport(String name, String ownerId) {
		return (ReportBean) getJdbcTemplate().queryForObject(SELECT_REPORT_BY_NAME_AND_OWNER_ID, new Object[]{name, ownerId}, new ReportBeanRowMapper());
	}
	
	public List loadAllReportsOrderByName() {
		return getJdbcTemplate().query(SELET_ALL_REPORTS_QUERY, new ReportBeanRowMapper());
	}
	
	public List loadAllReportsWithGivenCreatorOrderByName(String userId) {
		return getJdbcTemplate().query(SELET_ALL_REPORTS_FOR_USER_QUERY, new Object[] {userId}, new ReportBeanRowMapper());
	}
	
	public void updateReport(ReportBean reportBean) {
		getJdbcTemplate().update(UPDATE_REPORT_QUERY, new Object[] {reportBean.getName(), reportBean.getOwnerId(), new Integer(reportBean.getId())});
	}

	public List executeQuery(String sql, Object[] params) {
		return getJdbcTemplate().query(sql, params, new ReportRowRowMapper());
	}
	
}
