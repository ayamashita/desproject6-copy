package no.simula.des;

import java.io.Serializable;

import no.simula.AbstractJdbcPersistable;

/**
 * This class represents properties of the DES admin module itself. Currently,
 * the only property modelled is the opening welcome text of the admin module.
 */
public class AdminModule extends AbstractJdbcPersistable implements
		Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8570107529891215356L;

	/** Creates a new admin module. */
	public AdminModule() {
	}

	/**
	 * Creates a new admin module with the specified id and opening text.
	 * 
	 * @param id
	 *            the id as given by the persistence service
	 * @param openingText
	 *            the opening text
	 */
	public AdminModule(Integer id, String openingText) {
		this.id = id;
		this.openingText = openingText;
	}

	private String openingText;
	private Integer id;

	/**
	 * Returns the opening text.
	 * 
	 * @return the opening text
	 */
	public String getOpeningText() {
		return (openingText);
	}

	/**
	 * Sets the opening text.
	 * 
	 * @param openingText
	 *            the opening text
	 */
	public void setOpeningText(String openingText) {
		this.openingText = openingText;
	}

	public Integer getIdAsInteger() {
		return (id);
	}

	public void setId(Integer id) {
		this.id = id;
	}
}