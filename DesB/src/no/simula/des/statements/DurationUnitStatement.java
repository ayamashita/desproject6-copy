/*
 * Created on 30.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import no.halogen.statements.ObjectStatementImpl;
import no.simula.des.DurationUnit;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Frdoe Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class DurationUnitStatement extends ObjectStatementImpl {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(DurationUnitStatement.class);

  /**
   * Field BEAN_NAME
   */
  private final String BEAN_NAME = "no.simula.des.DurationUnit";

  /**
   * Field INSERT_COLUMNS
   */
  private final String INSERT_COLUMNS = "du_name";

  /**
   * Field INSERT_VALUES
   */
  private final String INSERT_VALUES = "(?)";

  /**
   * Field KEY
   */
  private final String KEY = "du_id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS = "du_id, du_name";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "du_durationunit";

  /**
   * Field UPDATE_VALUES
   */
  private final String UPDATE_VALUES = "du_name = ?";

  /**
   * Returns the columns and the order they must appear in in the insert statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return INSERT_COLUMNS;
  }

  /**
   * Returns name of the id column
   * 
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    return KEY;
  }

  /**
   * Returns the columns that will be fetched in a select statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    return SELECT_COLUMNS;
  }

  /**
   * Returns the database name of the table
   * 
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    return TABLE_NAME;
  }

  /**
   * Returns the columns and and a '?' to use in a prepared update statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return UPDATE_VALUES;
  }

  /** 
   * Populates the data bean with the result from a query
   * 
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    DurationUnit durationUnit = null;
    try {
      if (getDataBean() != null) {
        durationUnit = (DurationUnit) getDataBean().getClass().newInstance();
      } else {
        durationUnit = new DurationUnit();
      }
    } catch (InstantiationException e) {
      log.error("fetchListResults() - Could not create new duration type data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchListResults() - Could not create new duration type data object");
      e.printStackTrace();
    }

    durationUnit.setId(new Integer(rs.getInt("du_id")));
    durationUnit.setName(rs.getString("du_name"));

    return (durationUnit);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  /**
   * Method getInsertValues
   * @return String
   */
  public String getInsertValues() {
    return (INSERT_VALUES);
  }

  /**
   * As for now, this table is not updated using the application, so this method is not implemented
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
		if (!getConditions().isEmpty()) {
			super.generateValues(pstmt);
		} else {
			DurationUnit durationUnit = (DurationUnit) getDataBean();

			pstmt.setString(1, durationUnit.getName());
		}
  }

}
