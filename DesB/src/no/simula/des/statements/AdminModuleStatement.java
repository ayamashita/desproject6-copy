/*
 * Created on 04.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import no.halogen.statements.ObjectStatementImpl;
import no.simula.des.AdminModule;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class AdminModuleStatement extends ObjectStatementImpl {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(AdminModuleStatement.class);

  /**
   * Field BEAN_NAME
   */
  private final String BEAN_NAME = "String";

  /**
   * Field INSERT_COLUMNS
   */
  private final String INSERT_COLUMNS = "at_content";

  /**
   * Field INSERT_VALUES
   */
  private final String INSERT_VALUES = "(?)";

  /**
   * Field KEY
   */
  private final String KEY = "at_id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS = "at_id, at_content";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "at_adminmoduletext";

  /**
   * Field UPDATE_VALUES
   */
  private final String UPDATE_VALUES = "at_content = ?";

  /**
   * Returns the columns and the order they must appear in in the insert statement
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return INSERT_COLUMNS;
  }

  /**
   * Returns name of the id column
   * 
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    return KEY;
  }

  /**
   * Returns the columns that will be fetched in a select statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    return SELECT_COLUMNS;
  }

  /**
   * Returns the database name of the table
   * 
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    return TABLE_NAME;
  }

  /**
   * Returns the columns and and a '?' to use in a prepared update statement
   * 
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return UPDATE_VALUES;
  }
  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatement#fetchResults(java.sql.ResultSet)
   */
  /**
   * Method fetchResults
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    AdminModule adminModule = null;

    try {
      if (getDataBean() != null) {
        adminModule = (AdminModule) getDataBean().getClass().newInstance();
      } else {
        adminModule = new AdminModule();
      }

    } catch (InstantiationException e) {
      log.error("fetchResults() - Could not create new admin modul data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchResults() - Could not create new admin modul data object");
      e.printStackTrace();
    }

    adminModule.setId(new Integer(rs.getInt("at_id")));
    adminModule.setOpeningText(rs.getString("at_content"));

    return (adminModule);
  }

  /**
   * Populates the prepared statement string for an update pr insert statement with values from the data bean
   * 
   * Used e.g. by INSERT statements
   * 
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    } else {
      AdminModule adminModule = (AdminModule) getDataBean();

      pstmt.setString(1, adminModule.getOpeningText());
    }
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  /**
   * Method getInsertValues
   * @return String
   */
  public String getInsertValues() {
    return (INSERT_VALUES);
  }

}
