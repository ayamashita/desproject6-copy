/*
 * Created on 04.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.statements;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import no.halogen.statements.ObjectStatementImpl;
import no.simula.Privilege;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Frode Langseth
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class PrivilegeStatement extends ObjectStatementImpl {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(PrivilegeStatement.class);

  /**
   * Field KEY
   */
  private final String KEY = "pr_id";

  /**
   * Field SELECT_COLUMNS
   */
  private final String SELECT_COLUMNS = "pr_id, pr_name";

  /**
   * Field TABLE_NAME
   */
  private final String TABLE_NAME = "pr_privilege";

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  /**
   * Method getInsertColumnNames
   * @return String
   * @see no.halogen.statements.Entity#getInsertColumnNames()
   */
  public String getInsertColumnNames() {
    return null;
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#getInsertValues()
   */
  /**
   * Method getInsertValues
   * @return String
   */
  public String getInsertValues() {
    return null;
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getKey()
   */
  /**
   * Method getKey
   * @return String
   * @see no.halogen.statements.Entity#getKey()
   */
  public String getKey() {
    return (KEY);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  /**
   * Method getSelectColumnNames
   * @return String
   * @see no.halogen.statements.Entity#getSelectColumnNames()
   */
  public String getSelectColumnNames() {
    return (SELECT_COLUMNS);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getTableName()
   */
  /**
   * Method getTableName
   * @return String
   * @see no.halogen.statements.Entity#getTableName()
   */
  public String getTableName() {
    return (TABLE_NAME);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  /**
   * Method getUpdateValuesString
   * @return String
   * @see no.halogen.statements.Entity#getUpdateValuesString()
   */
  public String getUpdateValuesString() {
    return null;
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatement#fetchResults(java.sql.ResultSet)
   */
  /**
   * Method fetchResults
   * @param rs ResultSet
   * @return Object
   * @throws SQLException
   * @see no.halogen.statements.ObjectStatement#fetchResults(ResultSet)
   */
  public Object fetchResults(ResultSet rs) throws SQLException {
    Privilege privilege = null;

    try {
      if (getDataBean() != null) {
        privilege = (Privilege) getDataBean().getClass().newInstance();
      } else {
        privilege = new Privilege();
      }
    } catch (InstantiationException e) {
      log.error("fetchListResults() - Could not create new privilege data object");
      e.printStackTrace();
    } catch (IllegalAccessException e) {
      log.error("fetchListResults() - Could not create new privilege data object");
      e.printStackTrace();
    }

    privilege.setId(new Integer(rs.getInt("pr_id")));
    privilege.setName(rs.getString("pr_name"));

    return (privilege);
  }

  /* (non-Javadoc)
   * @see no.halogen.statements.ObjectStatementImpl#generateValues(java.sql.PreparedStatement)
   */
  /**
   * Method generateValues
   * @param pstmt PreparedStatement
   * @throws SQLException
   */
  public void generateValues(PreparedStatement pstmt) throws SQLException {
    if (!getConditions().isEmpty()) {
      super.generateValues(pstmt);
    } else {
      Privilege entity = (Privilege) getDataBean();

      pstmt.setInt(1, entity.getIdAsInteger().intValue());
      pstmt.setString(2, entity.getName());
    }
  }

}
