package no.simula.des.presentation.web;

import java.io.IOException;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.File;
import no.simula.des.StudyMaterial;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/** Returns the content of the file as specified by the path info of the
 * request from <CODE>Simula</CODE>.
 * <p>
 * The format of the path info (the path after the path to the servlet itself)
 * should be /&lt;id-of-study-material&gt;/&lt;file-name-with-extension&gt;
 * <p>
 * I.e.: http://www.simula.no/des/file<b>/23/questionnaire.doc</b>
 *
 * @author Stian Eide
 */
public class DownloadServlet extends HttpServlet {

  private static Log log = LogFactory.getLog(DownloadServlet.class);
    
  /** Initializes the servlet.
   */
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    
  }
  
  /** Destroys the servlet.
   */
  public void destroy() {
    
  }
  
  /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
   * @param request servlet request
   * @param response servlet response
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    
    try {
      response.setContentType("application/x-download");
      ServletOutputStream out = response.getOutputStream();
      StringTokenizer tokenizer = new StringTokenizer(request.getPathInfo(), "/");
      if(log.isDebugEnabled()) {
        log.debug("The Path Info has " + tokenizer.countTokens() + " tokens.");
      }
      Integer id = null;
      if(tokenizer.hasMoreTokens()) {
        id = Integer.valueOf(tokenizer.nextToken());
        log.debug("File Id: " + id);
      }
      if(id != null) {

        Simula simula = SimulaFactory.getSimula();
        StudyMaterial material = simula.getStudyMaterial(id);
        
        if(material instanceof File) {
          File file = (File)material;        
          response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
          out.write(file.getContent());
        }
      }
      out.close();
    }
    catch(Exception e) {
      log.error("Could not load file.", e);
      throw new ServletException("Could not load file.");
    }
  }
  
  /** Handles the HTTP <code>GET</code> method.
   * @param request servlet request
   * @param response servlet response
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    processRequest(request, response);
  }
  
  /** Handles the HTTP <code>POST</code> method.
   * @param request servlet request
   * @param response servlet response
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    processRequest(request, response);
  }
  
  /** Returns a short description of the servlet.
   */
  public String getServletInfo() {
    return "Short description";
  }  
}