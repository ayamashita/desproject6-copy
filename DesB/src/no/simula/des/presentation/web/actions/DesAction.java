/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.presentation.web.actions;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.Person;
import no.simula.persistence.factory.PersistenceObjectsFactory;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;


/**
 * This abstract class is used as a super class for all Struts actions that
 * may need authentication. Hence all authentication mechanisms is located here.
 */
public abstract class DesAction extends Action {
	
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(DesAction.class);
	

    //Set this vatiable i the subclass to controll access level
    protected int min_access_level = 2;

    protected Person getLoggedUser(HttpServletRequest request) {
    	Principal principal = request.getUserPrincipal();
    	if (principal != null) {
        	String id = principal.getName();
        	return PersistenceObjectsFactory.getPersistancePerson().findByKeyWithPriviliges(id);
    	}
    	return null;
    }
    

    
    public ActionForward execute(ActionMapping mapping, ActionForm form,
        HttpServletRequest request, HttpServletResponse response)
        throws Exception {
        
    	Person user = getLoggedUser(request);

        LOG.debug("The user is: " + user);

        if (user != null) {
           	LOG.debug(getAccessLevel() + " <-- getAccessLevel()");
            if (user.hasPrivilege(getAccessLevel())) {
                return executeAuthenticated(mapping, form, request, response);
            } else {
                request.setAttribute("AccessLevel",
                    new Integer(getAccessLevel()));
                LOG.info("insufficient privileges for " + user);
                return (mapping.findForward("notAuthenticated"));
            }
        }

        return (mapping.findForward("logon"));

        //failure: login screen / frontpage?
    }

    public ActionForward executeAuthenticated(ActionMapping mapping,
        ActionForm form, HttpServletRequest request,
        HttpServletResponse response) throws Exception {
        return (mapping.findForward("logon"));
    }

    public abstract int getAccessLevel();
}
