package no.simula.des.presentation.web.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.Study;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/** Display study details.
 *
 * The action forward <CODE>view</CODE> is returned to view the study and
 * <CODE>return</CODE> is returned if the action is cancelled. This will be the
 * case for the "back" button in the JSP-view.
 */
public class StudiesViewAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {
        
    if(isCancelled(request)) {
      return(mapping.findForward("return"));
    }
    
    Simula simula = SimulaFactory.getSimula();

    Integer id = Integer.valueOf(request.getParameter("id"));
    Study study = simula.getStudy(id);
    request.setAttribute(Constants.FORM_STUDY, study);
          
    return(mapping.findForward("view"));
  }
}