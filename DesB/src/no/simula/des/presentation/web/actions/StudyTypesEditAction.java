package no.simula.des.presentation.web.actions;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.StudyType;
import no.simula.des.presentation.web.forms.SimpleForm;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/** Handles creation of new study types, editing existing study types and
 * saving changes in both cases.
 * <p>
 * The study type will not be saved if no name is specified or if the name is equal
 * to another study type's name.
 * <p>
 * Returns <CODE>edit</CODE> when the study type form should be displayed and
 * <CODE>return</CODE> when cancel is hit or the changes have been successfully
 * saved.
 */
public class StudyTypesEditAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {

    ///////////////////////////////////////////////////////
    // Inputs that redirects the user to other
    // actions than this one.
    
    if(isCancelled(request)) {
      return(mapping.findForward("return"));
    }
        
    SimpleForm form = (SimpleForm)actionForm;
    ActionErrors errors = null;
    Simula simula = SimulaFactory.getSimula();
    
    if(saveChanges(request)) {

      errors = validate(simula, form);
      if(errors == null) {
        
        StudyType studyType;

        // Get existing study type from cache to allow tables in session to be 
        // updated automatically (they point to the same object in memory)
        if(isNewStudyType(form)) {
          studyType = new StudyType();
        }
        else {
          studyType = simula.getStudyType(form.getId());
        }
        BeanUtils.copyProperties(studyType, form);
        
        if(isNewStudyType(form)) {
          simula.addStudyType(studyType);
        }
        else {
          studyType.setId(form.getId());
          simula.storeStudyType(studyType);
        }

        return(mapping.findForward("return"));
      }
    }

    if(isInitialRequest(request)) {      
      if(isNew(request)) {
        form.setId(null);
        form.setName(null);
      }
      else {
        Integer id = Integer.valueOf(request.getParameter("id"));
        BeanUtils.copyProperties(form, simula.getStudyType(id));
      }
    }

    if(errors != null) {
      saveErrors(request, errors);
    }
    return(mapping.findForward("edit"));
  }

  ///////////////////////////////////////////////////////////
  // Helper methods to make main method cleaner and more
  // readable.

  private ActionErrors validate(Simula simula, SimpleForm form) throws Exception {
    ActionErrors errors = new ActionErrors();

    if(form.getName() == null || form.getName().length() == 0) {
      ActionError error = new ActionError("errors.required", "name");
      errors.add("name", error);
    }
    
    for(Iterator i = simula.getStudyTypes().iterator(); i.hasNext();) {
      StudyType studyType = (StudyType)i.next();
      if(
        (
          form.getId() == null ||
          (form.getId() != null && !form.getId().equals(studyType.getIdAsInteger()))
        ) &&
        studyType.getName().trim().equals(form.getName().trim())
      ) {
        ActionError error = new ActionError("errors.name.equal", "study type", form.getName());
        errors.add("name", error);
        break;
      }
    }
        
    if(errors.isEmpty()) {
      return(null);
    }
    else {
      return(errors);
    }
  }
  
  private boolean isInitialRequest(HttpServletRequest request) {
    return(request.getParameter("new") != null || request.getParameter("id") != null);
  }
  
  private boolean isNew(HttpServletRequest request) {
    return(request.getParameter("new") != null);
  }

  private boolean attachFile(HttpServletRequest request) {
    return(request.getParameter("attach") != null);
  }

  private boolean removeFile(HttpServletRequest request) {
    return(request.getParameter("remove") != null);
  }
  
  private boolean uploadFile(HttpServletRequest request) {
    return(request.getParameter("upload") != null);
  }
  
  private boolean attachIsCancelled(HttpServletRequest request) {
    return(request.getParameter("attach-cancel") != null);
  }

  private boolean saveChanges(HttpServletRequest request) {
    return(request.getParameter("save") != null);
  }

  private boolean isNewStudyType(SimpleForm form) {
    return(form.getId() == null);
  }
}