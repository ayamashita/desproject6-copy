package no.simula.des.presentation.web.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.AdminModule;
import no.simula.des.presentation.web.forms.AdminModuleForm;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/** Takes care of copying the content from the data bean to the form bean
 * for editing and copying the data back for saving.
 * <p>
 * Returns <CODE>edit</CODE> action forward for displaying the form and
 * <CODE>return</CODE> when the process is either saved or cancelled.
 */
public class OpeningTextEditAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {
        
    if(isCancelled(request)) {
      return(mapping.findForward("return"));
    }
        
    AdminModuleForm form = (AdminModuleForm)actionForm;
    Simula simula = SimulaFactory.getSimula();
    AdminModule admin;
    
    if(saveChanges(request)) {
      admin = new AdminModule();
      BeanUtils.copyProperties(admin, form);
      simula.storeAdminModule(admin);
      return(mapping.findForward("return"));
    }
    
    admin = simula.getAdminModule();
    if(admin == null) {
      admin = new AdminModule();
    }
    BeanUtils.copyProperties(form, admin);

    return(mapping.findForward("edit"));
  }
  
  private boolean saveChanges(HttpServletRequest request) {
    return(request.getParameter("save") != null);
  }
}