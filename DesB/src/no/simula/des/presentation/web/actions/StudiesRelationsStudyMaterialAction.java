package no.simula.des.presentation.web.actions;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.halogen.struts.ActionForwardUtil;
import no.halogen.utils.table.ActionTableColumn;
import no.halogen.utils.table.SortableTableColumn;
import no.halogen.utils.table.Table;
import no.halogen.utils.table.TableRenderer;
import no.halogen.utils.table.TableSourceList;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.File;
import no.simula.des.Link;
import no.simula.des.StudyMaterial;
import no.simula.des.StudyMaterialManager;
import no.simula.des.presentation.web.StudyMaterialTypeColumnDecorator;
import no.simula.des.presentation.web.forms.StudyForm;
import no.simula.des.presentation.web.forms.StudyMaterialForm;
import no.simula.des.presentation.web.forms.StudyStudyMaterialRelationForm;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/** Populate a table with all study material that is not attached to the current study
 * and a list of attached study material.
 * <p>
 * Attach-requests move the given study material from the table to the list and
 * detach-requests move study material the other way.
 * <p>
 * If the user selects ok, the changes are updated in <CODE>StudyForm</CODE>, so
 * that the changes will be stored permanently when the study is saved.
 * <p>
 * This action also handles creation of new study material. This will be saved
 * permanently immidiatly and it will be attached to the study in the same way as
 * described for other study material above.
 * <p>
 * The study material will not be saved if no name is specified or if the name is equal
 * to another study material's name.
 * <p>
 * Returns <CODE>manage</CODE> action forward to display the attach/detach view,
 * and <CODE>return</CODE> when ok or cancel is chosen.
 */
public class StudiesRelationsStudyMaterialAction extends Action {

  private StudyMaterialManager smm = StudyMaterialManager.getInstance();

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {

    if(isCancelled(request)) {
      return(mapping.findForward("return"));
    }
    
    StudyStudyMaterialRelationForm form = (StudyStudyMaterialRelationForm)actionForm;
    ActionErrors errors = null;
    Simula simula = SimulaFactory.getSimula();
    Table table = (Table)request.getSession().getAttribute(Constants.TABLE_STUDY_RELATIONS_STUDYMATERIAL);

    if(form.getStudyMaterial() == null) {
      form.setStudyMaterial(new StudyMaterialForm());
    }
                
    if(editStudyMaterial(request)) {
      ActionForward forward = mapping.findForward("edit");
      forward = ActionForwardUtil.addRequestParameter(forward, request, "id");
      return(forward);
    }

    if(newStudyMaterial(request)) {
      return(mapping.findForward("new"));
    }
        
    if(isOk(request)) {
      StudyForm studyForm = (StudyForm)request.getSession().getAttribute(Constants.FORM_STUDY);
      studyForm.setStudyMaterial(new ArrayList(form.getAttachedStudyMaterial()));
      return(mapping.findForward("return"));
    }

    if(table == null) {
      List columns = new ArrayList(2);
      SortableTableColumn description = new SortableTableColumn("description", "studymaterial.table.heading.description", null, null, null, "/adm/studies/relations/studymaterial?edit=true", "id", "id", new Integer(100));
      SortableTableColumn type = new SortableTableColumn("type", "studymaterial.table.heading.type", null);
      StudyMaterialTypeColumnDecorator studyMaterialDecorator = new StudyMaterialTypeColumnDecorator();
      Properties resource = ((TableRenderer)request.getSession().getServletContext().getAttribute(Constants.TABLE_RENDERER)).getResource();
      studyMaterialDecorator.setResource(resource);
      type.setDecorator(studyMaterialDecorator);
      columns.add(description);
      columns.add(type);
      columns.add(new ActionTableColumn(null, "studymaterial.table.button.attach", null, null, null, "/adm/studies/relations/studymaterial", Constants.PARAMETER_ATTACH, "id", new Integer(20)));
      table = new Table(columns, new TableSourceList(form.getDetachedStudyMaterial()), 10, description);
      request.getSession().setAttribute(Constants.TABLE_STUDY_RELATIONS_STUDYMATERIAL, table);
    }
    
    if(isInitialRequest(request)) {
      StudyForm studyForm = (StudyForm)request.getSession().getAttribute(Constants.FORM_STUDY);
      form.setAttachedStudyMaterial(new ArrayList(studyForm.getStudyMaterial()));
      form.setDetachedStudyMaterial(new ArrayList(simula.getStudyMaterials()));
      form.getDetachedStudyMaterial().removeAll(form.getAttachedStudyMaterial());
      if(table != null) {
        table.setSource(new TableSourceList(form.getDetachedStudyMaterial()));
        table.sortBy("description", true);
      }
      resetStudyMaterial(form);
    }

    else if(attachStudyMaterial(request)) {
      Integer id = new Integer(request.getParameter(Constants.PARAMETER_ATTACH));
      StudyMaterial studyMaterial = simula.getStudyMaterial(id);
      if(!form.getAttachedStudyMaterial().contains(studyMaterial)) {
        form.getAttachedStudyMaterial().add(studyMaterial);
        form.getDetachedStudyMaterial().remove(studyMaterial);
      }
    }

    else if(attachNewStudyMaterial(request)) {

      errors = validate(simula, form.getStudyMaterial());
      if(errors == null) {
        
        StudyMaterial studyMaterial = smm.createStudyMaterial(form.getStudyMaterial().getType());

        studyMaterial.setDescription(form.getStudyMaterial().getDescription());
        if(form.getStudyMaterial().getType().equals(File.TYPE)) {
          File file = (File)studyMaterial;
          file.setContent(form.getStudyMaterial().getRemoteFile().getFileData());
          file.setName(form.getStudyMaterial().getRemoteFile().getFileName());
        }
        else {
          Link link = (Link)studyMaterial;
          link.setUrl(form.getStudyMaterial().getUrl());
        }
        studyMaterial = simula.addStudyMaterial(studyMaterial);
        form.getAttachedStudyMaterial().add(studyMaterial);
        form.getDetachedStudyMaterial().remove(studyMaterial);
        resetStudyMaterial(form);
      }
    }
    
    else if(detachStudyMaterial(request)) {
      Integer id = new Integer(request.getParameter(Constants.PARAMETER_DETACH));
      StudyMaterial studyMaterial = simula.getStudyMaterial(id);
      if(!form.getDetachedStudyMaterial().contains(studyMaterial)) {
        form.getDetachedStudyMaterial().add(studyMaterial);
        form.getAttachedStudyMaterial().remove(studyMaterial);
        table.doSort();
      }
    }

    else if(removeFile(request)) {
      if(form.getStudyMaterial().getRemoteFile() != null) {
        form.getStudyMaterial().getRemoteFile().destroy();
        form.getStudyMaterial().setRemoteFile(null);
      }
      form.getStudyMaterial().setContent(null);
      form.getStudyMaterial().setName(null);
    }
        
    String sortBy = request.getParameter("sortBy");
    if(sortBy != null && sortBy.length() > 0) {
      table.sortBy(sortBy);
    }

    String page = request.getParameter("page");
    if(page != null && page.length() > 0) {
      table.setCurrentPage(Integer.parseInt(page));
    }

    if(errors != null) {
      saveErrors(request, errors);
    }
    
    return(mapping.findForward("manage"));
  }

  ///////////////////////////////////////////////////////////
  // Helper methods to make main method cleaner and more
  // readable.

  private void resetStudyMaterial(StudyStudyMaterialRelationForm form) {
    form.getStudyMaterial().setTypes(smm.getTypes());
    form.getStudyMaterial().setId(null);
    form.getStudyMaterial().setName(null);
    form.getStudyMaterial().setRemoteFile(null);
    form.getStudyMaterial().setDescription(null);
    form.getStudyMaterial().setUrl(null);      
    form.getStudyMaterial().setType((String)StudyMaterialManager.TYPES.get(0));
  }
  
  private ActionErrors validate(Simula simula, StudyMaterialForm form) throws Exception {
    ActionErrors errors = new ActionErrors();
    
    if(form.getDescription() == null || form.getDescription().length() == 0) {
      ActionError error = new ActionError("errors.required", "description");
      errors.add("url", error);
    }
    
    if(form.getType().equals(File.TYPE)) {
      if(form.getRemoteFile() != null && form.getRemoteFile().getFileSize() == 0 && form.getName() == null) {
        ActionError error = new ActionError("errors.studymaterial.file");
        errors.add("remoteFile", error);
      }
    }
    
    else {
      if(form.getUrl() == null || form.getUrl().length() == 0) {
        ActionError error = new ActionError("errors.studymaterial.link");
        errors.add("url", error);
      }

      if(form.getUrl().indexOf("://") == -1) {
        ActionError error = new ActionError("errors.studymaterial.link.protocol");
        errors.add("url", error);
      }      
    }
    
    if(errors.isEmpty()) {
      return(null);
    }
    else {
      return(errors);
    }
  }
  
  private boolean isInitialRequest(HttpServletRequest request) {
    return(request.getParameter("initial") != null);
  }

  private boolean attachNewStudyMaterial(HttpServletRequest request) {
    return(request.getParameter("studyMaterial.attach") != null);
  }
  
  private boolean attachStudyMaterial(HttpServletRequest request) {
    return(request.getParameter(Constants.PARAMETER_ATTACH) != null);
  }

  private boolean detachStudyMaterial(HttpServletRequest request) {
    return(request.getParameter(Constants.PARAMETER_DETACH) != null);
  }

  private boolean isOk(HttpServletRequest request) {
    return(request.getParameter("ok") != null);
  }

  private boolean editStudyMaterial(HttpServletRequest request) {
    return(request.getParameter("edit") != null);
  }
  
  private boolean newStudyMaterial(HttpServletRequest request) {
    return(request.getParameter("new") != null);
  }
  
  private boolean removeFile(HttpServletRequest request) {
    return(request.getParameter("studyMaterial.remove") != null);
  }
}