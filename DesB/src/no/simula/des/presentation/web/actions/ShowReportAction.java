/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.presentation.web.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.ReportBean;
import no.simula.des.presentation.web.forms.ReportForm;
import no.simula.des.service.factory.ServiceFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.web.bind.ServletRequestUtils;

public class ShowReportAction extends SecuredReportDesAction {
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(ShowReportAction.class);

	public int getAccessLevel() {
		LOG.debug("ShowReportsAction.getAccessLevel()");
		return 1;
	}

	public ActionForward executeAuthenticated(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		int reportId = ServletRequestUtils.getIntParameter(request, "report", -1);
		if (reportId > 0) {
			ReportForm reportForm = (ReportForm) form;
			List report = ServiceFactory.getReportService().executeReport(reportId);
			ReportBean currentReport = ServiceFactory.getReportService().loadReportWithParameters(reportId);
			reportForm.setCurrentReport(currentReport);
			reportForm.setExecutedReport(report);
			return mapping.findForward("success");
		}
		return mapping.findForward("faliture");
	}
	
}
