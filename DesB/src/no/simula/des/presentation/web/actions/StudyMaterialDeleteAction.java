package no.simula.des.presentation.web.actions;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.StudyMaterial;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/** Controls the flow of the delete study materials process. The request may be in
 * two states:
 * <ul>
 *  <li>
 *    A list of deletable study materials exists in session
 *  </li>
 *  <li>
 *    If not an <CODE>error</CODE> action forward is issued
 *  </li>
 * </ul>
 *
 * Next, a study material, if specified by the <CODE>id</CODE> request parameter,
 * is deleted unless it is used by some study.
 * <p>
 * The deleted study material is deleted from deletable study materials, and the next
 * in this list is processed. If the list is empty a <CODE>return</CODE> action
 * forward is returned.
 * <p>
 * If the study material is unused, it is added as deletable to the request, if not,
 * it is added as not delatable along with a list of studies, which use this
 * study material. Then a <CODE>confirm</CODE> action forward is returned.
 */
public class StudyMaterialDeleteAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {
    
    if(isCancelled(request)) {
      return(mapping.findForward("return"));
    }
    
    Simula simula = SimulaFactory.getSimula();

    // Get deletable study materials from the session.
    List deletableStudyMaterial = (List)request.getSession().getAttribute(Constants.DELETE_STUDYMATERIAL);
    
    // If the list is null at this point, we have an illegal request and return an error.
    if(deletableStudyMaterial == null) {
      return(mapping.findForward("error"));
    }

    // Lets delete a study as specified in the request and display either a new
    // confirmation if there are more studies to be deleted or the new list of
    // studies.
    else {
      String deleteIdString = request.getParameter("id");
      if(!skipStudyMaterial(request) && deleteIdString != null) {
        Integer deleteId = Integer.valueOf(deleteIdString);
        if(simula.getStudiesByStudyMaterial(deleteId) == null) {
          simula.deleteStudyMaterial(deleteId);
        }
      }
      if(deletableStudyMaterial.isEmpty()) {
        return(mapping.findForward("return"));
      }
      else {
        StudyMaterial studyMaterial = (StudyMaterial)deletableStudyMaterial.remove(0);
        request.getSession().setAttribute(Constants.DELETE_STUDYMATERIAL, deletableStudyMaterial);
        List containingStudies = simula.getStudiesByStudyMaterial(studyMaterial.getIdAsInteger());
        if(containingStudies == null) {
          request.setAttribute(Constants.DELETE_THIS_STUDYMATERIAL, studyMaterial);
          request.setAttribute(Constants.DONT_DELETE_THIS_STUDYMATERIAL, null);
          request.setAttribute(Constants.BEAN_STUDIES, null);
        }
        else {
          request.setAttribute(Constants.DELETE_THIS_STUDYMATERIAL, null);
          request.setAttribute(Constants.DONT_DELETE_THIS_STUDYMATERIAL, studyMaterial);
          request.setAttribute(Constants.BEAN_STUDIES, containingStudies);
        }
        return(mapping.findForward("confirm"));
      }
    }
  }
  
  private boolean skipStudyMaterial(HttpServletRequest request) {
    return(request.getParameter("skip") != null);
  }
}