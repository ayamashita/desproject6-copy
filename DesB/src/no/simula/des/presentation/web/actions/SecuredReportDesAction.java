package no.simula.des.presentation.web.actions;

import javax.servlet.http.HttpServletRequest;

import no.simula.des.presentation.web.forms.ReportForm;

import org.apache.struts.action.ActionForm;


/**
 * @author mateusz
 *
 */
public abstract class SecuredReportDesAction extends SecuredDesAction {
	

	protected boolean checkPermissions(ActionForm form,
			HttpServletRequest request) {

		ReportForm reportForm = (ReportForm) form;
		if (reportForm.getCurrentReport() != null) {
			return reportForm.getCurrentReport().hasPermision(request.getUserPrincipal());
		}
		return false;
	}


}
