/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.presentation.web.actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.ReportBean;
import no.simula.des.presentation.web.forms.ReportForm;
import no.simula.des.service.factory.ServiceFactory;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.web.bind.ServletRequestUtils;

public class EditReportAction extends SecuredReportDesAction {
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(EditReportAction.class);

	public int getAccessLevel() {
		LOG.debug("EditReportAction.getAccessLevel()");
		return 1;
	}

	public ActionForward executeAuthenticated(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		LOG.debug("EditReportAction.executeAuthenticated()");
		int reportId = ServletRequestUtils.getIntParameter(request, "report", -1);
		if (reportId > 0) {
			ReportForm reportForm = (ReportForm) form;

			ReportBean reportBean = ServiceFactory.getReportService().loadReportWithParameters(reportId);
			reportForm.setCurrentReport(reportBean);
		}
		return mapping.findForward("success");
	}
	
}
