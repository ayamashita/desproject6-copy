/*
 * Created on 30.sep.2003
 *
 */
package no.simula.des.presentation.web.actions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.ReportParameterBean;
import no.simula.des.presentation.web.forms.ReportForm;
import no.simula.des.service.factory.ServiceFactory;

import org.apache.commons.lang.StringUtils;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.springframework.dao.DataAccessException;

public class SaveReportAction extends SecuredReportDesAction {
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(SaveReportAction.class);

	public int getAccessLevel() {
		LOG.debug("ShowReportsAction.getAccessLevel()");
		return 1;
	}

	public ActionForward executeAuthenticated(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {

		LOG.debug("ShowReportsAction.executeAuthenticated()");
		ReportForm reportForm = (ReportForm) form;

		ActionErrors errors = reportForm.performValidation(mapping, request);
		if (errors != null) {
            saveErrors(request, errors);
            return mapping.findForward("stillEdit");
		}
		
		if (reportForm.getAction() == ReportForm.SAVE_REPORT_ACTION) {
			
			fillReportParameters(request, reportForm);
			reportForm.getCurrentReport().setOwnerId(getLoggedUser(request).getIdAsString());

			try {
				if (reportForm.getCurrentReport().isPersistent()) {
					ServiceFactory.getReportService().updateReport(reportForm.getCurrentReport());
				} else {
					ServiceFactory.getReportService().createReport(reportForm.getCurrentReport());
				}
			} catch (DataAccessException e) {
				e.printStackTrace();
				
                errors = new ActionErrors();
                errors.add(null, new ActionError("error.des.report.integrity"));
                saveErrors(request, errors);
                return mapping.findForward("stillEdit");
			}
			
			return mapping.findForward("success");
			//return new ActionForward("/listReports.do");			
		} else if (reportForm.getAction() == ReportForm.ADD_PARAMETER_ACTION) {
			// add parameter
			fillReportParameters(request, reportForm);
			
			List parameters = reportForm.getCurrentReport().getParameters();
			
			// if no responsibles, don't add new ones
			ReportParameterBean parameterBean = (ReportParameterBean) parameters.get(ReportForm.FIRST_RESPONSIBLE_PARAMETER_INDEX);
			if (!parameterBean.isEmpty()) {
				parameters = removeEmptyParameters(parameters);
				parameters.add(new ReportParameterBean(ReportParameterBean.TYPE_OR));
				reportForm.getCurrentReport().setParameters(parameters);
			}
			return mapping.findForward("stillEdit");
		}
		return mapping.findForward("faliture");
	}
	
	private void fillReportParameters(HttpServletRequest request, ReportForm reportForm) {
		int index = 0;

		List parameters = new ArrayList();
		parameters.add(index, reportForm.getCurrentReport().getParameters().get(index));
		index++;
		parameters.add(index, reportForm.getCurrentReport().getParameters().get(index));
		index++;
		reportForm.getCurrentReport().setParameters(parameters);
		
		Set paramNames = request.getParameterMap().keySet();
		for (Iterator iterator = paramNames.iterator(); iterator.hasNext();) {
			String name = (String) iterator.next();
			if (name.startsWith("param_") && name.endsWith("_value")) {
				String value = request.getParameter(name);
				String type = request.getParameter(name.replaceAll("_value", "_type"));
				LOG.debug(type + " <-- type");
				if (!StringUtils.isEmpty(value)) {
					addParameter(reportForm, type, value, index);
					index++;
				}
			}
		}
		// filling selectFields
		String[] selectParameters = reportForm.getSelectparametersAsArray();
		for (int j = 0; j < selectParameters.length; j++) {
			String parameterValue = selectParameters[j];
			addParameter(reportForm, ReportParameterBean.TYPE_SELECT, parameterValue, index);		
			index++;
		}
		
	}

	private void addParameter(ReportForm reportForm, String type, String value, int index) {
		ReportParameterBean parameterBean = new ReportParameterBean();
		parameterBean.setType(type);
		parameterBean.setValue(value);
		parameterBean.setIndex(index);
		parameterBean.setReportBean(reportForm.getCurrentReport());
		reportForm.getCurrentReport().getParameters().add(index, parameterBean);	
	}
	
	private List removeEmptyParameters(List parameters) {
		List out = new ArrayList(parameters.size());
		for (Iterator iterator = parameters.iterator(); iterator.hasNext();) {
			ReportParameterBean bean = (ReportParameterBean) iterator.next();
			// don't delete first 2 parameters
			if (out.indexOf(bean) ==  ReportForm.FIRST_RESPONSIBLE_PARAMETER_INDEX || out.indexOf(bean) ==  ReportForm.ORDER_PARAMETER_INDEX || !bean.isEmpty()) {
				out.add(bean);
			}
		}
		return out;
	}

}
