package no.simula.des.presentation.web.actions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.halogen.struts.ActionForwardUtil;
import no.halogen.utils.table.SortableTableColumn;
import no.halogen.utils.table.Table;
import no.halogen.utils.table.TableRenderer;
import no.halogen.utils.table.TableSource;
import no.halogen.utils.table.TableSourceList;
import no.simula.Simula;
import no.simula.SimulaFactory;
import no.simula.des.presentation.web.StudyMaterialTypeColumnDecorator;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionError;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/** Displays the table of registered study material and handles dispatching of
 * requests to other actions or views used to manage study material.
 * <p>
 * Returns:
 *
 * <ul>
 *  <li><CODE>new</CODE> to display an empty study material form</li>
 *  <li><CODE>edit</CODE> to display a study material form with values</li>
 *  <li><CODE>delete</CODE> to confirm deletion of selected study material</li>
 *  <li><CODE>list</CODE> to display the table of registered study material</li>
 * </ul>
 *
 * Please note that all actions linked from the main view of manage study material
 * is dispatched through this action
 */
public class StudyMaterialManageAction extends Action {

  /** Execute this action
   * @param mapping the action mapping that match the current request
   * @param actionForm the action form linked to the current action mapping
   * @param request the current servlet request object
   * @param response the current servlet response object
   * @throws Exception if the action could not be successfully completed
   * @return an action forward from the action mapping depending on the outcome of this
   * action
   */  
  public ActionForward execute(
    ActionMapping mapping,
    ActionForm actionForm,
    HttpServletRequest request,
    HttpServletResponse response)
    throws Exception
  {
    Simula simula = SimulaFactory.getSimula();
    Table table = (Table)request.getSession().getAttribute(Constants.TABLE_STUDYMATERIAL);

    if(deleteStudyMaterial(request)) {
      // Firstly, we check if this is an initial delete-request. If it is, the deletable
      // is populated with the ids in the request.
      List deletableStudyMaterialIds = new ArrayList();
      for(Iterator i = request.getParameterMap().keySet().iterator(); i.hasNext();) {
        Object o = i.next();
        if(o instanceof String) {
          String paramName = (String)o;
          if(paramName.startsWith("id[")) {
            String id = paramName.substring(paramName.indexOf("[")+1, paramName.indexOf("]"));
            deletableStudyMaterialIds.add(Integer.valueOf(id));
          }
        }
      }
      if(deletableStudyMaterialIds.isEmpty()) {
        ActionError error = new ActionError("errors.delete.empty", "study material");
        ActionErrors errors = new ActionErrors();
        errors.add(ActionErrors.GLOBAL_ERROR, error);
        saveErrors(request, errors);
      }
      else {
        List deletableStudyMaterial = simula.getStudyMaterialByIds(deletableStudyMaterialIds);
        request.getSession().setAttribute(Constants.DELETE_STUDYMATERIAL, deletableStudyMaterial);
        return(mapping.findForward("delete"));
      }
    }

    if(editStudyMaterial(request)) {
      ActionForward forward = mapping.findForward("edit");
      forward = ActionForwardUtil.addRequestParameter(forward, request, "id");
      return(forward);
    }
      
    if(newStudyMaterial(request)) {
      return(mapping.findForward("new"));
    }

    if(table == null) {
      TableSource source = new TableSourceList(simula.getStudyMaterials());
      List columns = new ArrayList(2);
      SortableTableColumn description = new SortableTableColumn("description", "studymaterial.table.heading.description", null, null, null, "/adm/studymaterial/manage?edit=true", "id", "id", new Integer(100));
      SortableTableColumn type = new SortableTableColumn("type", "studymaterial.table.heading.type", null);
      StudyMaterialTypeColumnDecorator studyMaterialDecorator = new StudyMaterialTypeColumnDecorator();
      Properties resource = ((TableRenderer)request.getSession().getServletContext().getAttribute(Constants.TABLE_RENDERER)).getResource();
      studyMaterialDecorator.setResource(resource);
      type.setDecorator(studyMaterialDecorator);
      columns.add(description);
      columns.add(type);
      table = new Table(columns, source, 10, description);
      request.getSession().setAttribute(Constants.TABLE_STUDYMATERIAL, table);
    }

    String sortBy = request.getParameter("sortBy");
    if(sortBy != null && sortBy.length() > 0) {
      table.sortBy(sortBy);
    }

    String page = request.getParameter("page");
    if(page != null && page.length() > 0) {
      table.setCurrentPage(Integer.parseInt(page));
    }

    return(mapping.findForward("list"));
  }

  private boolean deleteStudyMaterial(HttpServletRequest request) {
    return(request.getParameter("delete") != null);
  }

  private boolean newStudyMaterial(HttpServletRequest request) {
    return(request.getParameter("new") != null);
  }

  private boolean editStudyMaterial(HttpServletRequest request) {
    return(request.getParameter("edit") != null);
  }
}