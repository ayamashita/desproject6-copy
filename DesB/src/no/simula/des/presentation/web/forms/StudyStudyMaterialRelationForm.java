package no.simula.des.presentation.web.forms;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

/** Contains data that is used to attach and detach study material to and from
 * studies. Additionally, this form contains the <CODE>StudyMaterialForm</CODE> so
 * study material may be created directly from the view that uses this form.
 */
public class StudyStudyMaterialRelationForm extends ActionForm {
    
  private ArrayList detachedStudyMaterial;
  private ArrayList attachedStudyMaterial;
  
  /** Holds value of property studyMaterial. */
  private StudyMaterialForm studyMaterial;
  
//  public void reset(ActionMapping mapping, HttpServletRequest request) {
//    studyMaterial = new StudyMaterialForm();
//  }
  
  /** Getter for property attachedStudyMaterial.
   * @return Value of property attachedStudyMaterial.
   *
   */
  public List getAttachedStudyMaterial() {
    return(attachedStudyMaterial);
  }  
  
  /** Setter for property attachedStudyMaterial.
   * @param attachedStudyMaterial New value of property attachedStudyMaterial.
   *
   */
  public void setAttachedStudyMaterial(List attachedStudyMaterial) {
    this.attachedStudyMaterial = new ArrayList(attachedStudyMaterial);
  }

  /** Getter for property detachedStudyMaterial.
   * @return Value of property detachedStudyMaterial.
   *
   */
  public List getDetachedStudyMaterial() {
    return(detachedStudyMaterial);
  }
  
  /** Setter for property detachedStudyMaterial.
   * @param detachedStudyMaterial New value of property detachedStudyMaterial.
   *
   */
  public void setDetachedStudyMaterial(List detachedStudyMaterial) {
    this.detachedStudyMaterial = new ArrayList(detachedStudyMaterial);
  }
  
  /** Getter for property studyMaterial.
   * @return Value of property studyMaterial.
   *
   */
  public StudyMaterialForm getStudyMaterial() {
    return this.studyMaterial;
  }
  
  /** Setter for property studyMaterial.
   * @param studyMaterial New value of property studyMaterial.
   *
   */
  public void setStudyMaterial(StudyMaterialForm studyMaterial) {
    this.studyMaterial = studyMaterial;
  }
  
}