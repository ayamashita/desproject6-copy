package no.simula.des.presentation.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import no.simula.Simula;
import no.simula.SimulaFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/** Returns all studies from <CODE>Simula</CODE> in CSV-format as a file.
 *  The file will be named as specified by the path info in the request.
 *
 * @author Stian Eide
 */
public class ExportStudiesServlet extends HttpServlet {

  private static Log log = LogFactory.getLog(ExportStudiesServlet.class);
  
  /** Initializes the servlet.
   */
  public void init(ServletConfig config) throws ServletException {
    super.init(config);
    
  }
  
  /** Destroys the servlet.
   */
  public void destroy() {
    
  }
  
  /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
   * @param request servlet request
   * @param response servlet response
   */
  protected void processRequest(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    try {
      Simula simula = SimulaFactory.getSimula();
      String name = request.getPathInfo().substring(1);

      // Set the headers.
      response.setContentType("application/x-download");
      response.setHeader("Content-Disposition", "attachment; filename=" + name);
      
      PrintWriter out = response.getWriter();
      out.write(simula.getStudiesAsCSV(';'));
      out.close();
    }
    
    catch(Exception e) {
      log.error("Could not export studies.", e);
      throw new ServletException(e);
    }
  }
  
  /** Handles the HTTP <code>GET</code> method.
   * @param request servlet request
   * @param response servlet response
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    processRequest(request, response);
  }
  
  /** Handles the HTTP <code>POST</code> method.
   * @param request servlet request
   * @param response servlet response
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    processRequest(request, response);
  }
  
  /** Returns a short description of the servlet.
   */
  public String getServletInfo() {
    return "Short description";
  }
  
}
