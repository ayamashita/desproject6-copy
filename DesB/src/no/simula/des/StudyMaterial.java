package no.simula.des;

import java.io.Serializable;

import no.simula.AbstractJdbcPersistable;

/** A study material may be attached to study. Currently, two implementations
 * exsist; <CODE>File</CODE> and <CODE>Link</CODE>. All study material have a
 * description and a URL where they may be accessed.
 */
public abstract class StudyMaterial extends AbstractJdbcPersistable implements Serializable {
  
  /** @depricated <CODE>StudyMaterial</CODE> is abstract and cannot be instantiated. */  
  public StudyMaterial() {
  }
  
  /** @depricated <CODE>StudyMaterial</CODE> is abstract and cannot be instantiated. */  
  public StudyMaterial(Integer id) {
    this.id = id;
  }
  
  /** @depricated <CODE>StudyMaterial</CODE> is abstract and cannot be instantiated. */  
  public StudyMaterial(Integer id, String description) {
    this.id = id;
    this.description = description;
  }
  
  Integer id;
  private String description;
  
  /** Returns the URL to this study material.
   * @return the URL to this study material
   */  
  public abstract String getUrl();
  /** Returns the name of the type of this study material.
   * @return the name of the study material type
   */  
  public abstract String getType();
    
  /** Returns the description of this study material.
   * @return the description of this study material
   */
  public String getDescription() {
    return(description);
  }
  
  /** Sets the description of this study material.
   * @param description the description of this study material
   */
  public void setDescription(String description) {
    this.description = description;
  }
  
  public Integer getIdAsInteger() {
    return(id);
  }
  
  public void setId(Integer id) {
    this.id = id;
  }
  
  /** Returns whether two instances of study material are equal. The are equal only if
   * their ids are equal.
   * @param o the other study material
   * @return <CODE>true</CODE> if <CODE>this.id.equals(o.id)</CODE>
   */  
  public boolean equals(Object o) {
    return(((StudyMaterial)o).getIdAsInteger().equals(this.id));
  }  
}