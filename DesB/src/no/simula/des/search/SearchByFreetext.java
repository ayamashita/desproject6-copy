/**
 * @(#) SearchByFreetext.java
 */

package no.simula.des.search;

import no.halogen.search.CompositeCriterium;
import no.halogen.search.Criterium;

public interface SearchByFreetext extends Criterium, CompositeCriterium
{
	
}
