/**
 * @(#) SearchByStudyTypeImpl.java
 */

package no.simula.des.search;

import java.util.Iterator;

import no.halogen.search.CriteriumException;
import no.halogen.search.CriteriumImpl;
import no.halogen.statements.ObjectStatement;
import no.simula.des.statements.StudyStatement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * @author Frode Langseth
 */
public class SearchByStudyTypeImpl extends CriteriumImpl implements SearchByStudyType {
  /**
   * Field log
   */
  private static Log log = LogFactory.getLog(SearchByStudyType.class);

  /**
   * Field DATABASE_COLUMN
   */
  private final String DATABASE_COLUMN = "stu_sty_id";
  /**
   * Field DEFAULT_OPERATOR
   */
  private final String DEFAULT_OPERATOR = new String(" IN ");

  /** 
  	 * Generates the criteria and "?" character as value representators as a part of a SQL where clause
  	 * Due to use of prepared statements, the value attributes must be fetched when a prepared statement is instatiated and will use the criterium
  	 * The method does not generate the start of the clause (WHERE), nor the operator (AND, OR ..)
  	 * @return String
   * @throws CriteriumException
   * @see no.halogen.search.Criterium#generateWhereClause()
   */
  public String generateWhereClause() throws CriteriumException {
    StringBuffer statement = new StringBuffer();

    if (getAttributes() == null) {
      log.error("generateWhereClause - No attributes in SearchByStudyType criterium");
      throw new CriteriumException("Error! No attributes ...");
    }

    statement.append(" ("); // puts paranthesis first and last in the where clause, to make clear the criteria scope

    statement.append(" " + getCriteriumColumn() + " IN (");

    Iterator i = getAttributes().iterator();
    boolean firstIteration = true;
    while (i.hasNext()) {
      if (!firstIteration) { // if more than one type
        statement.append(", ");
      }

      statement.append("?");

      i.next();
      firstIteration = false;
    }

    statement.append("))"); // puts paranthesis first and last in the where clause, to make clear the criteria scope

    return (statement.toString());
  }

  /**
  * @return String
   */
  private String getCriteriumColumn() {
    return DATABASE_COLUMN;
  }

  /* (non-Javadoc)
   * @see no.halogen.search.Criterium#getStatement()
   */
  /**
   * Method getStatement
   * @return ObjectStatement
   * @see no.halogen.search.Criterium#getStatement()
   */
  public ObjectStatement getStatement() {
    return (new StudyStatement());
  }

}
