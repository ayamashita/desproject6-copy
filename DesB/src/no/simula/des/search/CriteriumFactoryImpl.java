/**
 * @(#) CriteriumFactoryImpl.java
 */

package no.simula.des.search;

import java.io.Serializable;

import no.halogen.search.Criterium;
import no.halogen.search.CriteriumFactory;
import no.simula.search.SearchByPublicationAuthor;
import no.simula.search.SearchByPublicationAuthorImpl;
import no.simula.search.SearchByPublicationTitle;
import no.simula.search.SearchByPublicationTitleImpl;

/**
 * Creates a search criterium
 * 
 * @author Frode Langseth
 */
public class CriteriumFactoryImpl implements CriteriumFactory, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5701825861597706755L;

	/**
	 * Method create
	 * 
	 * @param criterium
	 *            String
	 * @return Criterium
	 * @see no.halogen.search.CriteriumFactory#create(String)
	 */
	public Criterium create(String criterium) {
		Criterium concreteCriterium = null;

		if (criterium.equals(SearchByFreetext.class.getName())) {
			concreteCriterium = new SearchByFreetextImpl();
		} else if (criterium.equals(SearchByResponsible.class.getName())) {
			concreteCriterium = new SearchByResponsibleImpl();
		} else if (criterium.equals(SearchByStudyEndDate.class.getName())) {
			concreteCriterium = new SearchByStudyEndDateImpl();
		} else if (criterium.equals(SearchByStudyType.class.getName())) {
			concreteCriterium = new SearchByStudyTypeImpl();
		} else if (criterium.equals(SearchByStudyName.class.getName())) {
			concreteCriterium = new SearchByStudyNameImpl();
		} else if (criterium.equals(SearchByStudyDescription.class.getName())) {
			concreteCriterium = new SearchByStudyDescriptionImpl();
		} else if (criterium.equals(SearchByMaterialTitle.class.getName())) {
			concreteCriterium = new SearchByMaterialTitleImpl();
		} else if (criterium.equals(SearchByPublicationTitle.class.getName())) {
			concreteCriterium = new SearchByPublicationTitleImpl();
		} else if (criterium.equals(SearchByPublicationAuthor.class.getName())) {
			concreteCriterium = new SearchByPublicationAuthorImpl();
		} else if (criterium.equals(SearchByStudyKeyword.class.getName())) {
			concreteCriterium = new SearchByStudyKeywordImpl();
		}

		return concreteCriterium;
	}

}
