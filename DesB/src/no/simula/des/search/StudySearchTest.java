/*
 * Created on 10.nov.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.des.search;

import java.util.Date;

import junit.framework.TestCase;
import no.halogen.search.Search;

/**
 * @author frodel
 *
 * To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
public class StudySearchTest extends TestCase {
  Search studySearch = null;
  SearchByStudyEndDate endDate1 = null;
  SearchByStudyType studyType1 = null;
  SearchByStudyType studyType2 = null;
  SearchByStudyType studyType3 = null;
  SearchByResponsible resposible1 = null;
  SearchByResponsible resposible2 = null;
  SearchByResponsible resposible3 = null;
  SearchByFreetext freetext1 = null;
  SearchByFreetext freetext2 = null;
  SearchByFreetext freetext3 = null;

  public static void main(String[] args) {
  }

  /*
   * @see TestCase#setUp()
   */
  protected void setUp() throws Exception {
    super.setUp();

    studySearch = new StudySearch();

    endDate1 = new SearchByStudyEndDateImpl();
    endDate1.addAttribute(new Date("01/06/2003"));

    studyType1 = new SearchByStudyTypeImpl();
    studyType1.addAttribute(new Integer(2));

    resposible1 = new SearchByResponsibleImpl();
    resposible1.addAttribute(new Integer(15));

    freetext1 = new SearchByFreetextImpl();
    freetext1.addAttribute(new String("leave"));

  }

  public void testDoSearch() {
    studySearch.addCriterium(studyType1);
    /*studySearch.addCriterium(endDate1);*/
    studySearch.addCriterium(freetext1);

    studySearch.doSearch();
  }

}
