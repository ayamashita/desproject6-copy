package no.simula.des.service;

import java.security.Principal;
import java.util.List;

import no.simula.ReportBean;

public interface ReportService {

	/**
	 * @return
	 */
	List loadAllReportsOrderByName();
	
	/**
	 * @param reportId
	 * @return
	 */
	ReportBean loadReportWithParameters(int reportId);
	
	/**
	 * @param peopleBean
	 * @return
	 */
	List LoadReportsForUser(Principal principal);
	
	/**
	 * @param reportBean
	 */
	void createReport(ReportBean reportBean);
	
	/**
	 * @param reportBean
	 */
	void updateReport(ReportBean reportBean);

	/**
	 * @param reportId
	 * @return
	 */
	void deleteReport(int reportId);
	
	/**
	 * @param reportId
	 * @return
	 * @throws Exception 
	 */
	public List executeReport(int reportId) throws Exception;
	
}
