package no.simula.des.service.impl;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import no.halogen.utils.StudyHelper;
import no.simula.NameValueBean;
import no.simula.Person;
import no.simula.Privilege;
import no.simula.Publication;
import no.simula.ReportBean;
import no.simula.ReportColumnBean;
import no.simula.ReportParameterBean;
import no.simula.ReportRowBean;
import no.simula.des.dao.impl.JdbcReportDao;
import no.simula.des.dao.impl.JdbcReportParameterDao;
import no.simula.des.presentation.web.forms.ReportForm;
import no.simula.des.service.ReportService;
import no.simula.persistence.factory.PersistenceObjectsFactory;

public class DefaultReportService implements ReportService {

	
	private static final List PEOPLE_COLUMN_NAMES = Arrays.asList(new String[] {"stu_lasteditedby", "stu_owner"});
	private static final List STUDY_COLUMN_NAMES = Arrays.asList(new String[] {"stu_id"});


	public List loadAllReportsOrderByName() {
		return JdbcReportDao.getInstance().loadAllReportsOrderByName();
	}

	public ReportBean loadReportWithParameters(int reportId) {
		ReportBean reportBean = JdbcReportDao.getInstance().loadReport(reportId);
		List parameters = JdbcReportParameterDao.getInstance().loadReportParametersForReport(reportId);
		for (Iterator iterator = parameters.iterator(); iterator.hasNext();) {
			ReportParameterBean parameterBean = (ReportParameterBean) iterator.next();
			parameterBean.setReportBean(reportBean);
		}
		reportBean.setParameters(parameters);
		return reportBean;
	}

	public List LoadReportsForUser(Principal principal) {
		if (principal == null) {
			throw new IllegalArgumentException("principal == null");
		}

		Person person = PersistenceObjectsFactory.getPersistancePerson().findByKeyWithPriviliges(principal.getName());
	
		if (person != null) {
			if (person.hasPrivilege(Privilege.DB_ADMIN_ID)) {
				return loadAllReportsOrderByName();
			}
			return JdbcReportDao.getInstance().loadAllReportsWithGivenCreatorOrderByName(person.getIdAsString());
		}
		return null;
	}

	public void createReport(ReportBean reportBean) {
		JdbcReportDao.getInstance().createReport(reportBean);		
		String name = reportBean.getName();
		String ownerId = reportBean.getOwnerId();
		List params = reportBean.getParameters();
		reportBean = JdbcReportDao.getInstance().loadReport(name, ownerId);
		reportBean.setParameters(params);
		createParameters(reportBean);

	}

	private void createParameters(ReportBean bean) {
		for (Iterator iterator = bean.getParameters().iterator(); iterator.hasNext();) {
			ReportParameterBean reportParameterBean = (ReportParameterBean) iterator.next();
			reportParameterBean.setReportBean(bean);
			JdbcReportParameterDao.getInstance().createReportParameter(reportParameterBean);
		}
	}
	
	public void updateReport(ReportBean reportBean) {
		JdbcReportDao.getInstance().updateReport(reportBean);		
		JdbcReportParameterDao.getInstance().deleteReportParameters(reportBean.getId());
		createParameters(reportBean);
	}

	public void deleteReport(int reportId) {
		JdbcReportParameterDao.getInstance().deleteReportParameters(reportId);
		JdbcReportDao.getInstance().deleteReport(reportId);
	}

	public List executeReport(int reportId) throws Exception {
		ReportBean reportBean = loadReportWithParameters(reportId);
		String sql = reportBean.getSqlQuery();
		Object[] params = reportBean.getSqlQueryParameters();
		List reportRows = JdbcReportDao.getInstance().executeQuery(sql, params);
		
		List columnNames = reportBean.getColumnNames();
		int columnIndex = 1; // first column contains study ID


		for (Iterator iterator = columnNames.iterator(); iterator.hasNext();) {
			String columnName = ((ReportParameterBean) iterator.next()).getValue();
			if (PEOPLE_COLUMN_NAMES.contains(columnName)) {
				for (Iterator iterator2 = reportRows.iterator(); iterator2.hasNext();) {
					ReportRowBean reportRowBean = (ReportRowBean) iterator2.next();
					ReportColumnBean bean = (ReportColumnBean) reportRowBean.getColumns().get(columnIndex);
					Person person = (Person) PersistenceObjectsFactory.getPersistancePerson().findByKey(bean.getValue());
					if (person != null) {
						bean.setUrl(person.getHomeSiteUrl());
					}
				}
			} else if (STUDY_COLUMN_NAMES.contains(columnName)) {
				for (Iterator iterator2 = reportRows.iterator(); iterator2.hasNext();) {
					ReportRowBean reportRowBean = (ReportRowBean) iterator2.next();
					ReportColumnBean bean = (ReportColumnBean) reportRowBean.getColumns().get(columnIndex);
					Integer id = Integer.valueOf(bean.getValue());
					bean.setUrl(StudyHelper.getUrl(id.intValue()));
				}
			} else if (ReportForm.JOIN_PUBLICATIONS_FILED_VALUE.equals(columnName)) {
				for (Iterator iterator2 = reportRows.iterator(); iterator2.hasNext();) {
					ReportRowBean reportRowBean = (ReportRowBean) iterator2.next();
					ReportColumnBean idBean = (ReportColumnBean) reportRowBean.getColumns().get(0);
					int studyId = Integer.valueOf(idBean.getValue()).intValue();
					ReportColumnBean columnBean = (ReportColumnBean) reportRowBean.getColumns().get(columnIndex);
					columnBean.setNamesAndUrlValues(new ArrayList());
					Collection publications = PersistenceObjectsFactory.getPersistentPublication().findByStudy(new Integer(studyId));
					for (Iterator iterator3 = publications.iterator(); iterator3
							.hasNext();) {
						Publication publicationBean = (Publication) iterator3.next();
						NameValueBean nameValueBean = new NameValueBean(publicationBean.getTitle(), publicationBean.getUrl());
						columnBean.getNamesAndUrlValues().add(nameValueBean);
						
					}
				}
			} else if (ReportForm.JOIN_RESPONSIBLES_FILED_VALUE.equals(columnName)) {
				for (Iterator iterator2 = reportRows.iterator(); iterator2.hasNext();) {
					ReportRowBean reportRowBean = (ReportRowBean) iterator2.next();
					ReportColumnBean idBean = (ReportColumnBean) reportRowBean.getColumns().get(0);
					int studyId = Integer.valueOf(idBean.getValue()).intValue();
					ReportColumnBean columnBean = (ReportColumnBean) reportRowBean.getColumns().get(columnIndex);
					columnBean.setNamesAndUrlValues(new ArrayList());
					Collection responsibles = PersistenceObjectsFactory.getPersistancePerson().findResponsibleByStudy(new Integer(studyId));
					for (Iterator iterator3 = responsibles.iterator(); iterator3
							.hasNext();) {
						Person peopleBean = (Person) iterator3.next();
						NameValueBean nameValueBean = new NameValueBean(peopleBean.getFullName(), peopleBean.getHomeSiteUrl());
						columnBean.getNamesAndUrlValues().add(nameValueBean);
					}
				}
			}
			columnIndex++;
		}
		
		// delete idBean
		for (Iterator iterator2 = reportRows.iterator(); iterator2.hasNext();) {
			ReportRowBean reportRowBean = (ReportRowBean) iterator2.next();
			reportRowBean.getColumns().remove(0);
		}
		
		return reportRows;
	}
	
}
