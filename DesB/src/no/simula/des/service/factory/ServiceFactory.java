package no.simula.des.service.factory;

import no.simula.des.service.ReportService;
import no.simula.des.service.impl.DefaultReportService;

public abstract class ServiceFactory {
	public static ReportService getReportService() {
		return new DefaultReportService();
	}
}
