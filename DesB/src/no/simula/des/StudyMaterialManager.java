package no.simula.des;

import java.util.ArrayList;
import java.util.List;

/**
 * This class manages study material. Use this class to create study material
 * of a certain type, by using the <code>createStudyMaterial</code>-method.
 *
 * @author  Stian Eide
 */
public class StudyMaterialManager {
  
  /** Creates a new instance of StudyMaterialFactory */
  private StudyMaterialManager() {}

  /** A list of the names of all known study material types */  
  public final static List TYPES;
  
  static {
     TYPES = new ArrayList(2);
     TYPES.add(File.TYPE);
     TYPES.add(Link.TYPE);
  } 
  
  /** Returns an instance of <CODE>StudyMaterialManager</CODE>.
   * @return an instance of <CODE>StudyMaterialManager</CODE>
   */  
  public synchronized static StudyMaterialManager getInstance() {
    if(instance == null) {
      instance = new StudyMaterialManager();
    }
    return(instance);
  }
  
  private static StudyMaterialManager instance;

  /** Returns a list of the names of all known study material types.
   * @return a list of known study material types
   */  
  public List getTypes() {
    ArrayList types = new ArrayList(2);
    types.add(createStudyMaterial(File.TYPE));
    types.add(createStudyMaterial(Link.TYPE));
    return(types);
  }
  
  /** Creates an instance of <CODE>StudyMaterial</CODE> of the specified type.
   * @param type the name of the study material type
   * @return a <CODE>StudyMaterial</CODE> of the specified type
   */  
  public StudyMaterial createStudyMaterial(String type) {
    if(type.equals(File.TYPE)) {
      return(new File());
    }
    else {
      return(new Link());
    }
  }
}