package no.simula;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * A person is someone that has a relation to the DES-application. This includes
 * roles as a privileged user, a study responsible or as an author of
 * publications.
 */
public class Person extends AbstractXmlRpcPersistable implements Comparable, Serializable {

	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(Person.class);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7232788015722689766L;

	/** Creates an empty person. */
	public Person() {
	}

	/**
	 * Creates a person with the specified id.
	 * 
	 * @param id
	 *            the id
	 */
	public Person(String id) {
		LOG.debug("Person.Person()");
		LOG.debug(id + " <-- id");
		this.id = id;
	}
	
	/**
	 * @param id
	 * @deprecated
	 */
	public Person(Integer id) {
		LOG.debug("Person.Person()");
		LOG.debug(id + " <-- id");
		this.id = id.toString();
	}

	/**
	 * Creates a person with the specified id, first name, family name, email
	 * and list of privileges.
	 * 
	 * @param id
	 *            the id
	 * @param firstName
	 *            the first name
	 * @param familyName
	 *            the family name
	 * @param email
	 *            the e-mail
	 * @param privileges
	 *            a list of privileges
	 */
	public Person(String id, String firstName, String familyName,
			String email, List privileges) {
		this.id = id;
		this.firstName = firstName;
		this.familyName = familyName;
		this.email = email;
		this.privileges = privileges;
	}
	
	public Person(Integer id, String firstName, String familyName,
			String email, List privileges) {
		this.id = id.toString();
		this.firstName = firstName;
		this.familyName = familyName;
		this.email = email;
		this.privileges = privileges;
	}	

	private String id;
	private String firstName;
	private String familyName;
	private String email;
	private List privileges;
	private String homeSiteUrl;

	/**
	 * Returns the list of privileges that this person have.
	 * 
	 * @return the list of privileges for this person
	 */
	public List getPrivileges() {
		return (privileges);
	}

	/**
	 * Grants, i.e.&nbsp;adds, a privilege to this person
	 * 
	 * @param privilege
	 *            the privilege to add
	 */
	public void grantPrivilege(Privilege privilege) {
		if (!hasPrivilege(privilege)) {
			if (privileges == null) {
				privileges = new ArrayList();
			}
			privileges.add(privilege);
		}
	}

	/**
	 * Returns whether this person has a certain privilege.
	 * 
	 * @param privilege
	 *            the privilege to check
	 * @return <CODE>true</CODE> if this person has the given privilege
	 */
	public boolean hasPrivilege(Privilege privilege) {
		return privileges != null && privileges.contains(privilege);
	}

	/**
	 * @param privilegeId
	 * @return
	 */
	public boolean hasPrivilege(int privilegeId) {
		for (Iterator iterator = privileges.iterator(); iterator.hasNext();) {
			Privilege privilege = (Privilege) iterator.next();
			if (privilege.getIdAsInteger().intValue() == privilegeId) {
				return true;
			}
		}
		return false;
	}

	
	/**
	 * Revokes, i.e.&nbsp;removes, a privilege from this person
	 * 
	 * @param privilege
	 *            the privilege to remove
	 */
	public void revokePrivilege(Privilege privilege) {
		privileges.remove(privilege);
	}

	/**
	 * Getter for property familyName.
	 * 
	 * @return Value of property familyName.
	 * 
	 */
	public String getFamilyName() {
		return (familyName);
	}

	/**
	 * Setter for property familyName.
	 * 
	 * @param familyName
	 *            New value of property familyName.
	 * 
	 */
	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	/**
	 * Getter for property firstName.
	 * 
	 * @return Value of property firstName.
	 * 
	 */
	public String getFirstName() {
		return (firstName);
	}

	/**
	 * Setter for property firstName.
	 * 
	 * @param firstName
	 *            New value of property firstName.
	 * 
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Returns the persons name in the form "FirstName FamilyName"
	 * 
	 * @return The full name of this person
	 */
	public String getFirstAndFamilyName() {
		return (firstName + " " + familyName);
	}

	/**
	 * Returns the persons name in the form "FamilyName, FirstName"
	 * 
	 * @return The full name of this person
	 */
	public String getFamilyAndFirstName() {
		return (familyName + ", " + firstName);
	}

	/**
	 * Returns the persons name in the form "FamilyName, FirstName"
	 * 
	 * @return The full name of this person
	 */
	public String getFullName() {
		return (getFamilyAndFirstName());
	}

	/**
	 * Returns the full name of this person as specified by the
	 * <CODE>getFullName</CODE>-method.
	 * 
	 * @return the full name of this person
	 */
	public String toString() {
		return (getFullName());
	}

	public String getIdAsString() {
		return (id);
	}

	public void setId(String id) {
		LOG.debug("Person.setId()");
		LOG.debug(id + " <-- id");
		this.id = id;
	}

	/**
	 * Returns whether this person is equal to another person. Two persons are
	 * equal if their ids are equal.
	 * 
	 * @param o
	 *            another person
	 * @return <CODE>true</CODE> if <CODE>this.id.equals(o.id)</CODE>
	 */

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Person other = (Person) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	/**
	 * Getter for property email.
	 * 
	 * @return Value of property email.
	 * 
	 */
	public String getEmail() {
		return (email);
	}

	/**
	 * Setter for property email.
	 * 
	 * @param email
	 *            New value of property email.
	 * 
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Sets the privileges of this person
	 * 
	 * @param privileges
	 *            list of priviliges to be set for this person
	 */
	public void setPrivileges(List privileges) {
		this.privileges = privileges;
	}

	/**
	 * Compares two persons by their ids.
	 * 
	 * @param o
	 *            another person
	 * @return the value <code>0</code> if this id is equal to the other id; a
	 *         value less than <code>0</code> if this id is numerically less
	 *         than the other id; and a value greater than <code>0</code> if
	 *         this id is numerically greater than the other id (signed
	 *         comparison).
	 */
	public int compareTo(Object o) {
		return (id.compareTo(((Person) o).id));
	}

	public String getHomeSiteUrl() {
		return homeSiteUrl;
	}

	public void setHomeSiteUrl(String homeSiteUrl) {
		this.homeSiteUrl = homeSiteUrl;
	}
	
	/**
	 * @return
	 * @deprecated
	 * 
	 */
	public Integer getIdAsInteger() {
		return null;
	}

	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}



}