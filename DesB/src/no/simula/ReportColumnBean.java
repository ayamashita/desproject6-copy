package no.simula;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReportColumnBean {

	private List namesAndUrlValues = new ArrayList();

	public String toString() {
		return "ReportColumnBean value: " + getValue();
	}

	public String getValue() {
		return (String) (namesAndUrlValues.isEmpty() ? null : ((NameValueBean)namesAndUrlValues.get(0)).getName());
	}

	public void setValue(String value) {
		namesAndUrlValues = new ArrayList();
		NameValueBean nameValueBean = new NameValueBean(value, null);
		namesAndUrlValues.add(nameValueBean);
	}

	public String getUrl() {
		return (String) (namesAndUrlValues.isEmpty() ? null : ((NameValueBean)namesAndUrlValues.get(0)).getValue());
	}

	public void setUrl(String url) {
		NameValueBean nameValueBean = (NameValueBean) namesAndUrlValues.get(0);
		nameValueBean.setValue(url);
	}

	public List getNamesAndUrlValues() {
		return namesAndUrlValues;
	}

	public void setNamesAndUrlValues(List values) {
		this.namesAndUrlValues = values;
	}

	public String getNames() {
		StringBuffer out = new StringBuffer();
		for (Iterator iterator = namesAndUrlValues.iterator(); iterator.hasNext();) {
			NameValueBean nameValueBean = (NameValueBean) iterator.next();
			out.append(nameValueBean.getName() + ";\n");
		}
		return out.toString();
	}
}
