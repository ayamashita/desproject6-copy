package no.simula;

import no.halogen.persistence.XmlRpcPersistable;

public abstract class AbstractXmlRpcPersistable implements XmlRpcPersistable {

	public Object getId() {
		return getIdAsString();
	}

	public void setId(Object id) {
		setId(id.toString());
	}
	
}
