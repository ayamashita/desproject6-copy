package no.simula.wrapper;

import java.util.Map;

import no.simula.Publication;

public class XmlRpcPublicationWrapper extends AbstractXmlRpcWrapper {
	private no.simula.Publication publication;
	
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(XmlRpcPublicationWrapper.class);
	
	public XmlRpcPublicationWrapper(Map xmlRpcResults) {
		super(xmlRpcResults);
		publication = new Publication();
		/*
        bean.setId(rs.getInt(1));
        bean.setYear(rs.getInt(2));
        bean.setTitle(rs.getString(3));
        bean.setAbstact(rs.getString(4));
        boolean b = (rs.getString(5).equals("n")? true:false);
        bean.setExternal(b);
        String firstname = rs.getString(6);
        String lastname = rs.getString(7);
        if(b || (firstname == null && lastname == null ) ){
          firstname = (firstname == null? "": firstname );
          lastname = (lastname == null? "n/a": lastname);

          bean.setEditor(lastname);
        }
        else{
          bean.setEditor(lastname+","+firstname);
        }

		id=Simula.SE.159,
		abstract=It is well known that software development companies tend to produce over-optimistic cost estimates and that this over-optimism may lead to delivery problems for the clients as well as the providers. In this paper we summarize evidence suggesting that the clients can reduce the likelihood of selecting providers with bids based on over-optimistic cost estimates through their control of the bidding processes. Important means for this purpose include: avoid inviting many bidders when price is an important criterion for selection; avoid using price as an important criterion for selection when the ability to assess provider competence is low; apply bidding processes that ensure that the provider understands the complexity of the project; avoid budget or price expectation related information in the bidding material and avoid a negotiation process in which you ask for bid updates on reduced functionality. The evidence presented in this paper can also be used by software providers to identify bidding rounds where it is likely that they win only when strongly over-optimistic about the cost.
		authors=[Ljava.lang.Object;@1037c71,
		title=How%20to%20Avoid%20Selecting%20Providers%20with%20Bids%20Based%20on%20Over-Optimistic%20Cost%20Estimates,
		source=Accepted for publication in IEEE Software (May/June).
		year=2009
		hasPdf=true
		type=Article in International Journal
		url=http://simula.no:9080/simula/research/engineering/publications/Simula.SE.159
		creator=magnej
		*/
		publication.setId(getString("id"));
		publication.setTitle(decode(getString("title")));
		publication.setUrl(getString("url"));
		
		Object[] authors = (Object[]) getObject("authors");
		StringBuffer authorsString = new StringBuffer();
		for (int i = 0; i < authors.length; i++) {
			Map author = (Map) authors[i];
			//{lastname=Holmeide, firstname=?yvind, homepage=},{lastname=Skeie, firstname=Tor, uid=d720210b81f71f916127564d86cd3082, homepage=/people/tskeie, username=tskeie
			authorsString.append(author.get("firstname") + ", " + author.get("lastname"));
		}
		if (authorsString.length() > 0) {
			authorsString.deleteCharAt(authorsString.length()-1);
		}
		
		LOG.debug(authorsString + " <-- authorsString");
		publication.setAuthors(authorsString.toString());
		
	}
	
	public Publication getPublication() {
		return publication;
	}
}
