package no.simula.wrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class XmlRpcPeopleWrapper {

	
	public XmlRpcPeopleWrapper(Map[] out) {
		people = new ArrayList(out.length);
		for (int i = 0; i < out.length; i++) {
			Map map = out[i];
			people.add(new XmlRpcPersonWrapper(map).getPerson());
		}
	}
	
	private List people;

	public List getPeople() {
		return people;
	}
	
}
