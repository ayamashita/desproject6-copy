package no.simula.wrapper;

import java.util.Map;

import no.simula.Person;

public class XmlRpcPersonWrapper extends AbstractXmlRpcWrapper {
	private Person person;
	
	public XmlRpcPersonWrapper(Map xmlRpcResults) {
		super(xmlRpcResults);
		person = new Person();
		/*
		id=andremcc,
		jobtitle=Adjunct Research Scientist, 
		email=andremcc@simula.no, 
		lastname=McCulloch, 
		firstname=Andrew, 
		url=http://simula.no:9080/simula/people/andremcc, 
		Title=Andrew McCulloch
		*/
		person.setId(getString("id"));
		person.setFirstName(getString("firstname"));
		person.setFamilyName(getString("lastname"));
		person.setEmail(getString("email"));
		person.setHomeSiteUrl(getString("url"));
		//peopleBean.set
	}
	
	public Person getPerson() {
		return person;
	}
}
