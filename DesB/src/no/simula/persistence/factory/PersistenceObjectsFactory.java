package no.simula.persistence.factory;

import no.simula.persistence.XmlRpcPersistentPerson;
import no.simula.persistence.XmlRpcPersistentPublication;

public class PersistenceObjectsFactory {

	public static XmlRpcPersistentPerson getPersistancePerson() {
		return new XmlRpcPersistentPerson();
	}
	
	public static XmlRpcPersistentPublication getPersistentPublication() {
		return new XmlRpcPersistentPublication();
	}
	
	

}
