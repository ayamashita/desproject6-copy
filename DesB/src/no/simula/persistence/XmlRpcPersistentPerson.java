/**
 * @(#) PersistentPerson.java
 */

package no.simula.persistence;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import no.halogen.persistence.AbstractXmlRpcPersistentObject;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.persistence.XmlRpcPersistentObject;
import no.halogen.statements.StatementException;
import no.simula.Person;
import no.simula.Privilege;
import no.simula.des.persistence.PersistentPrivilege;
import no.simula.des.statements.PersonPrivilegesRelStatement;
import no.simula.des.statements.PersonStudyRelStatement;
import no.simula.des.statements.PrivilegePersonsRelStatement;
import no.simula.statements.PersonStatement;
import no.simula.wrapper.XmlRpcPeopleWrapper;

import org.apache.xmlrpc.XmlRpcException;

/**
 * @author Frode Langseth
 */
public class XmlRpcPersistentPerson extends AbstractXmlRpcPersistentObject
		implements XmlRpcPersistentObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7557775923539737005L;
	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(XmlRpcPersistentPerson.class);

	/**
	 * Constructor for PersistentPerson
	 */
	public XmlRpcPersistentPerson() {

	}

	/**
	 * The Person object itself is in the simula database, and will not be
	 * created. But the privileges related to a person might be updated, so this
	 * method will update that relation
	 * 
	 * @param instance
	 * @return null
	 * 
	 * @see no.halogen.persistence.PersistentObject#create(Object)
	 */
	public Integer create(Object instance) {
		throw new UnsupportedOperationException("create is not supported)");
	}

	/**
	 * As this entity is in the simulaweb database, DES will never delete it ...
	 * The method is not implemented
	 * 
	 * @param id
	 * @return boolean
	 * @see no.halogen.persistence.PersistentObject#delete(Integer)
	 */
	public boolean delete(Object instance) {
		throw new UnsupportedOperationException("delete is not supported)");
	}

	/**
	 * Finds the persons responsible for a study
	 * 
	 * @param id -
	 *            id of the study to find responsibles for
	 * @return A List that contatins all the responsible persons
	 * 
	 */
	public List findResponsibleByStudy(Integer id) {
		List personIds = new ArrayList();
		List persons = new ArrayList();
		List privileges = null;

		personIds = (ArrayList) getPersonsByStudy(id);
		persons = (ArrayList) findByKeys(personIds);
		Iterator i = persons.iterator();

		while (i.hasNext()) {
			Person entity = (Person) i.next();
			privileges = getPersonPrivileges(entity.getIdAsString());
			entity.setPrivileges(privileges);
		}

		return (persons);
	}

	/**
	 * The Person object itself is in the simula database, and will not be
	 * updated. But the privileges related to a person might be updated, so this
	 * method will update that relation
	 * 
	 * @param id
	 *            Integer
	 * @param instance
	 * 
	 * @return boolean
	 * @see no.halogen.persistence.PersistentObject#update(Integer, Object)
	 */
	public boolean update(Integer id, Object instance) {
		throw new UnsupportedOperationException("update is not supported)");
	}

	/**
	 * Method getPersonsByStudy
	 * 
	 * @param id
	 *            Integer
	 * @return List
	 */
	private List getPersonsByStudy(Integer id) {
		ArrayList persons = new ArrayList(); // List to receive result from
		// the query

		/*
		 * Integer personId is the "dataBean". Since the tables are in two
		 * different databases, two queries must be performed. First one to
		 * fetch ids for the Simulaweb.People database, then one to get the
		 * Person information from Simulaweb. Since the first query won't return
		 * any populated objects, String in an ArrayList will contain the result
		 * (person ids)
		 */
		String personId = "";

		PersonStudyRelStatement studyPersonRelStatement = new PersonStudyRelStatement();

		studyPersonRelStatement.setDataBean(personId);

		studyPersonRelStatement.setWhereClause(" WHERE stu_resp_id = ?");
		studyPersonRelStatement.getConditions().add(id);

		try {
			persons = (ArrayList) studyPersonRelStatement.executeSelect();
		} catch (StatementException e) {
			LOG.error("findByStudy() - Fetching of persons related to study "
					+ id + " failed!!", e);
			return (null);
		}

		return (persons);
	}

	/**
	 * Finds persons by a list of identities/key
	 * 
	 * @param entityIds -
	 *            A List of perons id's, each id as an Integer
	 * @return List with populated Person objects. If the List is empty, all
	 *         persons will be returned
	 * 
	 * @see no.halogen.persistence.PersistentObject#findByKeys(List)
	 */
	public List findByKeys(List entityIds) {
		List persons = new ArrayList();
		if (entityIds != null && entityIds.size() > 0) {
			for (Iterator iterator = entityIds.iterator(); iterator.hasNext();) {
				String id = (String) iterator.next();

				Person person = (Person) findByKey(id);

				List privileges = getPersonPrivileges(id);
				person.setPrivileges(privileges);

				persons.add(person);
			}
		}
		return (persons);
	}

	/**
	 * @param personId
	 *            the id of the person to fetch privileges for
	 * @return A list of privileges
	 */
	private List getPersonPrivileges(String personId) {
		List condition = new ArrayList();
		PrivilegePersonsRelStatement statement = new PrivilegePersonsRelStatement();
		List results = new ArrayList();
		PersistentPrivilege persistentPrivilege = null;
		List privileges = new ArrayList();

		statement.setWhereClause(" WHERE people_id = ?");

		condition.add(personId);
		statement.setConditions(condition);

		try {
			results = (List) statement.executeSelect();
		} catch (StatementException e) {
			LOG.error(
					"getPersonPrivileges() - Error when fetching privileges for person "
							+ personId, e);
			return (null);
		}

		persistentPrivilege = new PersistentPrivilege();
		privileges = persistentPrivilege.findByKeys(results);

		return (privileges);
	}

	/**
	 * As this entity is in the simulaweb database, DES will never update it ...
	 * The method is not implemented
	 * 
	 * @param instance
	 * 
	 * @return boolean
	 * @throws UpdatePersistentObjectException
	 * @see no.halogen.persistence.PersistentObject#update(Object)
	 */
	public boolean update(Object instance)
			throws UpdatePersistentObjectException {
		//update privs
		boolean result = false;
		Person person = (Person) instance;

		/*
		 * First, delete the existing privileges
		 */
		PersonPrivilegesRelStatement statement = new PersonPrivilegesRelStatement();

		try {
			result = statement.executeDelete(person);
			
			result =  setPrivilege(person);
			} catch (StatementException e) {
			LOG.error("update() - Could not delete privileges from person.");
		}

		return (result);
	}
	
	private boolean setPrivilege(Person person) {
		List privileges = person.getPrivileges();
		PersonPrivilegesRelStatement statement = new PersonPrivilegesRelStatement();
		/*
		 * There's no databean representing the privilege-person, a list is
		 * created, and populated with the privilege and person ids
		 */
		if (!privileges.isEmpty()) {
			Iterator i = privileges.iterator();
			while (i.hasNext()) {
				List entity = new ArrayList();

				Privilege privilege = (Privilege) i.next();

				entity.add(privilege.getIdAsInteger());
				entity.add(person.getId());

				statement.setDataBean(entity);

				try {
					statement.executeInsert();
				} catch (StatementException e) {
					LOG
							.error("update() - Could not delete privileges from person.");
				}
			}
		}

		return (true);
	}	

	/**
	 * Finding all Persons
	 * 
	 * @return - All persons in the database
	 * 
	 * @see no.halogen.persistence.PersistentObject#find()
	 */
	public List find() {
		HashMap params = new HashMap();
		try {
			Map[] out = executeMethod("getPeople", params);
			List people = new XmlRpcPeopleWrapper(out).getPeople();
			return people;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Finds the persons responsible for a study
	 * 
	 * @param email -
	 *            id of the study to find responsibles for
	 * @return A List that contatins all the responsible persons
	 * 
	 */
	public Object findPersonByEmail(String email) {
		List persons = new ArrayList();

		PersonStatement personStatement = new PersonStatement();

		Person person = null;

		personStatement.setDataBean(person);

		personStatement.setWhereClause(" WHERE people_email = ?");
		personStatement.getConditions().add(email);

		try {
			persons = (List) personStatement.executeSelect();
		} catch (StatementException e) {
			LOG.error(
					"findPersonByEmail() - Statement failed for person with email adress: "
							+ email, e);
			return (null);
		}

		return (persons.get(0));
	}

	/**
	 * Finds the persons responsible for a study
	 * 
	 * @param id -
	 *            id of the study to find responsibles for
	 * @return A List that contatins all the responsible persons
	 * 
	 */
	public List findPersonsByPrivilege(Integer id) {

		return null;
	}

	/**
	 * As this entity is in the simulaweb database, DES will never update it ...
	 * The method is not implemented
	 * 
	 * @param instances
	 * @return int
	 * @throws UpdatePersistentObjectException
	 */
	public int update(List instances) throws UpdatePersistentObjectException {
		int updated = 0;

		Iterator i = instances.iterator();

		while (i.hasNext()) {
			if (update(i.next())) {
				updated++;
			}
		}

		return (updated);
	}

	public Person findByKeyWithPriviliges(String id) {
		Person person = (Person) findByKey(id);
		if (person != null) {
			List privileges = getPersonPrivileges(id);
			person.setPrivileges(privileges);
		}
		return person;		
	}

	public Object findByKey(Object id) {
		HashMap params = new HashMap();
		params.put("id", id);
		LOG.debug(id + " <-- id");
		try {
			Map[] out = executeMethod("getPeople", params);
			if (out.length == 1) {
				Person person = new no.simula.wrapper.XmlRpcPersonWrapper(
						out[0]).getPerson();
				LOG.debug(person + " <-- person");
				return person;
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
