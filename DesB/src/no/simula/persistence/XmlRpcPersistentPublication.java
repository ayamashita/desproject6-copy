/*
 * Created on 29.okt.2003
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package no.simula.persistence;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import no.halogen.persistence.AbstractXmlRpcPersistentObject;
import no.halogen.persistence.CreatePersistentObjectException;
import no.halogen.persistence.DeletePersistentObjectException;
import no.halogen.persistence.UpdatePersistentObjectException;
import no.halogen.persistence.XmlRpcPersistentObject;
import no.halogen.statements.StatementException;
import no.simula.des.statements.StudyPublicationRelStatement;
import no.simula.wrapper.XmlRpcPublicationWrapper;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.xmlrpc.XmlRpcException;

/**
 * @author Frode Langseth
 * 
 * To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Generation - Code and Comments
 */
public class XmlRpcPersistentPublication extends AbstractXmlRpcPersistentObject
		implements XmlRpcPersistentObject {
	/**
	 * Field log
	 */
	private static Log LOG = LogFactory
			.getLog(XmlRpcPersistentPublication.class);

	/**
	 * As this entity is in the simulaweb database, DES will never create it ...
	 * The method is not implemented
	 * 
	 * @param instance
	 * @return null
	 * 
	 * @throws CreatePersistentObjectException
	 * @see no.halogen.persistence.PersistentObject#create(Object)
	 */
	public Integer create(Object instance)
			throws CreatePersistentObjectException {
		throw new UnsupportedOperationException("update");
	}

	/**
	 * As this entity is in the simulaweb database, DES will never delete it ...
	 * The method is not implemented
	 * 
	 * @param id
	 * @return boolean
	 * @see no.halogen.persistence.PersistentObject#delete(Integer)
	 */
	public boolean delete(Integer id) {
		throw new UnsupportedOperationException("update");
	}

	/**
	 * Finds a publication by it's id/key
	 * 
	 * @param id -
	 *            The publication's id/key
	 * @return Populated Publication object
	 * 
	 * @see no.halogen.persistence.PersistentObject#findByKey(Integer)
	 */
	public Object findByKey(Object id) {
		HashMap params = new HashMap();
		params.put("id", id);
		try {
			Map[] out = executeMethod("getPublications", params);
			if (out.length == 1) {
				return new XmlRpcPublicationWrapper(out[0]).getPublication();
			}
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * As this entity is in the simulaweb database, DES will never update it ...
	 * The method is not implemented
	 * 
	 * @param id
	 *            Integer
	 * @param instance
	 * 
	 * @return boolean
	 * @see no.halogen.persistence.PersistentObject#update(Integer, Object)
	 */
	public boolean update(Integer id, Object instance) {
		throw new UnsupportedOperationException("update");
	}

	/**
	 * Finds publications by a list of identities/key
	 * 
	 * @param entityIds -
	 *            A List of publication ids, each id as an Integer
	 * @return List with populated Publication objects. If the List is empty,
	 *         all publications will be returned
	 * 
	 * @see no.halogen.persistence.PersistentObject#findByKeys(List)
	 */
	public List findByKeys(List entityIds) {
		List publications = new ArrayList();
		if (entityIds != null && !entityIds.isEmpty()) {
			for (Iterator iterator = entityIds.iterator(); iterator.hasNext();) {
				String id = (String) iterator.next();
				publications.add(findByKey(id));
			}
		}
		return (publications);
	}

	/**
	 * Finds publications by a study id, that is, finds the publications related
	 * to a study
	 * 
	 * @param id -
	 *            a study id
	 * @return List with populated Publication objects. If the List is empty,
	 *         all publications will be returned
	 * 
	 */

	public List findByStudy(Integer id) {
		List publicationIds = (ArrayList) getPublicationsByStudy(id);
		List publications = (ArrayList) findByKeys(publicationIds);
		return (publications);
	}

	/**
	 * @param id
	 * @return ArrayList
	 */
	private ArrayList getPublicationsByStudy(Integer id) {
		ArrayList publications = new ArrayList(); // List to receive result
		// from the query

		/*
		 * String publicationId is the "dataBean". Since the tables are in two
		 * different databases, two queries must be performed. First, one to
		 * fetch ids for the Simulaweb.Publication database. Then, another one
		 * to get the Publication information from Simulaweb. Since the first
		 * query won't return any populated objects, String in an ArrayList will
		 * contain the result (publication ids)
		 */
		String publicationId = "";

		StudyPublicationRelStatement studyPublicationRelStatement = new StudyPublicationRelStatement();

		studyPublicationRelStatement.setDataBean(publicationId);

		studyPublicationRelStatement.setWhereClause(" WHERE stu_pub_id = ?");
		studyPublicationRelStatement.getConditions().add(id);

		try {
			publications = (ArrayList) studyPublicationRelStatement
					.executeSelect();
		} catch (StatementException e) {
			LOG.error(
					"getPublicationsByStudy() - Fetching of publications related to study "
							+ id + " failed!!", e);
			return (null);
		}

		return (publications);
	}

	/**
	 * As this entity is in the simulaweb database, DES will never update it ...
	 * The method is not implemented
	 * 
	 * @param instance
	 * 
	 * @return boolean
	 * @throws UpdatePersistentObjectException
	 * @see no.halogen.persistence.PersistentObject#update(Object)
	 */
	public boolean update(Object instance)
			throws UpdatePersistentObjectException {
		throw new UnsupportedOperationException("update");
	}

	/**
	 * Finding all Publications
	 * 
	 * @return - All publicatoins in the database
	 * 
	 * @see no.halogen.persistence.PersistentObject#find()
	 */
	public List find() {
		HashMap params = new HashMap();
		try {
			Map[] out = executeMethod("getPublications", params);
			List collection = new ArrayList(out.length);
			for (int i = 0; i < out.length; i++) {
				Map map = out[i];
				collection.add(new XmlRpcPublicationWrapper(map).getPublication());
			}
			return collection;
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XmlRpcException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public boolean delete(Object instance)
			throws DeletePersistentObjectException {
		throw new UnsupportedOperationException("update");
	}

}
