package no.simula;

import no.halogen.persistence.JdbcPersistable;

public abstract class AbstractJdbcPersistable implements JdbcPersistable {

	public Object getId() {
		return getIdAsInteger();
	}

	public void setId(Object id) {
		setId((Integer)id);
	}
	
}
