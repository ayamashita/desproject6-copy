package no.simula.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import no.simula.ReportBean;

import org.springframework.jdbc.core.RowMapper;

/**
 * @author mateusz
 *
 */
public class ReportBeanRowMapper implements RowMapper {

	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		ReportBean reportBean = new ReportBean();
		reportBean.setId(rs.getInt("id"));
		reportBean.setName(rs.getString("name"));
		reportBean.setOwnerId(rs.getString("owner_id"));
		return reportBean;
	}

}
