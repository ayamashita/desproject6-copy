package no.simula.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import no.simula.ReportColumnBean;
import no.simula.ReportRowBean;

import org.springframework.jdbc.core.RowMapper;

public class ReportRowRowMapper implements RowMapper {

	private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger
			.getLogger(ReportRowRowMapper.class);
	
	public Object mapRow(ResultSet rs, int rowNum) throws SQLException {
		int count = rs.getMetaData().getColumnCount();
		LOG.debug(count + " <-- count");
		List columns = new ArrayList(count);
		
		for (int i = 1; i <= count; i++) {
			String s = rs.getString(i);
			ReportColumnBean bean = new ReportColumnBean();
			bean.setValue(s);
			columns.add(bean);
		}
		
		ReportRowBean row = new ReportRowBean();
		row.setColumns(columns);
		return row;
	}

}
