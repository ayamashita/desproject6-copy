package no.simula;

import java.io.IOException;
import java.util.Properties;

/**
 * Use this factory to retrieve an instance of <code>Simula</code>.
 *
 * @author  Stian Eide
 */
public class SimulaFactory {

  private static final String CONFIG = "no/simula/simula.properties";
  private static final String IMPLEMENTATION = "simula.impl";
  private static Simula instance;    
  
  /** Returns an implementation of <CODE>Simula</CODE> as specified by the key
   * <CODE>simula.impl</CODE> in <CODE>simula.properties</CODE>.
   * @throws IOException if the properties file cannot be read
   * @throws ClassNotFoundException if the implementation as specified by <CODE>simula.impl</CODE> cannot be found
   * @throws InstantiationException if the implementation cannot be instantiated
   * @throws IllegalAccessException if the implementation cannot be accessed
   * @return an implementation of <CODE>Simula</CODE>
   */  
  public synchronized static Simula getSimula() throws
    IOException, 
    ClassNotFoundException, 
    InstantiationException, 
    IllegalAccessException
  {
    if(instance == null) {
      ClassLoader cl = SimulaFactory.class.getClassLoader();
      Properties pr = new Properties();
      pr.load(cl.getResourceAsStream(CONFIG));
      String clazz = pr.getProperty(IMPLEMENTATION);
      instance = (Simula)cl.loadClass(clazz).newInstance();
    }
    return(instance);
  } 
}