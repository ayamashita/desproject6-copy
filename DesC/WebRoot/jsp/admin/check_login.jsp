<%@page import="org.apache.commons.httpclient.*"%>
<%@page import="org.apache.commons.httpclient.methods.*"%>
<%@page import="org.apache.commons.httpclient.protocol.*"%>
<%@page import="org.apache.commons.httpclient.cookie.*"%>
<%@page import="org.apache.commons.httpclient.contrib.ssl.*"%>
<%@page import="org.apache.commons.httpclient.contrib.utils.*"%>
<%@page import="java.util.*"%>
<%
    // Get status from the session object to check if user has already logged in
    if ( true ) {   // Create a local scope for this file
    String status = (String)session.getAttribute("status");
    String cookieName = getServletContext().getInitParameter("php.cookie.name");
    String loginPage = getServletContext().getInitParameter("php.login.page");
    String statusPage = getServletContext().getInitParameter("php.status.page");
    if ( statusPage == null || statusPage.length() == 0 ) {
        statusPage = "/get_sessionstatus.php";
    }
    String loginHost = getServletContext().getInitParameter("php.login.host");
    if ( loginHost == null || loginHost.length() == 0 ) {
        loginHost = "localhost";
    }
    String loginProtocol = getServletContext().getInitParameter("php.login.protocol");
    if ( loginProtocol == null || loginProtocol.length() == 0 ) {
        loginProtocol = "https";
    }
    // Get any cookies sent and find the php session id
    javax.servlet.http.Cookie[] cookies = request.getCookies();
    String domain = null;
    String value = null;
    String path = null;
    int maxAge = -1;
    StringBuffer sb = new StringBuffer();
    int size = cookies != null ? cookies.length : 0;
    for ( int i = 0;i < size;i++) {
        String name = cookies[i].getName();
        sb.append(name);
        sb.append(",");
        if ( name != null && name.equals(cookieName)) {
            domain = cookies[i].getDomain();
            value = cookies[i].getValue();
            path = cookies[i].getPath();
            maxAge = cookies[i].getMaxAge();
        }
    }
    // Send a request to the status page to discover the clients status
    GetMethod httpget = null;
    String res = null;
    String error = null;
    String stack = null;
    // Variables holding the result of the check
    String userName = null;
    String userId = null;
    String userRight = null;
    try {
        //if ( status == null || status.equalsIgnoreCase("NOTOK")) {
            HttpClient httpclient = new HttpClient();
            if ( loginProtocol.equals("https")) {
                Protocol myhttps = new Protocol(loginProtocol, new EasySSLProtocolSocketFactory(), 443);
                httpclient.getHostConfiguration().setHost(loginHost, 443, myhttps);
                httpclient.setConnectionTimeout(30000);
                httpget = new GetMethod(statusPage);
            } else {
                httpget = new GetMethod(loginProtocol + "://" + loginHost  + statusPage);
            }
            httpclient.getState().setCookiePolicy(CookiePolicy.COMPATIBILITY);
            org.apache.commons.httpclient.Cookie phpCookie = new org.apache.commons.httpclient.Cookie(loginHost, cookieName, value);
            phpCookie.setPath("/");
            httpclient.getState().addCookie(phpCookie);
            
            int result = httpclient.executeMethod(httpget);
            res = httpget.getResponseBodyAsString();
            // Intepret the result, which should be NOTOK or OK;simula_username;simula_id;simula_right
            //System.err.println("result is " + res);
            int semiIndex = res.indexOf(";");
            if ( semiIndex > 0 ) {
                status = res.substring(0, semiIndex);
                int oldSemiIndex = semiIndex+1;
                semiIndex = res.indexOf(";", semiIndex+1);
                userName = res.substring(oldSemiIndex, semiIndex);
                oldSemiIndex = semiIndex+1;
                semiIndex = res.indexOf(";", semiIndex+1);
                userId = res.substring(oldSemiIndex, semiIndex);
                oldSemiIndex = semiIndex+1;
                userRight = res.substring(oldSemiIndex);
                //System.err.println("Parsed result is stat='" + status + "', username='" + userName + "', userid='" + userId + "', userright='" + userRight + "'");
            } else {
                status = res;
            }
        //}
    } catch(Exception ex) {
        error = ex.getClass().getName() + ": " + ex.getMessage();
    } finally {
        if ( httpget != null ) {
            httpget.releaseConnection();
        }
    }
    if ( status == null || status.length() == 0 || status.equalsIgnoreCase("NOTOK")) {
        session.setAttribute("status", "NOTOK");
        String redirectUrl = loginProtocol + "://" + loginHost + loginPage;
        //System.err.println("Redirecting to " + redirectUrl);
        //response.sendRedirect(redirectUrl);
        //response.flushBuffer();
    } else {
        session.setAttribute("status", "OK");
        session.setAttribute("simula_username", userName);
        //System.err.println("Userid is " + userId);
        session.setAttribute("simula_userid", userId);
        session.setAttribute("simula_userright", userRight);
    }
    }

%>
<%-- if ( false ) { %>
<html>
<head>
</head>
<body>
    date = <%=new Date().toString() %><br>
    #cookies = <%=size%><br>
    found cookies = <%=sb.toString()%>
    <hr>
    domain = <%=domain%><br>
    name = <%=cookieName%><br>
    value = <%=value%><br>
    path = <%=path%><br>
    <hr>
    status= <%=status%><br>
    username= <%=userName%><br>
    userid= <%=userId%><br>
    userright= <%=userRight%><br>
    <hr>
    error = <%=error%><br>
    result = <%=res%><br>

</body>
</HTML>
<% } --%>