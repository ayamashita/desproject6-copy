package no.machina.simula;

import java.util.*;
import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.math.NumberUtils;

/**
 * This class works as a gateway to the database.
 * The class is a singleton.
 */
public class DB{
    
    // Singleton
    private static DB instance = new DB();
    
    // just for debugging. contains the last stmt executed.
    public static String lastStmt = "";
    
    /** Ascending sort order */
    public static final int SORT_ORDER_ASCENDING = 0;
    /** Descending sort order */
    public static final int SORT_ORDER_DESCENDING = 1;
    /** Sort studies by study name. */
    public static final int SORT_STUDIES_STUDY_NAME = 0;
    /** Sort studies by study type id. */
    public static final int SORT_STUDIES_STUDY_TYPE_ID = 1;
    /** Sort studies by study end date. */
    public static final int SORT_STUDIES_STUDY_END_DATE = 2;
    
    //------- SQL-statements -------
    private static final String LAST_INSERT_ID_STMT = "SELECT LAST_INSERT_ID()";
    
    private static final String GET_ADMIN_MESSAGE_STMT =
    "SELECT message FROM tbl_admin_page";
    
    private static final String UPDATE_ADMIN_MESSAGE_STMT =
    "UPDATE tbl_admin_page SET message = ?";
    
    private static final String GET_STUDIES_STMT =
    "SELECT tbl_study.* "
    + "FROM tbl_study ";
    
    private static final String GET_STUDIES_STMT_STUDY_TYPE =
    " tbl_study.study_type_id = ? ";
    private static final String GET_STUDIES_STMT_EARLY_END_DATE =
    " study_end_date >= ? ";
    private static final String GET_STUDIES_STMT_LATE_END_DATE =
    " study_end_date <= ? ";
    private static final String GET_STUDIES_STMT_RESPONSIBLES =
    " responsible_id IN (";
    
    private static final String GET_STUDIES_FREE_TEXT_STMT =
    "SELECT tbl_study.* "
    + "FROM tbl_study, tbl_study_type "
    + "WHERE tbl_study.study_type_id = tbl_study_type.study_type_id "
    + "AND (study_name LIKE ? OR study_desc LIKE ? OR study_start_date LIKE ? OR study_end_date LIKE ? "
    + "OR keywords LIKE ? OR study_notes LIKE ? OR study_type_name LIKE ?)";
    
    private static final String GET_STUDIES_RESPONSIBLES_FREE_TEXT_STMT =
    "SELECT tbl_study.study_id, study_name, tbl_study.study_type_id, study_end_date, study_desc "
    + "FROM tbl_study, tbl_study_has_responsible "
    + "WHERE tbl_study.study_id = tbl_study_has_responsible.study_id "
    + "AND responsible_id IN (";
    
    private static final String GET_STUDIES_PUBLICATION_FREE_TEXT_STMT =
    "SELECT tbl_study.study_id, study_name, tbl_study.study_type_id, study_end_date, study_desc "
    + "FROM tbl_study, tbl_study_has_publication "
    + "WHERE tbl_study.study_id = tbl_study_has_publication.study_id "
    + "AND publication_id IN (";
    
    private static final String GET_RESPONSIBLE_IDS_STMT =
    "SELECT people_id FROM people WHERE people_first_name LIKE ? OR people_family_name LIKE ?";
    
    private static final String GET_PUBLICATION_IDS_STMT =
    "SELECT publication_id FROM publication WHERE publication_title LIKE ?";
    
    private static final String GET_RESPONSIBLES_FOR_STUDY_STMT =
    "SELECT responsible_id FROM tbl_study_has_responsible "
    + "WHERE study_id = ?";
    
    private static final String GET_PUBLICATIONS_FOR_STUDY_STMT =
    "SELECT publication_id FROM tbl_study_has_publication WHERE study_id = ?";
    
    private static final String GET_SINGLE_STUDY_STMT =
    "SELECT study_name, study_type_id, study_desc, study_duration, "
    + "study_duration_unit_id, study_start_date, study_end_date, keywords, no_of_students, "
    + "no_of_professionals, study_notes "
    + "FROM tbl_study "
    + "WHERE study_id = ?";
    
    private static final String DELETE_STUDY_STMT = "DELETE FROM tbl_study WHERE study_id IN(";
    
    private static final String DELETE_RESPONSIBLES_FOR_STUDY_STMT =
    "DELETE FROM tbl_study_has_responsible WHERE study_id = ?";
    
    private static final String DELETE_PUBLICATIONS_FOR_STUDY_STMT =
    "DELETE FROM tbl_study_has_publication WHERE study_id = ?";
    
    private static final String GET_ORPHAN_FILES_STMT =
    "SELECT tbl_file.file_id, file_name "
    + "FROM tbl_file LEFT JOIN tbl_study_has_file ON tbl_file.file_id = tbl_study_has_file.file_id "
    + "WHERE tbl_study_has_file.file_id IS NULL";
    
    private static final String INSERT_STUDY_STMT =
    "INSERT INTO tbl_study (study_id, study_name, study_type_id, study_duration, "
    + "study_duration_unit_id, study_start_date, study_end_date, study_desc, keywords, "
    + "no_of_students, no_of_professionals, study_notes) "
    + "VALUES (NULL, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    
    private static final String INSERT_STUDY_RESPONSIBLE_STMT =
    "INSERT INTO tbl_study_has_responsible (study_has_responsible_id, study_id, responsible_id) "
    + "VAlUES (NULL, ?, ?)";
    
    private static final String INSERT_STUDY_PUBLICATION_STMT =
    "INSERT INTO tbl_study_has_publication (study_has_publication_id, study_id, publication_id) "
    + "VAlUES (NULL, ?, ?)";
    
    private static final String UPDATE_STUDY_STMT =
    "UPDATE tbl_study SET study_name = ?, study_type_id = ?, study_duration = ?, "
    + "study_duration_unit_id = ?, study_start_date = ?, study_end_date = ?, "
    + "study_desc = ?, keywords = ?, no_of_students = ?, no_of_professionals = ?, "
    + "study_notes = ? "
    + "WHERE study_id = ?";
    
    private final String STUDY_EXISTS_STMT =
    "SELECT study_name FROM tbl_study WHERE study_name = ?";
    
    private static final String GET_STUDY_TYPE_NAME_STMT =
    "SELECT study_type_name FROM tbl_study_type WHERE study_type_id = ?";
    
    private static final String GET_UNIT_NAME_STMT =
    "SELECT unit_name FROM tbl_unit WHERE unit_id = ?";
    
    private static final String INSERT_FILE_STMT =
    "INSERT INTO tbl_file (file_id, file_name, content_type, file_desc, file, file_size) VALUES (NULL, ?, ?, ?, ?, ?)";
    
    private static final String INSERT_FILE_FOR_STUDY_STMT =
    "INSERT INTO tbl_study_has_file (study_has_file_id, study_id, file_id) VALUES (NULL, ?, ?)";
    
    private static final String DELETE_FILE_STMT =
    "DELETE FROM tbl_file WHERE file_id = ?";
    
    private static final String DELETE_FILE_FOR_STUDY_STMT =
    "DELETE FROM tbl_study_has_file WHERE study_id = ? AND file_id =  ?";
    
    private static final String GET_FILE_STMT =
    "SELECT file FROM tbl_file WHERE file_id = ?";
    
    private static final String GET_FILE_INFO_STMT =
    "SELECT file_name, content_type, file_desc, file_size FROM tbl_file WHERE file_id = ?";
    
    private static final String GET_FILES_FOR_STUDY_STMT =
    "SELECT tbl_file.file_id, file_name, content_type, file_desc, file_size "
    + "FROM tbl_file, tbl_study_has_file "
    + "WHERE tbl_file.file_id = tbl_study_has_file.file_id AND study_id = ?";
    
    private static final String INSERT_URL_STMT =
    "INSERT INTO tbl_url (url_id, study_id, url, url_desc) VALUES (NULL, ?, ?, ?)";
    
    private static final String GET_URL_STMT =
    "SELECT url, url_desc FROM tbl_url WHERE study_id = ?";
    
    private static final String DELETE_URL_STMT =
    "DELETE FROM tbl_url WHERE study_id = ?";
    
    private static final String GET_ALL_RESPONSIBLES_STMT =
    "SELECT people_id, people_first_name, people_family_name FROM people ORDER BY people_family_name, people_first_name";
    
    private static final String GET_ALL_PUBLICATIONS_STMT =
    "SELECT publication_id, publication_title FROM publication ORDER BY publication_title";
    
    private static final String GET_USERACTION_STMT =
    "select tbl_action.* " +
    "from tbl_user, tbl_user_type, tbl_user_type_has_action, tbl_action " +
    "where " +
    "tbl_user_type_has_action.action_id=tbl_action.action_id " +
    "and tbl_user_type_has_action.user_type_id=tbl_user_type.user_type_id " +
    "and tbl_user.user_type_id=tbl_user_type.user_type_id " +
    "and tbl_user.people_id=?";
    
    private static final String GET_USERACTIONID_STMT =
    "select * " +
    "from tbl_user_type, tbl_user_type_has_action, tbl_action " +
    "where " +
    "tbl_user_type_has_action.action_id=tbl_action.action_id " +
    "and tbl_user_type_has_action.user_type_id=tbl_user_type.user_type_id " +
    "and tbl_user_type.user_type_id=?";
    
    private static final String GET_USER_STMT =
    "select * " +
    "from tbl_user, tbl_user_type " +
    "where " +
    "tbl_user.user_type_id=tbl_user_type.user_type_id " +
    "and tbl_user.user_id = ? ";
    
    
    
    // DES Database connection parameters
    private String DRIVER;
    private String URL;
    private String USER;
    private String PASSWORD;
    
    // Simulaweb Database connection parameters
    private String SIMULA_DRIVER;
    private String SIMULA_URL;
    private String SIMULA_USER;
    private String SIMULA_PASSWORD;
    
    
    private DB() {
        // Get DES connection parameters
        DRIVER = System.getProperty("no.machina.simula.dbdriver");
        URL = System.getProperty("no.machina.simula.dburl");
        USER = System.getProperty("no.machina.simula.dbuser");
        PASSWORD = System.getProperty("no.machina.simula.dbpwd");
        
        // Get Simula connection parameters
        SIMULA_DRIVER = System.getProperty("no.machina.simula.simula_dbdriver");
        SIMULA_URL = System.getProperty("no.machina.simula.simula_dburl");
        SIMULA_USER = System.getProperty("no.machina.simula.simula_dbuser");
        SIMULA_PASSWORD = System.getProperty("no.machina.simula.simula_dbpwd");
    };
    
    /**
     * Returns an instance of the class.
     *
     * @return A DB object.
     */
    public static DB getInstance(){
        return instance;
    }
    
    /**
     * Gets a connection to the DES database. Abstracts the way in which this is
     * done, so that this can be changed later.
     *
     * @return A connection to the DES database.
     */
    public Connection getConnection() throws Exception, SQLException{
        Connection con = null;
        
        // Load database driver
        Class.forName(DRIVER).newInstance();
        
        // Connect to database
        con = DriverManager.getConnection(URL, USER, PASSWORD);
        
        return con;
    }
    
    /**
     * Gets a connection to the Simulaweb database. Abstracts the way in which this is
     * done, so that this can be changed later.
     *
     * @return A connection to the Simulaweb database.
     */
    public Connection getSimulaConnection() throws Exception, SQLException{
        Connection con = null;
        
        // Load database driver
        Class.forName(SIMULA_DRIVER).newInstance();
        
        // Connect to database
        con = DriverManager.getConnection(SIMULA_URL, SIMULA_USER, SIMULA_PASSWORD);
        //System.err.println(SIMULA_URL + " " + SIMULA_USER + " " + SIMULA_PASSWORD);
        return con;
    }
    
    
    /**
     * Closes a database connection.
     *
     * @param con The database connection to be closed.
     */
    public void closeConnection(Connection con){
        try{
            con.close();
        } catch(Exception e){
        }
    }
    
    /** Gets the last auto incremented id. This is done on a per-connection
     * basis by the database and hence is safe (other clients' inserts
     * will not interfere).
     * @ con The database connection.
     * @return The value of the last auto incremented id, 0 if there isn't one.
     */
    private int getLastInsertId(Connection con) throws Exception, SQLException{
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(LAST_INSERT_ID_STMT);
        rs.next();
        return rs.getInt(1);
    }
    
    /**
     * Gets the admin message.
     *@return The message.
     */
    public String getAdminMessage() throws Exception, SQLException{
        Connection con = getConnection();
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery(GET_ADMIN_MESSAGE_STMT);
        String msg = "";
        if (rs.next()){
            msg = rs.getString(1);
        }
        closeConnection(con);
        return msg;
    }
    
    /**
     * Updates the admin message.
     * @param The message.
     */
    public void updateAdminMessage(String msg) throws Exception, SQLException{
        if (msg == null){
            msg = "";
        }
        Connection con = getConnection();
        PreparedStatement stmt = con.prepareStatement(UPDATE_ADMIN_MESSAGE_STMT);
        stmt.setString(1, msg);
        stmt.executeUpdate();
        closeConnection(con);
    }
    
    
    /**
     * Checks wether a study name already exists.
     * @param studyName The name of the study
     * @return true if the study name already exists, false otherwise
     */
    public boolean doesStudyNameExist(String studyName) throws Exception, SQLException{
        Connection con = getConnection();
        
        PreparedStatement stmt = con.prepareStatement(STUDY_EXISTS_STMT);
        stmt.setString(1, studyName);
        ResultSet rs = stmt.executeQuery();
        
        if (rs.next()){
            closeConnection(con);
            return true;
        }
        closeConnection(con);
        return false;
    }
    
    /**
     * Gets responsibles whose name match the search string.
     * @param searchString The string to match.
     * @return An array of the ids of the responsibles matching the search string.
     */
    public List getResponsibleIds(String searchString) throws Exception, SQLException{
        List ids = new ArrayList();
        if (searchString == null){return ids;}
        
        Connection con = getSimulaConnection();
        PreparedStatement stmt = con.prepareStatement(GET_RESPONSIBLE_IDS_STMT);
        stmt.setString(1, searchString);
        stmt.setString(2, searchString);
        ResultSet rs = stmt.executeQuery();
        
        while (rs.next()){
            ids.add(new Integer(rs.getInt(1)));
        }
        return ids;
    }
    
    /**
     * Gets publications whose name match the search string.
     * @param searchString The string to match.
     * @return An array of the ids of the publications matching the search string.
     */
    public List getPublicationIds(String searchString) throws Exception, SQLException{
        List ids = new ArrayList();
        if (searchString == null){return ids;}
        
        Connection con = getSimulaConnection();
        PreparedStatement stmt = con.prepareStatement(GET_PUBLICATION_IDS_STMT);
        stmt.setString(1, searchString);
        ResultSet rs = stmt.executeQuery();
        
        while (rs.next()){
            ids.add(new Integer(rs.getInt(1)));
        }
        return ids;
    }
    
    
    /**
     * Gets all responsibles for a study.
     * @param studyId The id of the study.
     * @return The responsibles for the study. If there are no responsibles the list
     * will be empty.
     */
    public List getResponsiblesForStudy(int studyId) throws Exception, SQLException{
        // Get ids of responsibles for study
        Connection DESCon = getConnection();
        PreparedStatement respStmt = DESCon.prepareStatement(GET_RESPONSIBLES_FOR_STUDY_STMT);
        respStmt.setInt(1, studyId);
        ResultSet respRs = respStmt.executeQuery();
        
        boolean noResponsibles = true; // initially assume study has no responsibles
        String simulaSql = "SELECT people_id, people_first_name, people_family_name FROM people WHERE people_id IN (";
        
        if (respRs.next()){
            noResponsibles = false;
            simulaSql += respRs.getInt(1);
        }
        while (respRs.next()){
            // Build SQL for people
            simulaSql += ", " + respRs.getInt(1);
        }
        simulaSql += ")";
        closeConnection(DESCon);
        
        List responsibles = new ArrayList();
        
        // If there are responsibles, get them and add to list.
        if (noResponsibles == false){
            Connection simulaCon = getSimulaConnection();
            Statement stmt = simulaCon.createStatement();
            ResultSet rs = stmt.executeQuery(simulaSql);
            
            while (rs.next()){
                Responsible resp = new Responsible();
                resp.setId(rs.getInt(1));
                resp.setFirstName(rs.getString(2));
                resp.setFamilyName(rs.getString(3));
                responsibles.add(resp);
            }
            closeConnection(simulaCon);
        }
        return responsibles;
    }
    
    
    /**
     * Gets all publications for a study.
     * @param studyId The id of the study.
     * @return The publications for the study. If there are no publications the list
     * will be empty.
     */
    public List getPublicationsForStudy(int studyId) throws Exception, SQLException{
        // Get ids of publications for study
        Connection DESCon = getConnection();
        PreparedStatement pubStmt = DESCon.prepareStatement(GET_PUBLICATIONS_FOR_STUDY_STMT);
        pubStmt.setInt(1, studyId);
        ResultSet pubRs = pubStmt.executeQuery();
        
        boolean noPublications = true; // initially assume study has no publications
        String simulaSql = "SELECT publication_id, publication_title FROM publication WHERE publication_id IN (";
        
        if (pubRs.next()){
            noPublications = false;
            simulaSql += pubRs.getInt(1);
        }
        while (pubRs.next()){
            // Build SQL for people
            simulaSql += ", " + pubRs.getInt(1);
        }
        simulaSql += ")";
        closeConnection(DESCon);
        
        List publications = new ArrayList();
        
        // If there are publications, get them and add to list.
        if (noPublications == false){
            Connection simulaCon = getSimulaConnection();
            Statement stmt = simulaCon.createStatement();
            ResultSet rs = stmt.executeQuery(simulaSql);
            
            while (rs.next()){
                Publication pub = new Publication();
                pub.setId(rs.getInt(1));
                pub.setTitle(rs.getString(2));
                publications.add(pub);
            }
            closeConnection(simulaCon);
        }
        return publications;
    }
    
    
    /** Retrieves all studies satisfying the given parameters.
     * A value of NULL for any of the parameters will return all studies
     * for this parameter.
     * @param studyType The study type of the returned studies.
     * @param earlyEndDate The minimum end date of the returned studies.
     * @param lateEndDate The maximum end date of the returned studies.
     * @param responsibleIds A list of the responsibles of the returned studies.
     * @param sortBy Which column to sort by.
     * @param sortOrder The sort order (ascending or descending).
     * @return A list of the studies satisfying the given parameters.
     */
    public List getStudies(int studyType, java.util.Date earlyEndDate,
    java.util.Date lateEndDate, int[] responsibleIds, int sortBy, int sortOrder)
    throws Exception, SQLException{
        List studies = new ArrayList();
        boolean where = false; // wether the WHERE-clase has been entered
        Connection con = getConnection();
        
        String query = GET_STUDIES_STMT;
        
        // Set up query according to parameters
        if (responsibleIds != null && responsibleIds.length > 0){
            query += ", tbl_study_has_responsible "
            + "WHERE tbl_study.study_id = tbl_study_has_responsible.study_id ";
            where = true;
        }
        
        if (studyType > 0){
            if (where){
                query += "AND ";
            }
            else{
                query += "WHERE ";
                where = true;
            }
            query += GET_STUDIES_STMT_STUDY_TYPE;
        }
        if (earlyEndDate != null){
            if (where){
                query += "AND ";
            }
            else{
                query += "WHERE ";
                where = true;
            }
            query += GET_STUDIES_STMT_EARLY_END_DATE;
        }
        if (lateEndDate != null){
            if (where){
                query += "AND ";
            }
            else{
                query += "WHERE ";
                where = true;
            }
            query += GET_STUDIES_STMT_LATE_END_DATE;
        }
        
        // Add responsibles criteria
        if (responsibleIds != null && responsibleIds.length > 0){ // check that at least one
            query += " AND " + GET_STUDIES_STMT_RESPONSIBLES;
            // Build criteria string: id1, id2, ...
            String responsibleCriteria = String.valueOf(responsibleIds[0]);
            for (int i = 1; i < responsibleIds.length; i++){
                responsibleCriteria += ", " + responsibleIds[i];
            }
            query += responsibleCriteria + ")";
        }
        
        // Set sorting
        if (sortBy == SORT_STUDIES_STUDY_NAME){
            query += " ORDER BY study_name ";
            if (sortOrder == SORT_ORDER_ASCENDING){
                query += " ASC ";
            }
            else{
                query += " DESC ";
            }
        }
        else if (sortBy == SORT_STUDIES_STUDY_TYPE_ID){
            query += " ORDER BY study_type_id ";
            if (sortOrder == SORT_ORDER_ASCENDING){
                query += " ASC ";
            }
            else{
                query += " DESC ";
            }
        }
        else if (sortBy == SORT_STUDIES_STUDY_END_DATE){
            query += " ORDER BY study_end_date ";
            if (sortOrder == SORT_ORDER_ASCENDING){
                query += " ASC ";
            }
            else{
                query += " DESC ";
            }
        }
        else{ // default
            query += " ORDER BY study_name ASC ";
        }
        
        lastStmt = query;
        //System.out.println("getStudies(?,?,?): " + query);
        
        // Set up statement and bind statement parameters
        // No guarantee that any where-statements will be there, so we need
        // to set the query's parameter indices accordingly.
        int parameterIndex = 1;
        PreparedStatement stmt = con.prepareStatement(query);
        if (studyType > 0) stmt.setInt(parameterIndex++, studyType);
        if (earlyEndDate != null) stmt.setDate(parameterIndex++, new java.sql.Date(earlyEndDate.getTime()));
        if (lateEndDate != null) stmt.setDate(parameterIndex++, new java.sql.Date(lateEndDate.getTime()));
        
        PreparedStatement responsiblesStmt =
        con.prepareStatement(GET_RESPONSIBLES_FOR_STUDY_STMT);
        
        PreparedStatement publicationsStmt =
        con.prepareStatement(GET_PUBLICATIONS_FOR_STUDY_STMT);
        
        
        // Execute statement and get results
        ResultSet rs = stmt.executeQuery();
        while (rs.next()){
            Study study = new Study();
            List responsibles = new ArrayList();
            List publications = new ArrayList();
            
            study.setId(rs.getInt("study_id"));
            study.setName(rs.getString("study_name"));
            study.setTypeId(rs.getInt("study_type_id"));
            study.setEnd(rs.getDate("study_end_date"));
            study.setDesc(rs.getString("study_desc"));
            study.setNoOfProfessionals(rs.getInt("no_of_professionals"));
            study.setNoOfStudents(rs.getInt("no_of_students"));
            study.setDuration(rs.getInt("study_duration"));
            study.setStart(rs.getDate("study_start_date"));
            study.setKeywords(rs.getString("keywords"));
            study.setUnitId(rs.getInt("study_duration_unit_id"));
            study.setUnit(getUnitName(study.getUnitId()));
            
            String lastEditBy = getLastEditedBy(study.getId());
            study.setLastEditedBy(lastEditBy);
            String owner = getOwner(study.getId());
            study.setOwner(owner);
            
            responsibles = getResponsiblesForStudy(rs.getInt(1));
            study.setResponsibles(responsibles);
            
            publications = getPublicationsForStudy(rs.getInt(1));
            study.setPublications(publications);
            
            responsiblesStmt.clearParameters();
            publicationsStmt.clearParameters();
            
            // Add to list
            studies.add(study);
        }
        stmt.clearParameters();
        
        closeConnection(con);
        return studies;
    }
    
    
    
    /** Retrieves all studies satisfying the given free text search string.
     * A value of NULL for this parameter will return all studies.
     *
     * @param search A free text search string.
     * @return A list of the studies satisfying the search string.
     */
    public List getStudies(String search, int sortBy, int sortOrder)
    throws Exception, SQLException{
        List studies = new ArrayList();
        Connection con = getConnection();
        
        if (search == null){
            // return all studies
        }
        
        String query = GET_STUDIES_FREE_TEXT_STMT;
        
        // Set up query according to parameters
        
        // Set sorting
        if (sortBy == SORT_STUDIES_STUDY_NAME){
            query += " ORDER BY study_name ";
            if (sortOrder == SORT_ORDER_ASCENDING){
                query += " ASC ";
            }
            else{
                query += " DESC ";
            }
        }
        else if (sortBy == SORT_STUDIES_STUDY_TYPE_ID){
            query += " ORDER BY study_type_id ";
            if (sortOrder == SORT_ORDER_ASCENDING){
                query += " ASC ";
            }
            else{
                query += " DESC ";
            }
        }
        else if (sortBy == SORT_STUDIES_STUDY_END_DATE){
            query += " ORDER BY study_end_date ";
            if (sortOrder == SORT_ORDER_ASCENDING){
                query += " ASC ";
            }
            else{
                query += " DESC ";
            }
        }
        else{ // default
            query += " ORDER BY study_name ASC ";
        }
        
        lastStmt = query;
        
        // Set up statement and bind statement parameters
        search = "%" + search + "%";
        PreparedStatement stmt = con.prepareStatement(query);
        stmt.setString(1, search);
        stmt.setString(2, search);
        stmt.setString(3, search);
        stmt.setString(4, search);
        stmt.setString(5, search);
        stmt.setString(6, search);
        stmt.setString(7, search);
        
        // Execute statement and get results
        ResultSet rs = stmt.executeQuery();
        while (rs.next()){
            Study study = new Study();
            List responsibles = new ArrayList();
            List publications = new ArrayList();
            
            study.setId(rs.getInt("study_id"));
            study.setName(rs.getString("study_name"));
            study.setTypeId(rs.getInt("study_type_id"));
            study.setEnd(rs.getDate("study_end_date"));
            study.setDesc(rs.getString("study_desc"));
            study.setNoOfProfessionals(rs.getInt("no_of_professionals"));
            study.setNoOfStudents(rs.getInt("no_of_students"));
            study.setDuration(rs.getInt("study_duration"));
            study.setStart(rs.getDate("study_start_date"));
            study.setKeywords(rs.getString("keywords"));
            study.setStudyNotes(rs.getString("study_notes"));
            study.setUnitId(rs.getInt("study_duration_unit_id"));
            study.setUnit(getUnitName(study.getUnitId()));
            
            String lastEditBy = getLastEditedBy(study.getId());
            study.setLastEditedBy(lastEditBy);
            String owner = getOwner(study.getId());
            study.setOwner(owner);

            responsibles = getResponsiblesForStudy(rs.getInt(1));
            study.setResponsibles(responsibles);
            
            publications = getPublicationsForStudy(rs.getInt(1));
            study.setPublications(publications);
            
            // Add to list
            studies.add(study);
        }
        stmt.clearParameters();
        
        // Get hits from reponsibles and build SQL
        List responsibleIds = getResponsibleIds(search);
        String responsibleSQL = GET_STUDIES_RESPONSIBLES_FREE_TEXT_STMT;
        for (ListIterator i = responsibleIds.listIterator(); i.hasNext(); ){
            Integer id = null;
            if (!i.hasPrevious()){ // it's the first one
                id =(Integer) i.next();
                responsibleSQL += id.toString();
            }
            else{
                id =(Integer) i.next();
                responsibleSQL += ", " + id.toString();
            }
        }
        responsibleSQL += ")";
        
        
        if (responsibleIds.size() > 0){
            // Execute query and add resulting studies to list
            Statement responsibleStmt = con.createStatement();
            ResultSet responsibleRs = responsibleStmt.executeQuery(responsibleSQL);
            while (responsibleRs.next()){
                Study study = new Study();
                List responsibles = new ArrayList();
                List publications = new ArrayList();
                
                study.setId(rs.getInt("study_id"));
                study.setName(rs.getString("study_name"));
                study.setTypeId(rs.getInt("study_type_id"));
                study.setEnd(rs.getDate("study_end_date"));
                study.setDesc(rs.getString("study_desc"));
                study.setNoOfProfessionals(rs.getInt("no_of_professionals"));
                study.setNoOfStudents(rs.getInt("no_of_students"));
                study.setDuration(rs.getInt("study_duration"));
                study.setStart(rs.getDate("study_start_date"));
                study.setKeywords(rs.getString("keywords"));
                study.setStudyNotes(rs.getString("study_notes"));
                study.setUnitId(rs.getInt("study_duration_unit_id"));
                study.setUnit(getUnitName(study.getUnitId()));
                
                responsibles = getResponsiblesForStudy(responsibleRs.getInt(1));
                study.setResponsibles(responsibles);
                publications = getPublicationsForStudy(responsibleRs.getInt(1));
                study.setPublications(publications);
                
                // Add to list
                studies.add(study);
            }
        }
        
        // Get hits from publications and build SQL
        List publicationIds = getPublicationIds(search);
        String publicationSQL = GET_STUDIES_PUBLICATION_FREE_TEXT_STMT;
        for (ListIterator i = publicationIds.listIterator(); i.hasNext(); ){
            Integer id = null;
            if (!i.hasPrevious()){ // it's the first one
                id =(Integer) i.next();
                publicationSQL += id.toString();
            }
            else{
                id =(Integer) i.next();
                publicationSQL += ", " + id.toString();
            }
        }
        publicationSQL += ")";
        
        if (publicationIds.size() > 0){
            // Execute query and add resulting studies to list
            Statement publicationStmt = con.createStatement();
            ResultSet publicationRs = publicationStmt.executeQuery(publicationSQL);
            while (publicationRs.next()){
                Study study = new Study();
                List responsibles = new ArrayList();
                List publications = new ArrayList();
                
                study.setId(rs.getInt("study_id"));
                study.setName(rs.getString("study_name"));
                study.setTypeId(rs.getInt("study_type_id"));
                study.setEnd(rs.getDate("study_end_date"));
                study.setDesc(rs.getString("study_desc"));
                study.setNoOfProfessionals(rs.getInt("no_of_professionals"));
                study.setNoOfStudents(rs.getInt("no_of_students"));
                study.setDuration(rs.getInt("study_duration"));
                study.setStart(rs.getDate("study_start_date"));
                study.setKeywords(rs.getString("keywords"));
                study.setStudyNotes(rs.getString("study_notes"));
                
                responsibles = getResponsiblesForStudy(publicationRs.getInt(1));
                study.setResponsibles(responsibles);
                publications = getPublicationsForStudy(publicationRs.getInt(1));
                study.setPublications(publications);
                
                // Add to list
                studies.add(study);
            }
        }
        
        closeConnection(con);
        return studies;
    }
    
    public String getLastEditedBy(int studyId) {
        String retValue = null;
        try {
            Audit audit = getMostRecentAudit(studyId);
            People p = getPeople(audit.getUserId());
            if ( audit != null ) {
                p = getPeople(audit.getUserId());
                retValue = p != null ? p.getPeopleFirstName() + " " + p.getPeopleFamilyName() : null;
            }
        } catch(Exception ex) {
            System.err.println(ex);
        }
        return retValue;
    }
    
    public String getOwner(int studyId) {
        String retValue = null;
        try {
            Audit audit = getFirstAudit(studyId);
            People p = null;
            if ( audit != null ) {
                p = getPeople(audit.getUserId());
                retValue = p != null ? p.getPeopleFirstName() + " " + p.getPeopleFamilyName() : null;
            }
        } catch(Exception ex) {
            System.err.println(ex);
        }
        return retValue;
    }
    
    /**
     * Retrieves study with the given id.
     *
     * @param id The id of the study to get.
     * @return The study with the given id.
     */
    public Study getSingleStudy(int id) throws Exception, SQLException{
        List responsibles = new ArrayList();
        List publications = new ArrayList();
        
        Connection con = getConnection();
        
        //System.out.println("getSingleStudy(" + id + ")");
        // Set up statements and bind statement parameters
        PreparedStatement stmt = con.prepareStatement(GET_SINGLE_STUDY_STMT);
        stmt.setInt(1, id);
        
        PreparedStatement responsiblesStmt =
        con.prepareStatement(GET_RESPONSIBLES_FOR_STUDY_STMT);
        responsiblesStmt.setInt(1, id);
        
        PreparedStatement publicationsStmt =
        con.prepareStatement(GET_PUBLICATIONS_FOR_STUDY_STMT);
        publicationsStmt.setInt(1, id);
        
        
        // Execute statements and get results
        ResultSet rs = stmt.executeQuery();
        rs.next();
        Study study = new Study();
        study.setId(id);
        study.setName(rs.getString(1));
        study.setTypeId(rs.getInt(2));
        study.setDesc(rs.getString(3));
        int duration = rs.getInt(4);
        if (rs.wasNull()){duration = -1;} // NULL so set to invalid
        study.setDuration(duration);
        int unitId = rs.getInt(5);
        if (rs.wasNull()){unitId = -1;} // NULL so set to invalid
        study.setUnitId(unitId);
        study.setStart(rs.getDate(6));
        study.setEnd(rs.getDate(7));
        study.setKeywords(rs.getString(8));
        int numStudents = rs.getInt(9);
        if (rs.wasNull()){numStudents = -1;} // NULL so set to invalid
        study.setNoOfStudents(numStudents);
        int numProfessionals = rs.getInt(10);
        if (rs.wasNull()){numProfessionals = -1;} // NULL so set to invalid
        study.setNoOfProfessionals(numProfessionals);
        study.setStudyNotes(rs.getString(11));
        
        responsibles = getResponsiblesForStudy(id);
        study.setResponsibles(responsibles);
        
        publications = getPublicationsForStudy(id);
        study.setPublications(publications);
        
        stmt.clearParameters();
        responsiblesStmt.clearParameters();
        publicationsStmt.clearParameters();
        
        return study;
    }
    
    /**
     * Inserts study into database.
     *
     */
    public int insertStudy(String studyName, int studyTypeId, int studyDuration,
    int studyDurationUnitId, java.util.Date studyStartDate, java.util.Date studyEndDate,
    String studyDesc, String studyKeywords, int numberOfStudents,
    int numberOfProfessionals, String studyNotes, int[] responsibleIds, int[] publicationIds)
    throws Exception, SQLException{
        
        Connection con = getConnection();
        
        // Convert dates to SQL dates.
        java.sql.Date start = null;
        if (studyStartDate != null){ // start date is allowed to be null
            start = new java.sql.Date(studyStartDate.getTime());
        }
        java.sql.Date end = new java.sql.Date(studyEndDate.getTime());
        
        // Set up statements and bind statement parameters
        PreparedStatement stmt = con.prepareStatement(INSERT_STUDY_STMT);
        stmt.setString(1, studyName);
        stmt.setInt(2, studyTypeId);
        if (studyDuration < 0) {stmt.setNull(3, Types.INTEGER);}
        else {stmt.setInt(3, studyDuration);};
        if (studyDurationUnitId  < 0) {stmt.setNull(4, Types.INTEGER);}
        else {stmt.setInt(4, studyDurationUnitId);}
        stmt.setDate(5, start);
        stmt.setDate(6, end);
        stmt.setString(7, studyDesc);
        stmt.setString(8, studyKeywords);
        if (numberOfStudents  < 0) {stmt.setNull(9, Types.INTEGER);}
        else {stmt.setInt(9, numberOfStudents);}
        if (numberOfProfessionals < 0) {stmt.setNull(10, Types.INTEGER);}
        else {stmt.setInt(10, numberOfProfessionals);}
        stmt.setString(11, studyNotes);
        
        // Execute statement
        stmt.executeUpdate();
        stmt.clearParameters();
        
        // Get the generated id for the study
        int studyId = getLastInsertId(con);
        
        
        // insert responsibles
        for (int i = 0; i < responsibleIds.length; i++){
            insertResponsible(studyId, responsibleIds[i]);
        }
        
        // insert publications
        if (publicationIds != null){ // there can be no publications
            for (int i = 0; i < publicationIds.length; i++){
                insertPublication(studyId, publicationIds[i]);
            }
        }
        
        return studyId;
    }
    
    /** Inserts publication into database
     * @param studyId The study id.
     * @param publicationId The publication id.
     * @return Number of affected records.
     */
    private int insertPublication(int studyId, int publicationId) throws Exception, SQLException{
        Connection con = getConnection();
        
        // Set up statements and bind statement parameters
        PreparedStatement stmt = con.prepareStatement(INSERT_STUDY_PUBLICATION_STMT);
        stmt.setInt(1, studyId);
        stmt.setInt(2, publicationId);
        
        // Execute statement
        int num = stmt.executeUpdate();
        return num;
    }
    
    /** Inserts study responsible into database
     * @param studyId The study id.
     * @param responsibleId The responsible's id.
     * @return Number of affected records.
     */
    private int insertResponsible(int studyId, int responsibleId) throws Exception, SQLException{
        Connection con = getConnection();
        
        // Set up statements and bind statement parameters
        PreparedStatement stmt = con.prepareStatement(INSERT_STUDY_RESPONSIBLE_STMT);
        stmt.setInt(1, studyId);
        stmt.setInt(2, responsibleId);
        
        // Execute statement
        int num = stmt.executeUpdate();
        stmt.clearParameters();
        return num;
    }
    
    /**
     * Inserts study into database.
     *
     */
    public int updateStudy(int studyId, String studyName, int studyTypeId, int studyDuration,
    int studyDurationUnitId, java.util.Date studyStartDate, java.util.Date studyEndDate,
    String studyDesc, String studyKeywords, int numberOfStudents,
    int numberOfProfessionals, String studyNotes, int[] responsibleIds, int[] publicationIds)
    throws Exception, SQLException{
        
        Connection con = getConnection();
        
        // Convert dates to SQL dates.
        java.sql.Date start = null;
        if (studyStartDate != null){ // start date is allowed to be null
            start = new java.sql.Date(studyStartDate.getTime());
        }
        java.sql.Date end = new java.sql.Date(studyEndDate.getTime());
        
        // Set up statements and bind statement parameters
        PreparedStatement stmt = con.prepareStatement(UPDATE_STUDY_STMT);
        stmt.setString(1, studyName);
        stmt.setInt(2, studyTypeId);
        stmt.setInt(3, studyDuration);
        stmt.setInt(4, studyDurationUnitId);
        stmt.setDate(5, start);
        stmt.setDate(6, end);
        stmt.setString(7, studyDesc);
        stmt.setString(8, studyKeywords);
        stmt.setInt(9, numberOfStudents);
        stmt.setInt(10, numberOfProfessionals);
        stmt.setString(11, studyNotes);
        stmt.setInt(12, studyId);
        
        
        // Execute statement
        int num = stmt.executeUpdate();
        stmt.clearParameters();
        
        // Delete existing responsibles
        deleteResponsiblesForStudy(studyId);
        
        // Delete existing publications
        deletePublicationsForStudy(studyId);
        
        // insert responsibles
        for (int i = 0; i < responsibleIds.length; i++){
            insertResponsible(studyId, responsibleIds[i]);
        }
        
        // insert publications
        if (publicationIds != null){ // publications are optional
            for (int i = 0; i < publicationIds.length; i++){
                insertPublication(studyId, publicationIds[i]);
            }
        }
        
        return num;
    }
    
    /**
     * Delete studies, including references to responsibles and publications.
     * @param ids The ids of the studies to delete.
     * @return The number of deleted studies.
     */
    public int deleteStudies(int[] ids) throws Exception, SQLException{
        Connection con = getConnection();
        
        // Set up statements and bind statement parameters
        Statement stmt = con.createStatement();
        String query = DELETE_STUDY_STMT;
        
        if (ids.length > 0){ // check that at least one
            // Build criteria string: id1, id2, ...
            String criteria = String.valueOf(ids[0]);
            for (int i = 1; i < ids.length; i++){
                criteria += ", " + ids[i];
            }
            query += criteria + ")";
        }
        
        // Delete responsibles and publications and audits for the studies
        for (int i = 0; i < ids.length; i++){
            deleteResponsiblesForStudy(ids[i]);
            deletePublicationsForStudy(ids[i]);
            deleteUrlsForStudy(ids[i]);
            removeAuditForStudy(ids[i]);
        }
        
        // Delete files for the studies
        for (int i = 0; i < ids.length; i++){
            List files = getFilesForStudy(ids[i]);
            for(ListIterator iter = files.listIterator(); iter.hasNext(); ){
                FileInfo fi = (FileInfo) iter.next();
                deleteFile(fi.getId());
                deleteFileForStudy(ids[i], fi.getId());
            }
        }
        // Delete studies
        int num = stmt.executeUpdate(query);
        return num;
    }
    
    /**
     * Deletes the conneciton between study and responsibles for a study.
     * @param studyId The study id.
     * @return Number of deleted records.
     */
    public int deleteResponsiblesForStudy(int studyId) throws Exception, SQLException{
        Connection con = getConnection();
        
        // Set up statements and bind statement parameters
        PreparedStatement stmt = con.prepareStatement(DELETE_RESPONSIBLES_FOR_STUDY_STMT);
        stmt.setInt(1, studyId);
        
        // Execute statements and get results
        int num = stmt.executeUpdate();
        return num;
    }
    
    /**
     * Deletes the conneciton between study and publications for a study.
     * @param studyId The study id.
     * @return Number of deleted records.
     */
    public int deletePublicationsForStudy(int studyId) throws Exception, SQLException{
        Connection con = getConnection();
        
        // Set up statements and bind statement parameters
        PreparedStatement stmt = con.prepareStatement(DELETE_PUBLICATIONS_FOR_STUDY_STMT);
        stmt.setInt(1, studyId);
        
        // Execute statements and get results
        int num = stmt.executeUpdate();
        return num;
    }
    
    /**
     * Gets the name of a study type.
     * @param studyTypeId The study type id.
     * @return The name of the study type.
     */
    public String getStudyTypeName(int studyTypeId) throws Exception, SQLException{
        Connection con = getConnection();
        
        // Set up statements and bind statement parameters
        PreparedStatement stmt = con.prepareStatement(GET_STUDY_TYPE_NAME_STMT);
        stmt.setInt(1, studyTypeId);
        
        ResultSet rs = stmt.executeQuery();
        if (rs.next()){
            return rs.getString(1);
        }
        else{
            return null;
        }
    }
    
    /**
     * Gets the name of a unit.
     * @param unitId The unit id.
     * @return The name of the unit type.
     */
    public String getUnitName(int unitId) throws Exception, SQLException{
        Connection con = getConnection();
        
        // Set up statements and bind statement parameters
        PreparedStatement stmt = con.prepareStatement(GET_UNIT_NAME_STMT);
        stmt.setInt(1, unitId);
        
        ResultSet rs = stmt.executeQuery();
        if (rs.next()){
            return rs.getString(1);
        }
        else{
            return null;
        }
    }
    
    /**
     * Gets all possible responsibles. For listing in selection box.
     * @ return List of all responsibles.
     */
    public List getAllResponsibles() throws Exception, SQLException{
        Connection con = getSimulaConnection();
        List responsibles = new ArrayList();
        Statement stmt = con.createStatement();
        
        ResultSet rs = stmt.executeQuery(GET_ALL_RESPONSIBLES_STMT);
        while (rs.next()){
            Responsible resp = new Responsible();
            resp.setId(rs.getInt(1));
            resp.setFirstName(rs.getString(2));
            resp.setFamilyName(rs.getString(3));
            responsibles.add(resp);
        }
        return responsibles;
    }
    
    /**
     * Gets all possible publications. For listing in selection box.
     * @ return List of all publications.
     */
    public List getAllPublications() throws Exception, SQLException{
        Connection con = getSimulaConnection();
        List publications = new ArrayList();
        Statement stmt = con.createStatement();
        
        ResultSet rs = stmt.executeQuery(GET_ALL_PUBLICATIONS_STMT);
        while (rs.next()){
            Publication pub = new Publication();
            pub.setId(rs.getInt(1));
            pub.setTitle(rs.getString(2));
            publications.add(pub);
        }
        return publications;
    }
    
    /**
     * Insert a file into the database.
     * @return The file id.
     */
    public int insertFile(String fileName, String fileType, String desc, int length, InputStream file) throws Exception, SQLException{
        Connection con = getConnection();
        
        PreparedStatement stmt = con.prepareStatement(INSERT_FILE_STMT);
        stmt.setString(1, fileName);
        stmt.setString(2, fileType);
        stmt.setString(3, desc);
        stmt.setBinaryStream(4, file, length);
        stmt.setInt(5, length);
        
        stmt.executeUpdate();
        
        int fileId = getLastInsertId(con);
        return fileId;
    }
    
    /**
     * Gets a file from the database.
     * @param id The id of the file.
     * @return An inputstream of the file.
     */
    public InputStream getFile(int id) throws Exception, SQLException{
        Connection con = getConnection();
        
        PreparedStatement stmt = con.prepareStatement(GET_FILE_STMT);
        stmt.setInt(1, id);
        
        ResultSet rs = stmt.executeQuery();
        InputStream ret = null;
        if (rs.next()){
            ret = rs.getBinaryStream(1);
        }
        return ret;
    }
    
    /**
     * Gets all information about a file.
     * @param id The id of the file.
     * @return The information for the file.
     */
    public FileInfo getFileInfo(int id) throws Exception, SQLException{
        Connection con = getConnection();
        
        PreparedStatement stmt = con.prepareStatement(GET_FILE_INFO_STMT);
        stmt.setInt(1, id);
        
        ResultSet rs = stmt.executeQuery();
        FileInfo ret = null;
        if (rs.next()){
            ret = new FileInfo();
            ret.setId(id);
            ret.setName(rs.getString(1));
            ret.setContentType(rs.getString(2));
            ret.setDesc(rs.getString(3));
            ret.setSize(rs.getInt(4));
        }
        return ret;
    }
    
    /**
     * Gets all files for a study.
     * @param id The id of the study.
     * @return A list of FileInfo objects.
     */
    public List getFilesForStudy(int id) throws Exception, SQLException{
        Connection con = getConnection();
        
        List files = new ArrayList();
        PreparedStatement stmt = con.prepareStatement(GET_FILES_FOR_STUDY_STMT);
        stmt.setInt(1, id);
        
        ResultSet rs = stmt.executeQuery();
        //FileInfo fi = null;
        while (rs.next()){
            FileInfo fi = new FileInfo();
            fi.setId(rs.getInt(1));
            fi.setName(rs.getString(2));
            fi.setContentType(rs.getString(3));
            fi.setDesc(rs.getString(4));
            fi.setSize(rs.getInt(5));
            files.add(fi);
        }
        return files;
    }
    
    /**
     * Connects a file to a study.
     * @param studyId The id of the study.
     * @param fileId The id of the file.
     * @return Number of inserted records.
     */
    public int insertFileForStudy(int studyId, int fileId) throws Exception, SQLException{
        Connection con = getConnection();
        
        PreparedStatement stmt = con.prepareStatement(INSERT_FILE_FOR_STUDY_STMT);
        stmt.setInt(1, studyId);
        stmt.setInt(2, fileId);
        
        int ret = stmt.executeUpdate();
        return ret;
    }
    
    /**
     * Deletes a file
     * @param id The id of the file.
     * @return Number of deleted files.
     */
    public int deleteFile(int id) throws Exception, SQLException{
        Connection con = getConnection();
        
        PreparedStatement stmt = con.prepareStatement(DELETE_FILE_STMT);
        stmt.setInt(1, id);
        
        int ret = stmt.executeUpdate();
        return ret;
    }
    
    /**
     * Disconnects a file from a study.
     * @param studyId The id of the study.
     * @param fileId The id of the file.
     * @return Number of deleted records.
     */
    public int deleteFileForStudy(int studyId, int fileId) throws Exception, SQLException{
        Connection con = getConnection();
        
        PreparedStatement stmt = con.prepareStatement(DELETE_FILE_FOR_STUDY_STMT);
        stmt.setInt(1, studyId);
        stmt.setInt(2, fileId);
        
        int ret = stmt.executeUpdate();
        return ret;
    }
    
    /**
     * Deletes all orphan files, i.e. all files that are not connected to a study.
     * @return Number of deleted files.
     */
    public int deleteOrphanFiles() throws Exception, SQLException{
        Connection con = getConnection();
        Statement stmt = con.createStatement();
        
        // Get ids of orphan files
        ResultSet rs = stmt.executeQuery(GET_ORPHAN_FILES_STMT);
        
        // Build delete statement based on file ids
        int numDeleted = 0;
        boolean noOrphans = true; // initially assume there are no orphans
        String deleteSql = "DELETE FROM tbl_file WHERE file_id IN (";
        
        if (rs.next()){
            noOrphans = false;
            deleteSql += rs.getInt(1);
        }
        
        while (rs.next()){
            deleteSql += ", " + rs.getInt(1);
        }
        deleteSql += ")";
        
        // If there are orphans delete them
        if (noOrphans == false){
            numDeleted = stmt.executeUpdate(deleteSql);
        }
        
        closeConnection(con);
        return numDeleted;
    }
    
    /**
     * Insert an URL with a description for the given study.
     * @param studyId The id of the study
     * @param url The URL to insert.
     * @param desc Description for the URL.
     * @return The id of the newly inserted URL.
     */
    public int insertUrl(int studyId, String url, String desc) throws Exception, SQLException{
        Connection con = getConnection();
        
        PreparedStatement stmt = con.prepareStatement(INSERT_URL_STMT);
        stmt.setInt(1, studyId);
        stmt.setString(2, url);
        stmt.setString(3, desc);
        
        stmt.executeUpdate();
        
        int urlId = getLastInsertId(con);
        closeConnection(con);
        return urlId;
    }
    
    /**
     * Get all URLs for a study.
     * @param studyId The id of the study.
     * @return A list containing all the URLs as no.machina.simula.Url.
     */
    public List /*Url*/ getUrlsForStudy(int studyId) throws Exception, SQLException{
        Connection con = getConnection();
        List urls = new ArrayList();
        
        PreparedStatement stmt = con.prepareStatement(GET_URL_STMT);
        stmt.setInt(1, studyId);
        
        ResultSet rs = stmt.executeQuery();
        
        while (rs.next()){
            Url u = new Url(rs.getString(1), rs.getString(2));
            urls.add(u);
        }
        
        closeConnection(con);
        return urls;
    }
    
    /**
     * Deletes all URLs for a study.
     * @param The id of the study.
     * @return The number of deleted URLs.
     */
    public int deleteUrlsForStudy(int studyId) throws Exception, SQLException{
        Connection con = getConnection();
        
        PreparedStatement stmt = con.prepareStatement(DELETE_URL_STMT);
        stmt.setInt(1, studyId);
        
        int ret = stmt.executeUpdate();
        closeConnection(con);
        return ret;
    }
    
    /**
     * Gets a person from the database.
     * @param userId The id of the person.
     * @return The People object for the person.
     */
    public People getPeople(int userId) throws Exception, SQLException{
        People people = null;
        Connection con = getSimulaConnection();
        String sql = "select people_id, people_first_name, people_family_name, people_position from people where people_id=?";
        
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, userId);
        
        ResultSet rs = stmt.executeQuery();
        
        if (rs.next()){
            people = new People();
            people.setPeopleId(rs.getInt(1));
            people.setPeopleFirstName(rs.getString(2));
            people.setPeopleFamilyName(rs.getString(3));
            people.setPeoplePosition(rs.getString(4));
        }
        /*
        List users = MasterDataDAO.getInstance().getValues(sql, new Object[] {new Integer(userId)});
        if ( users != null && users.size() > 0 ) {
            Map values = (Map)users.get(0);
            retValue = People.createPeople(values);
        }*/
        return people;
    }
    
    public List getUserActions(String userId) {
        List retList = new ArrayList();
        Connection con = null;
        int userIdInt = NumberUtils.stringToInt(userId, -1);
        try {
            con = getConnection();
            PreparedStatement stmt = con.prepareStatement(GET_USERACTION_STMT);
            stmt.setInt(1, userIdInt);
            ResultSet rs = stmt.executeQuery();
            while ( rs.next() ) {
                Action act = new Action();
                act.setActionDesc(rs.getString(Action.ACTION_DESC));
                act.setActionId(rs.getInt(Action.ACTION_ID));
                act.setActionName(rs.getString(Action.ACTION_NAME));
                retList.add(act);
            }
        } catch(SQLException ex) {
            System.err.println(ex);
        } catch(Exception ex) {
            System.err.println(ex);
        } finally {
            if ( con != null ) {
                closeConnection(con);
            }
        }
        return retList;
    }
    
    public boolean listContainsAction(List actionList, String actionName) {
        int size = actionList != null ? actionList.size() : 0;
        String security = System.getProperty("no.machina.simula.security");
        if ( security == null || !security.equalsIgnoreCase("true")) {
            return true;
        }
        boolean retValue = false;
        for ( int i = 0;!retValue && i < size;i++) {
            Action act = (Action)actionList.get(i);
            retValue = act.equalsName(actionName);
        }
        return retValue;
    }
    
    public String getAllUserActionsAsString(List actionList) {
        int size = actionList != null ? actionList.size() : 0;
        StringBuffer sb = new StringBuffer();
        if ( actionList == null ) {
            sb.append("<null>");
        } else if ( actionList.size() == 0) {
            sb.append("<none>");
        }
        for ( int i = 0;i < size;i++) {
            Action tmpAction = (Action)actionList.get(i);
            sb.append(tmpAction.getActionName());
            sb.append(",");
        }
        return sb.toString();
    }
    
    public List getUserActions(int userTypeId) {
        List retList = new ArrayList();
        Connection con = null;
        try {
            con = getConnection();
            PreparedStatement stmt = con.prepareStatement(GET_USERACTIONID_STMT);
            stmt.setInt(1, userTypeId);
            ResultSet rs = stmt.executeQuery();
            while ( rs.next() ) {
                Action act = new Action();
                act.setActionDesc(rs.getString(Action.ACTION_DESC));
                act.setActionId(rs.getInt(Action.ACTION_ID));
                act.setActionName(rs.getString(Action.ACTION_NAME));
                retList.add(act);
            }
        } catch(SQLException ex) {
            System.err.println(ex);
        } catch(Exception ex) {
            System.err.println(ex);
        } finally {
            if ( con != null ) {
                closeConnection(con);
            }
        }
        return retList;
    }
    
    public User getUser(int userId) throws Exception, SQLException{
        User retUser = null;
        Connection con = null;
        con = getConnection();
        PreparedStatement stmt = con.prepareStatement(GET_USER_STMT);
        stmt.setInt(1, userId);
        ResultSet rs = stmt.executeQuery();
        if ( rs.next() ) {
            User tmpUser = new User();
            tmpUser.setPassword(rs.getString("password"));
            tmpUser.setUserId(rs.getInt("user_id"));
            tmpUser.setUserName(rs.getString("username"));
            tmpUser.setUserType(rs.getString("user_type_name"));
            retUser = tmpUser;
        }
        closeConnection(con);
        return retUser;
    }
    
    public void addActionToUserType(String actionIdStr, String userTypeIdStr) {
        Connection con = null;
        int actionId = NumberUtils.stringToInt(actionIdStr, -1);
        int userTypeId = NumberUtils.stringToInt(userTypeIdStr, -1);
        if ( actionId == -1 || userTypeId == -1) {
            return;
        }
        try {
            con = getConnection();
            PreparedStatement stmt = con.prepareStatement("insert into tbl_user_type_has_action (action_id, user_type_id) values (?,?)");
            stmt.setInt(1, actionId);
            stmt.setInt(2, userTypeId);
            stmt.executeUpdate();
        } catch(Exception ex) {
            System.err.println(ex);
        } finally {
            this.closeConnection(con);
        }
    }
    
    public void removeActionToUserType(String actionIdStr, String userTypeIdStr) {
        Connection con = null;
        int actionId = NumberUtils.stringToInt(actionIdStr, -1);
        int userTypeId = NumberUtils.stringToInt(userTypeIdStr, -1);
        if ( actionId == -1 || userTypeId == -1) {
            return;
        }
        try {
            con = getConnection();
            PreparedStatement stmt = con.prepareStatement("delete from tbl_user_type_has_action where action_id=? and user_type_id=?");
            stmt.setInt(1, actionId);
            stmt.setInt(2, userTypeId);
            stmt.executeUpdate();
        } catch(Exception ex) {
            System.err.println(ex);
        } finally {
            this.closeConnection(con);
        }
    }
    
    public void modifyUserType(String peopleIdStr, String userTypeIdStr, List existingData) {
        Connection con = null;
        int peopleId = NumberUtils.stringToInt(peopleIdStr, -1);
        int userTypeId = NumberUtils.stringToInt(userTypeIdStr, -1);
        if ( peopleId == -1 ) {
            return;
        }
        
        People current = null;
        
        // Find the correct entry in existingData
        int size = existingData != null ? existingData.size() : 0;
        for ( int i = 0;i < size;i++) {
            People p = (People)existingData.get(i);
            if ( p.getPeopleId() == peopleId && p.getUserTypeId() != userTypeId) {
                current = p;
            }
        }
        
        // If we found an entry which has changed, update the database
        if ( current != null ) {
            try {
                con = getConnection();
                PreparedStatement stmt = null;
                stmt = con.prepareStatement("delete from tbl_user where people_id=? ");
                stmt.setInt(1, peopleId);
                stmt.executeUpdate();
                stmt.close();
                if ( userTypeId >= 0 ) {
                    stmt = con.prepareStatement("insert into tbl_user (people_id, user_type_id) values (?, ?) ");
                    stmt.setInt(1, peopleId);
                    stmt.setInt(2, userTypeId);
                    stmt.executeUpdate();
                    stmt.close();
                }
            } catch(Exception ex) {
                System.err.println(ex);
            } finally {
                this.closeConnection(con);
            }
        }
    }
    
    /*************************************
     *  Audit trails
     ************************************/
    
    /** Create a trail for the study, user
     */
    public void auditStudy(int studyId, int userId) {
        String sql = "insert into tbl_audit (study_id, user_id, audit_date) values (?,?,?)";
        MasterDataDAO.getInstance().runUpdateSql(sql, new Object[] {new Integer(studyId), new Integer(userId), new java.sql.Date(new Date().getTime())});
    }
    
    private int getActionId(String actionStr) {
        int retValue = -1;
        
        List actions = MasterDataDAO.getInstance().getList(Action.class);
        Action tmpAction = null;
        int size = actions != null ? actions.size() : 0;
        for ( int i = 0;i < size;i++) {
            Action tmp = (Action)actions.get(i);
            if ( tmp.equalsName(actionStr)) {
                tmpAction = tmp;
            }
        }
        if ( tmpAction != null ) {
            retValue = tmpAction.getActionId();
        }
        
        return retValue;
    }
    
    /**
     * Gets the most recent audit for a study.
     * @param studyId The id for the study.
     * @return The most recent audit for the study.
     */
    public Audit getMostRecentAudit(int studyId) throws Exception, SQLException{
        Audit retValue = null;
        //String sql = "select * from tbl_audit where study_id=? and user_id=? order by audit_date desc";
        String sql = "select * from tbl_audit where study_id=? order by audit_date desc, tbl_audit_id desc";
        Connection con = getConnection();
        
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, studyId);
        
        ResultSet rs = stmt.executeQuery();
        
        if (rs.next()){
            retValue = new Audit();
            retValue.setAuditId(rs.getInt(1));
            retValue.setStudyId(rs.getInt(2));
            retValue.setUserId(rs.getInt(3));
            retValue.setAuditDate(rs.getDate(4));
        }
        return retValue;
    }
    
    /**
     * Gets the first audit for a study.
     * @param studyId The id for the study.
     * @return The first audit (i.e. the creation) for the study
     */
    public Audit getFirstAudit(int studyId) throws Exception, SQLException {
        Audit retValue = null;
        //        String sql = "select * from tbl_audit, people where tbl_audit.user_id=people.people_id " +
        //        " and study_id=? order by audit_date asc";
        String sql = "select * from tbl_audit where " +
        " study_id=? order by audit_date asc, tbl_audit_id asc";
        //System.err.println("getFirstAudit: studyId = " + studyId);
        Connection con = getConnection();
        
        PreparedStatement stmt = con.prepareStatement(sql);
        stmt.setInt(1, studyId);
        
        ResultSet rs = stmt.executeQuery();
        
        if (rs.next()){
            retValue = new Audit();
            retValue.setAuditId(rs.getInt(1));
            retValue.setStudyId(rs.getInt(2));
            retValue.setUserId(rs.getInt(3));
            retValue.setAuditDate(rs.getDate(4));
        }
        return retValue;
    }
    
    
    public void removeAuditForStudy(int studyId) {
        String sql = "delete from tbl_audit where study_id=?";
        MasterDataDAO.getInstance().runUpdateSql(sql, new Object[] {new Integer(studyId)});
    }
    
    // test
    public static void main(String[] args) throws Exception, SQLException{
        DB db = DB.getInstance();
        Connection con = db.getConnection();
        System.out.println("Connected:" + con.getAutoCommit());
        //List l = db.getStudies(null, null, null, null);
        
        db.deleteOrphanFiles();
        Study s = db.getSingleStudy(1);
        System.out.println(s.getId() + s.getName());
        System.out.println(s.getResponsibles().get(0).toString());
        db.closeConnection(con);
        System.out.println(con.isClosed());
    }
}
