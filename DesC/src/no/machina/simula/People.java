/*
 * People.java
 *
 * Created on 14. oktober 2003, 22:10
 */

package no.machina.simula;

import java.util.Map;
import no.machina.simula.Converter;
import org.apache.commons.lang.math.NumberUtils;

/**
 *
 * @author  cato
 */
public class People {
    
    public static final String PEOPLE_ID="people_id";
    public static final String PEOPLE_POSITION="people_position";
    public static final String PEOPLE_FIRST_NAME="people_first_name";
    public static final String PEOPLE_FAMILY_NAME="people_family_name";
    public static final String USER_TYPE_ID="user_type_id";

    
    /** Holds value of property peopleId. */
    private int peopleId;
    
    /** Holds value of property peoplePosition. */
    private String peoplePosition;
    
    /** Holds value of property peopleFirstName. */
    private String peopleFirstName;
    
    /** Holds value of property peopleFamilyName. */
    private String peopleFamilyName;
    
    /** Holds value of property userTypeId. */
    private int userTypeId;
    
    /** Creates a new instance of People */
    public People() {
    }
    
    public static People createPeople(Map params) {
        People p = new People();
        p.setPeopleId(NumberUtils.stringToInt(Converter.getFirstString(params.get(PEOPLE_ID))));
        p.setPeoplePosition(Converter.getFirstString(params.get(PEOPLE_POSITION)));
        p.setPeopleFirstName(Converter.getFirstString(params.get(PEOPLE_FIRST_NAME)));
        p.setPeopleFamilyName(Converter.getFirstString(params.get(PEOPLE_FAMILY_NAME)));
        p.setUserTypeId(NumberUtils.stringToInt(Converter.getFirstString(params.get(USER_TYPE_ID))));
        return p;
    }
    
    public String toString() {
        return peopleId + ", " + peopleFirstName + " " + peopleFamilyName + ", " + peoplePosition;
    }
    
    public boolean equals(Object obj) {
        boolean retValue = false;

        if ( obj instanceof People ) {
            retValue = ((People)obj).getPeopleId() == getPeopleId();
        }

        return retValue;
    }
    
    /** Getter for property peopleId.
     * @return Value of property peopleId.
     *
     */
    public int getPeopleId() {
        return this.peopleId;
    }
    
    /** Setter for property peopleId.
     * @param peopleId New value of property peopleId.
     *
     */
    public void setPeopleId(int peopleId) {
        this.peopleId = peopleId;
    }
    
    /** Getter for property peoplePosition.
     * @return Value of property peoplePosition.
     *
     */
    public String getPeoplePosition() {
        return this.peoplePosition;
    }
    
    /** Setter for property peoplePosition.
     * @param peoplePosition New value of property peoplePosition.
     *
     */
    public void setPeoplePosition(String peoplePosition) {
        this.peoplePosition = peoplePosition;
    }
    
    /** Getter for property peopleFirstName.
     * @return Value of property peopleFirstName.
     *
     */
    public String getPeopleFirstName() {
        return this.peopleFirstName;
    }    

    /** Setter for property peopleFirstName.
     * @param peopleFirstName New value of property peopleFirstName.
     *
     */
    public void setPeopleFirstName(String peopleFirstName) {
        this.peopleFirstName = peopleFirstName;
    }    
    
    /** Getter for property peopleFamilyName.
     * @return Value of property peopleFamilyName.
     *
     */
    public String getPeopleFamilyName() {
        return this.peopleFamilyName;
    }
    
    /** Setter for property peopleFamilyName.
     * @param peopleFamilyName New value of property peopleFamilyName.
     *
     */
    public void setPeopleFamilyName(String peopleFamilyName) {
        this.peopleFamilyName = peopleFamilyName;
    }
    
    /** Getter for property userTypeId.
     * @return Value of property userTypeId.
     *
     */
    public int getUserTypeId() {
        return this.userTypeId;
    }
    
    /** Setter for property userTypeId.
     * @param userTypeId New value of property userTypeId.
     *
     */
    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }
    
}
